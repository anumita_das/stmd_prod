# Application Architecture
 - All of the app's code goes in a folder named `src`. All feature areas are in their own folder, with their own NgModule.
 - All content is one asset per file. Each component, service, and pipe is in its own file. All third party vendor scripts are stored in another folder and not in the `src` folder.
 - Strucure of the app maintains 'LIFT' and `Folders-by-feature`
        - `L`ocate code quickly
        - `I`dentify the code at a glance
        - keep the `F`lattest structure you can
        - `T`ry to be DRY
 - The entire application is splitted into three different modules — `Core`, `Shared` and `Feature`.

> [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself) (Don't Repeat Yourself) - Being DRY is important, but not crucial if it sacrifices the other elements of LIFT. That's why it's called T-DRY. 


#### CoreModule
All services which have to have one and only one instance per application (singleton services) should be implemented here. Typical example can be authentication service or user service. Let's look at an example of `CoreModule` implementation.
```sh
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { SomeSingletonService } from './some-singleton/some-singleton.service';

@NgModule({
  imports: [
    HttpClientModule,
  ],
  declarations: [],
  providers: [
    SomeSingletonService
  ]
})
export class CoreModule {
	constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
		throwIfAlreadyLoaded(parentModule, 'CoreModule');
	}
}
```

#### SharedModule
All the “dumb” components and pipes should be implemented here. These components don’t import and inject services from core or other features in their constructors. They should receive all data though attributes in the template of the component using them. This all sums up to the fact that `SharedModule` doesn’t have any dependency to the rest of our application.

#### FeatureModule
Multiple modules for every independent feature of our application should be added here. Feature modules should only import services from CoreModule. If feature module A needs to import service from feature module B consider moving that service into core.
> In some cases there is a need for services which are shared only by some features and it wouldn’t make much sense to move them into core. In that case we can create special shared feature modules.

# Installation
Clone git repository, install the dependencies and devDependencies and run the application.

```sh
$ git clone <REPOSITORY_URL>
$ npm install
$ ng serve
```

# Plugins
The application is currently extended with the following plugins. Instructions on how to use them in your own application are linked below.

| Plugin | README |
| ------ | ------ |
| ----- | [File](URL) |



# Build





**Bold Text**
