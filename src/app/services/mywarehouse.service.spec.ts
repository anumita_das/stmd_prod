import { TestBed, inject } from '@angular/core/testing';

import { MywarehouseService } from './mywarehouse.service';

describe('MywarehouseService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MywarehouseService]
    });
  });

  it('should be created', inject([MywarehouseService], (service: MywarehouseService) => {
    expect(service).toBeTruthy();
  }));
});
