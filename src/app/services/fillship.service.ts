import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Response } from '@angular/http';
import { Observable,BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../shared/models/user.model'; 
import { config } from '../app.config';


@Injectable({
  providedIn: 'root'
})
export class FillshipService {

	private rootUrl = config.api;
	private httpOptions: any;
  private extraCost = new BehaviorSubject('0.00');
  currentExtracost = this.extraCost.asObservable();
  isLoggedIn = false;
  redirectUrl = 'my-account';
  item : any;
  
  userId = '';

	constructor(private http: HttpClient) {
    this.item = JSON.parse(localStorage.getItem('tokens'));
    if (this.item != null) {
      if (this.item.token) {
        this.isLoggedIn = true;
      }
      this.userId = this.item.user.id;
    }

  	this.httpOptions = {
          headers: new HttpHeaders({ 'Content-Type': 'application/json'})
      };
	}

  getUserCartList(data) {
    return this.http.post(this.rootUrl+'getusercartlist/',data,this.httpOptions)
  }

  updateUserCart(formData,type){
    return this.http.post(this.rootUrl+'updateusercart/'+ this.userId + '/' +type, formData)
  }

  savecartlist(data){
    return this.http.post(this.rootUrl+'savecartlist/'+ this.userId,data,this.httpOptions)
  }

  findmybox(formData) {
    return this.http.post(this.rootUrl+'findmybox/'+ this.userId,formData,this.httpOptions)
  }

  removeCartItem(id) {
    const data = {id:id};
    return this.http.post(this.rootUrl+'removecartitem/',data,this.httpOptions)
  }

  submitfillshipdata(formData) {
    return this.http.post(this.rootUrl+'submitfillshipdata/'+ this.userId,formData,this.httpOptions)
  }
  
  updateFillShipData(formData) {
    return this.http.post(this.rootUrl+'updatefillshipdata/'+ this.userId,formData,this.httpOptions)
  }
  
  clearUserCart(type) {
    const formData = {userId : this.userId, type: type};
    return this.http.post(this.rootUrl+'clearusercart/',formData,this.httpOptions)
  }

   getCurrencyList(data) {
       return this.http.post(this.rootUrl+'getcurrencylist/',{currencyCode:data},this.httpOptions);
    }

    getPaymentMethodList(data) {
       return this.http.post(this.rootUrl+'getpaymentmethodlist/',data,this.httpOptions);
    }

    validatecouponcode(params) {
      const usercart = JSON.parse(localStorage.getItem('userFillShipCart'));
      const couponcode = {coupon:params, usercart:usercart, userId: this.userId};
      return this.http.post(this.rootUrl+'validatefillshipcoupon',couponcode,this.httpOptions)
    }

    validateEwallet(formData) {
      return this.http.post(this.rootUrl+'validateewallet/',formData,this.httpOptions);
    }

    submitOrder(formData) {
     return this.http.post(this.rootUrl+'submitfillshiporder/'+this.userId,formData,this.httpOptions);
    }

    getUserFillship(warehouse,userId,dataDuration) {
      const data = {userId:userId,warehouse:warehouse,dataDuration:dataDuration};
      return this.http.post(this.rootUrl+'getuserfillship',data);
    }

    shipout(fillshipShipmentId,ewalletTransferAmount) {
      const data = {fillshipShipmentId:fillshipShipmentId,ewalletTransferAmount:ewalletTransferAmount,userId:this.userId};
      return this.http.post(this.rootUrl+'fillshipshipout',data);
    }

    setExtracharge(extracost) {
      this.extraCost.next(extracost);
    }

    payextracharge(formData) {
      return this.http.post(this.rootUrl+'payfillshipextracharge',formData);
    }

    getDeliverydetail(itemId) {
      const data = {fillshipItemId:itemId}
      return this.http.post(this.rootUrl+'fillshipdeliverydetails',data);
    }
  
    getAllAddedBox() {
      return this.http.get(this.rootUrl+'fillshipalladdedboxes',this.httpOptions);
    }

    selectBox(data) {
      return this.http.post(this.rootUrl+'fillshipselectbox/'+this.userId,data,this.httpOptions);
    }
  
}
