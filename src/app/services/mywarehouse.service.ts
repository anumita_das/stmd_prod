import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Response } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../shared/models/user.model'; 
import { config } from '../app.config';


@Injectable({
  providedIn: 'root'
})
export class MywarehouseService {

	private rootUrl = config.api;
	private httpOptions: any;
  isLoggedIn = false;
  redirectUrl = 'my-account';
  item : any;
  
  userId = '';

  	constructor(private http: HttpClient) {
      //localStorage.clear();
      this.item = JSON.parse(localStorage.getItem('tokens'));
      //console.log(this.item.user.id);
      if (this.item != null) {
        if (this.item.token) {
          this.isLoggedIn = true;
        }
        this.userId = this.item.user.id;
      }

  		this.httpOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/json'})
        };
  	}

    getUserShipmentList(warehouse) {
      const formData = {userId : this.userId, warehouse : warehouse};

      return this.http.post(this.rootUrl+'getusershipmentlist/',formData,this.httpOptions)
    }

    allShippingMethodList(formData) {
      return this.http.post(this.rootUrl+'allshippingmethodlist',formData,this.httpOptions)
    }

    getDeliveryDetails(formData)
    {
      return this.http.post(this.rootUrl+'getdeliverydetails',formData,this.httpOptions)
    }

    calculateDeliveryShippingCost(formData) {
      return this.http.post(this.rootUrl+'calculatedeliveryshipping',formData,this.httpOptions)
    }

    adddeliveryothercharges(formData){
     return this.http.post(this.rootUrl+'adddeliveryothercharges',formData,this.httpOptions)
    }

    addpackagesnapshot(formData){
     return this.http.post(this.rootUrl+'addpackagesnapshot',formData,this.httpOptions)
    }
    
    sendwronginventorynotification(formData){
     return this.http.post(this.rootUrl+'sendwronginventorynotification',formData,this.httpOptions)
    }

    updateinventory(formData){
     return this.http.post(this.rootUrl+'updateinventory',formData,this.httpOptions)
    }

    getshippimgmethod(formData){
     return this.http.post(this.rootUrl+'getshippimgmethod',formData,this.httpOptions)
    }

    warehouseCheckout(formData) {
        return this.http.post(this.rootUrl+'warehousecheckout',formData,this.httpOptions)
    }

    updateUserWarehouseData(formData){
      return this.http.post(this.rootUrl+'updateuserwarehouse/'+ this.userId, formData)
    }

    getCurrencyList(data) {
     return this.http.post(this.rootUrl+'getcurrencylist/',{currencyCode:data},this.httpOptions);
    }

    getPaymentMethodList(data) {
       return this.http.post(this.rootUrl+'getpaymentmethodlist/',data,this.httpOptions);
    }

    validatecouponcode(params) {
      const usercart = JSON.parse(localStorage.getItem('userWarehouseCart'));
      const couponcode = {coupon:params, usercart:usercart, userId: this.userId};
      return this.http.post(this.rootUrl+'validateshipmentcoupon',couponcode,this.httpOptions)
    }

    validateEwallet(formData) {
      return this.http.post(this.rootUrl+'validateewallet/',formData,this.httpOptions);
    }

    submitOrder(formData) {
     return this.http.post(this.rootUrl+'submitshipmentorder/'+this.userId,formData,this.httpOptions);
    }

    uploadInvoice(fileToUpload: File,shipmentId) {
      const formData = new FormData();

      formData.append('fileItem', fileToUpload, fileToUpload.name);
      formData.append('shipmentId',shipmentId);

      return this.http.post(this.rootUrl+'uploadshipmentinvoice',formData); 
    }

    shipmentOrderHistory(params) {
      const formData = params;
      formData['userId'] = this.userId;
      return this.http.post(this.rootUrl+'shipmentorderhistory/',formData,this.httpOptions)
    }

    getallshipmentstatus() {
      return this.http.get(this.rootUrl+'getallshipmentstatus',this.httpOptions);
    }

    getShipmentdetail(shipmentid) {
      const data = {shipmentid:shipmentid};
      return this.http.post(this.rootUrl+'getshipmentdetails',data,this.httpOptions);
    }

    printInvoice(shipmentid,shipmentType,paymentStatus) {
      const data = {
        shipmentid:shipmentid,
        shipmentType:shipmentType,
        paymentStatus:paymentStatus,
      };
      return this.http.post(this.rootUrl+'printshipmentinvoice',data);
    }

    getShipmentsnapshots(shipmentid) {
      const data = {
        shipmentid:shipmentid
      }

      return this.http.post(this.rootUrl+'getshipmentsnapshots',data);
    }

    updateItemprice(packageId,price) {
      const data = {
        packageId : packageId,
        price : price,
        userId : this.userId,
      }

      return this.http.post(this.rootUrl+'updateitemprice',data);
    }

    uploadDicountedInvoice(formData) {

      return this.http.post(this.rootUrl+'uploaddicountedinvoice',formData);
    }

    getDiscountedUploadedInvoice(deliveryId) {
      console.log(deliveryId);
      const data = {
        deliveryId : deliveryId,
        userId : this.userId
      }

      return this.http.post(this.rootUrl+'getdiscounteduploadedinvoice',data);
    }

    downloadInvoice(packageId)
    {
      return this.http.get(this.rootUrl+'downloadinvoice/'+packageId);
    }

    selectDelieveryItems(formData)
    {
      return this.http.post(this.rootUrl+'getDelieveryItems',formData);
    }

    getShipmentImages(shipmentId, deliveryId, itemId)
    {
      const formData = {itemId:itemId, shipmentId: shipmentId, deliveryId:deliveryId};
       return this.http.post(this.rootUrl+'getshipmentsnapshots/',formData,this.httpOptions);
    }

    updateReturnData(formData)
    {
      return this.http.post(this.rootUrl+'updateReturnData',formData);
    }

    preocessPayeezy() {
      return this.http.get(this.rootUrl+'processpayeezy');
    }

    getAccountManager(representativeId)
    {
       const formData = {userId : this.userId, representativeId : representativeId};
      return this.http.post(this.rootUrl+'getaccountmanager', formData);
    }

}
