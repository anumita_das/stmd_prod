import { TestBed, inject } from '@angular/core/testing';

import { FillshipService } from './shopforme.service';

describe('FillshipService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FillshipService]
    });
  });

  it('should be created', inject([FillshipService], (service: FillshipService) => {
    expect(service).toBeTruthy();
  }));
});
