import { TestBed, inject } from '@angular/core/testing';

import { ShopformeService } from './shopforme.service';

describe('ShopformeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ShopformeService]
    });
  });

  it('should be created', inject([ShopformeService], (service: ShopformeService) => {
    expect(service).toBeTruthy();
  }));
});
