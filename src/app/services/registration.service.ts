import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Response } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../shared/models/user.model'; 
import { UserAddress } from '../shared/models/useraddress.model';
import { config } from '../app.config';
import { UserShipmentSettings } from '../shared/models/usershipmentsettings.model';


@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

	private rootUrl = config.api;
	private httpOptions: any;
        isLoggedIn = false;
        redirectUrl = 'my-account';
        item : any;

        userId = '';

  	constructor(private http: HttpClient) {
      this.item = JSON.parse(localStorage.getItem('tokens'));
      if (this.item != null) {
        if (this.item.token) {
          this.isLoggedIn = true;
        }
        this.userId = this.item.user.id;
      }

  		this.httpOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/json'})
        };
  	}

  	registerUser(user : User){

  		const data: User = {
  			title: user.title,
  			firstName : user.firstName,
  			lastName : user.lastName,
  			email : user.email,
        confirmEmail : user.confirmEmail,
  			number : user.number,
  			password : user.password,
  			confPassword : user.confPassword,
  			company : user.company,
        referralCode : user.referralCode,
        phcode : user.phcode,
        address: user.address,
        alternateaddress: user.alternateaddress,
        country : user.country,
        state : user.state,
        city : user.city,
        unit: user.unit
  
  		}

  		return this.http.post(this.rootUrl+'signup', data,this.httpOptions);
  	}

    updateUser (user : any) {
      const data = {
        title : user.title,
        firstName : user.firstName,
        lastName : user.lastName,
        company: user.company,
        oldPassword: user.oldPassword,
        newPassword : user.newPassword,
        newRepeatPassword : user.newRepeatPassword,
        dateOfBirth : user.dateOfBirth,
        newsletterSubscribe : user.newsletterSubscribe,
      }

      return this.http.post(this.rootUrl+'updateuser/'+this.userId,data,this.httpOptions);
    }

    login(data: any): Observable<any> {
      localStorage.clear();
      const url = this.rootUrl+'signin';
      return this.http.post(url, data);
    }

    loginforced(email, password): Observable<any> {
      localStorage.clear();
      const url = this.rootUrl+'userloginforce';
      const data = {email:email, password:password};
      return this.http.post(url, data);
    }

    checkActivationLink(data: any){
      const url = this.rootUrl+'activateuser';
      return this.http.post(url,data,this.httpOptions);

    }

    logout(){
        //console.log('reg serv');
        const url = this.rootUrl+'signout';
        const data = {id:this.userId};
        return this.http.post(url,data,this.httpOptions);
    }

    userShippingAddressLogin(type : string,userId) {
      const url = this.rootUrl+'usershippingaddress/'+type;
      const data = {id:userId};
      return this.http.post(url,data,this.httpOptions);
    }

    userShippingAddress(type : string) {
      const url = this.rootUrl+'usershippingaddress/'+type;
      const data = {id:this.userId};
      return this.http.post(url,data,this.httpOptions);
    }

    userSelectedShippingAddress(addressId){
      const url = this.rootUrl+'usershippingaddress';
      const data = {id:this.userId,addressId:addressId};
      return this.http.post(url,data,this.httpOptions);    
    }

    userShipmentsetting()
    {
        const url = this.rootUrl+'usershipmentsetting';
        const data = {id:this.userId};
        return this.http.post(url,data,this.httpOptions);
    }

    userTermssetting()
    {
        const url = this.rootUrl+'usertermssetting';
        const data = {id:this.userId};
        return this.http.post(url,data,this.httpOptions);
    }

    saveUserShippingAddress(user: any,addressbookId:number, addressType : string,postAction : string,isdCodes)
    {
      const data = {
          userid : this.userId,
          title : user.title,
          firstName : user.firstName,
          lastName : user.lastName,
          email : user.email,
          address : user.address,
          alternateAddress : user.alternateAddress,
          country : user.country,
          state : user.state,
          city : user.city,
          zipcode : user.zipcode,
          phCode : isdCodes.phCode,
          phone : user.phone,
          altphCode : isdCodes.altphCode,
          alternatePhone : user.alternatePhone,
          addressType : '',
          locationId : user.locationId,
          unit : user.unit,

          postAction : postAction
      }

      return this.http.post(this.rootUrl+'saveshippingaddress/'+this.userId+'/'+addressbookId,data,this.httpOptions);
    }

    setDefaultShippingAddress(addressbookId : number,type :string) {

      return this.http.get(this.rootUrl+'setdefaultshipping/'+this.userId+'/'+addressbookId+'/'+type,this.httpOptions);
    }

    uploadFile(fileToUpload: File):any{

      const formData = new FormData();

      formData.append('fileItem', fileToUpload, fileToUpload.name);

      return this.http.post(this.rootUrl+'updateprofileimage/'+this.userId,formData);
    }

    saveShipmentSettings(userSettings : UserShipmentSettings, userType)
    {
      const data = {
        settings : userSettings,
        type : userType
      }
      return this.http.post(this.rootUrl+'saveshipmentsettings/'+this.userId,data,this.httpOptions);
    }

    getShipmentSettings() {

      return this.http.get(this.rootUrl+'getshipmentsettings/'+this.userId,this.httpOptions);
    }

    sendInivitation(formData) {

      return this.http.post(this.rootUrl+'sendreferralinviation/'+this.userId,formData,this.httpOptions)
    }

    getUserCartList(data) {
      return this.http.post(this.rootUrl+'getusercartlist/',data,this.httpOptions)
    }

    updateUserCart(formData,type=''){
      return this.http.post(this.rootUrl+'updateusercart/'+ this.userId + '/' +type, formData)
    }

    savecartlist(data){
      return this.http.post(this.rootUrl+'savecartlist/'+ this.userId,data,this.httpOptions)
    }

    submitprocurementdata(formData) {
      return this.http.post(this.rootUrl+'submitprocurementdata/'+ this.userId,formData,this.httpOptions)
    }
    
    removeCartItem(id) {
      const data = {id:id};

      return this.http.post(this.rootUrl+'removecartitem/',data,this.httpOptions)
    }

    getPaymentHistory(data){
      return this.http.post(this.rootUrl+'getpaymenthistory/',data,this.httpOptions);
    }

    getInvoiceList(data){
      return this.http.post(this.rootUrl+'getinvoicelist/',data,this.httpOptions);
    }

    getInvoiceDetail(data) {
      return this.http.post(this.rootUrl+'getinvoicedetail/',data,this.httpOptions);
    }


    getPaymentMethodList(data) {
       return this.http.post(this.rootUrl+'getpaymentmethodbyuser/',data);
    }

    
    validateEwallet(formData) {
      return this.http.post(this.rootUrl+'validateewallet/',formData,this.httpOptions);
    }

    payinvoice(formData) {
      return this.http.post(this.rootUrl+'payinvoice/',formData,this.httpOptions);
    }

    saveSignature(formData)
    {
      return this.http.post(this.rootUrl+'saveSignature/'+this.userId,formData,this.httpOptions);
    }

    virtualTouFinished() {
      const data = {userId:this.userId};
      return this.http.post(this.rootUrl+'virtualtourfinished',data,this.httpOptions);
    }

    updateVirtualtourlog(logType,section,finished) {
      const data = {userId:this.userId,logType:logType,section:section,isFinished:finished};
      return this.http.post(this.rootUrl+'updatevirtualtourlog',data,this.httpOptions);
    }

    checkVirtualtourFinished(section) {
      const data = {userId:this.userId,section:section};
      return this.http.post(this.rootUrl+'checkvirtualtourfinished',data,this.httpOptions);
    }

    checkvirtualtourvisited(section) {
      const data = {userId:this.userId,section:section};
      return this.http.post(this.rootUrl+'checkvirtualtourvisited',data,this.httpOptions);
    }

    loginstatuscheck(userId) {
      const data = {userId:userId};
      return this.http.post(this.rootUrl+'checkuserloginstatus/',data,this.httpOptions);
    }

    resendActivation(userId) {
      const data = {userId:userId};
      return this.http.post(this.rootUrl+'resendactivationmail/',data);
    }

    sendOtpForModifyShippingAddress(userId, adressbookId, billingAddress){
      const data = {userId:userId, adressbookId:adressbookId, billingAddress:billingAddress};
      return this.http.post(this.rootUrl+'sendotpforshippingaddress/',data);
    }

    checkotpforshippingaddress(userId, otp){
      const data = {userId:userId, otp:otp};
      return this.http.post(this.rootUrl+'checkotpforshippingaddress/',data);
    }

    reSendOtpForModifyShippingAddress(userId, adressbookId){
      const data = {userId:userId, adressbookId:adressbookId};
      return this.http.post(this.rootUrl+'resendotpforshippingaddress/',data);
    }

    defaultcurrencycode() {
      return this.http.get(this.rootUrl+'getdefaultcurrencycode');
    }

    payreturn(formData)
    {
      return this.http.post(this.rootUrl+'payreturn/',formData,this.httpOptions);
    }

    subscriptionpayment(formData) {
      return this.http.post(this.rootUrl+'subscriptionpayment/',formData,this.httpOptions);
    }
    
    getusersubscriptiondetail(id) {
        const data = {userId : id};
        return this.http.post(this.rootUrl+'getusersubscriptiondetail/',data,this.httpOptions);
    }

    storeTempData(tempData,cartDataSection,paymentMethod) {
      const data = {
        tempData : tempData,
        userId : this.userId,
        cartDataSection : cartDataSection,
        paymentMethod : paymentMethod,
      }

      return this.http.post(this.rootUrl+'savetempcartdata',data,this.httpOptions);
    }
  
}
