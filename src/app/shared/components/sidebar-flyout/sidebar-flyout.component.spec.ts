import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarFlyoutComponent } from './sidebar-flyout.component';

describe('SidebarFlyoutComponent', () => {
	let component: SidebarFlyoutComponent;
	let fixture: ComponentFixture<SidebarFlyoutComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [SidebarFlyoutComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(SidebarFlyoutComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
