import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgForm } from '@angular/forms';
import { ContentService } from '../../../services/content.service';
import { RegistrationService } from '../../../services/registration.service';
import { UserAddress } from '../../../shared/models/useraddress.model';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';


@Component({
	selector: 'app-staticinfo-modal',
	templateUrl: './staticinfo-modal.component.html',
	styleUrls: ['./staticinfo-modal.component.scss']
})
export class StaticinfoModalComponent implements OnInit {
	title: string;
	closeBtnName: string;
	list: any[] = [];

	constructor(
		public bsModalRef: BsModalRef,
		private content:ContentService,
		private regService:RegistrationService,
		public toastr : ToastrService,
		private router: Router,
		) { }

	ngOnInit() {
	}

	proceedToNext() {

		let userData = JSON.parse(localStorage.getItem('tokens'));
		userData.user.virtualTourVisited = "1";
		localStorage.setItem('tokens', JSON.stringify(userData));
		this.regService.item.user.virtualTourVisited = "1";
		this.regService.virtualTouFinished().subscribe((data:any) =>{
			this.bsModalRef.hide();
			window.location.reload();
		});
	}
}
