import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddressbookModalComponent } from './addressbook-modal.component';

describe('AddressbookModalComponent', () => {
	let component: AddressbookModalComponent;
	let fixture: ComponentFixture<AddressbookModalComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [AddressbookModalComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(AddressbookModalComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
