import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReweighModalComponent } from './reweigh-modal.component';

describe('ReweighModalComponent', () => {
	let component: ReweighModalComponent;
	let fixture: ComponentFixture<ReweighModalComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ReweighModalComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ReweighModalComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
