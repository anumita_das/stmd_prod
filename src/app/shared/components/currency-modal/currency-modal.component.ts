import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgForm }   from '@angular/forms';
import { RegistrationService } from '../../../services/registration.service';
import { ContentService } from '../../../services/content.service';
import { ShopformeService } from '../../../services/shopforme.service';
import { AutoService } from '../../../shared/services/auto.service';
import { MywarehouseService } from '../../../services/mywarehouse.service';
import { FillshipService } from '../../../services/fillship.service';
import {Router} from "@angular/router";

@Component({
	selector: 'app-currency-modal',
	templateUrl: './currency-modal.component.html',
	styleUrls: ['./currency-modal.component.scss'],
})
export class CurrencyModalComponent implements OnInit {

	title: string;
	closeBtnName: string;
	currencyList  : Array<any>;
	selectedCurrency : any;

	constructor(
		public bsModalRef: BsModalRef,
		public userInfo : RegistrationService,
		public content : ContentService,
		private shopformeService :  ShopformeService,
		private fillshipService :  FillshipService,
		private router: Router,
		private autoService : AutoService,
		private mywarehouseService : MywarehouseService,
		) { }

	ngOnInit() {
		this.selectedCurrency = '';
	}

		
	currencyChange(event) {
		const routeUrl = this.router.url.split('/');

		if(routeUrl[1] == 'shop-for-me') {
			const usercart = JSON.parse(localStorage.getItem('userShopForMeCart'));

			const formData = {
			  fromCurrency : usercart.currencyCode,
		  	  toCurrency : event.target.value,
		  	  usercart : usercart,
		  	 };

			this.shopformeService.updateUserCartData(formData).subscribe((response:any) => {
				const usercart = JSON.parse(localStorage.getItem('userShopForMeCart'));

				//Reassign Storage Data
				usercart['currencyCode'] = response.procurement.currencyCode;
				usercart['currencySymbol'] = response.procurement.currencySymbol;
				usercart['defaultCurrencyCode'] = response.procurement.defaultCurrencyCode;
				usercart['defaultCurrencySymbol'] = response.procurement.defaultCurrencySymbol;
				usercart['isCurrencyChanged'] = response.procurement.isCurrencyChanged;
				usercart['exchangeRate'] = response.procurement.exchangeRate;
				usercart['items'] = response.procurement.items;
	    		usercart['data'] = response.procurement.data;

	   			localStorage.setItem('userShopForMeCart', JSON.stringify(usercart));

		 	    window.location.reload(true);
			});		
		}

		else if(routeUrl[1] == 'auto') {
			const usercart = JSON.parse(localStorage.getItem('userAutoCart'));
			const formData = {
			  fromCurrency : usercart.defaultCurrency,
		  	  toCurrency : event.target.value,
		  	  usercart : usercart,
		  	  userId : this.userInfo.userId,
		  	 };

		  	 this.autoService.updateUserCartData(formData).subscribe((data:any) => {
				if(data.status == '1')
				{
					localStorage.setItem('userAutoCart', '');
			 	    localStorage.setItem('userAutoCart', JSON.stringify(data.results));
			 	    window.location.reload(true);
			 	}
			});		
		}

		else if(routeUrl[1] == 'auto-parts') {
		const usercart = JSON.parse(localStorage.getItem('cartData'));

			const formData = {
			  fromCurrency : usercart.currencyCode,
		  	  toCurrency : event.target.value,
		  	  usercart : usercart,
		  	 };

			this.shopformeService.updateUserCartAutoPartsData(formData).subscribe((response:any) => {
				const usercart = JSON.parse(localStorage.getItem('cartData'));
				//Reassign Storage Data
				usercart['items'] = response.procurement.items;
	    		usercart['data'] = response.procurement.data;
				usercart['currencyCode'] = response.procurement.currencyCode;
				usercart['currencySymbol'] = response.procurement.currencySymbol;
				usercart['defaultCurrencyCode'] = response.procurement.defaultCurrencyCode;
				usercart['defaultCurrencySymbol'] = response.procurement.defaultCurrencySymbol;
				usercart['isCurrencyChanged'] = response.procurement.isCurrencyChanged;
				usercart['exchangeRate'] = response.procurement.exchangeRate;				
		 	    localStorage.setItem('cartData', JSON.stringify(usercart));
		 	    window.location.reload(true);
			});		
		} else if(routeUrl[1] == 'my-warehouse'){
			const usercart = JSON.parse(localStorage.getItem('userWarehouseCart'));

			const formData = {
			  fromCurrency : usercart.currencyCode,
		  	  toCurrency : event.target.value,
		  	  usercart : usercart,
		  	 };


			this.mywarehouseService.updateUserWarehouseData(formData).subscribe((response:any) => {
				const usercart = JSON.parse(localStorage.getItem('userWarehouseCart'));
				
				//Reassign Storage Data
				usercart['currencyCode'] = response.shipment.currencyCode;
				usercart['currencySymbol'] = response.shipment.currencySymbol;
				usercart['defaultCurrencyCode'] = response.shipment.defaultCurrencyCode;
				usercart['defaultCurrencySymbol'] = response.shipment.defaultCurrencySymbol;
				usercart['isCurrencyChanged'] = response.shipment.isCurrencyChanged;
	    		usercart['exchangeRate'] = response.shipment.exchangeRate;
			
		 	    localStorage.setItem('userWarehouseCart', JSON.stringify(usercart));
		 	    window.location.reload(true);
			});		

		}else if(routeUrl[1] == 'fill-and-ship'){
			const usercart = JSON.parse(localStorage.getItem('userFillShipCart'));

			const formData = {
			  fromCurrency : usercart.currencyCode,
		  	  toCurrency : event.target.value,
		  	  usercart : usercart,
		  	 };

			this.fillshipService.updateFillShipData(formData).subscribe((response:any) => {
				const usercart = JSON.parse(localStorage.getItem('userFillShipCart'));
				
				//Reassign Storage Data
				usercart['currencyCode'] = response.fillship.currencyCode;
				usercart['currencySymbol'] = response.fillship.currencySymbol;
				usercart['defaultCurrencyCode'] = response.fillship.defaultCurrencyCode;
				usercart['defaultCurrencySymbol'] = response.fillship.defaultCurrencySymbol;
				usercart['isCurrencyChanged'] = response.fillship.isCurrencyChanged;
	    		usercart['exchangeRate'] = response.fillship.exchangeRate;
			
		 	    localStorage.setItem('userFillShipCart', JSON.stringify(usercart));
		 	     window.location.reload(true);
			});		

		}



	}

}
