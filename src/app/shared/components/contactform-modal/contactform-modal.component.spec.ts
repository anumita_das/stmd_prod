import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuotePopupModalComponent } from './quote-popup-modal.component';

describe('QuotePopupModalComponent', () => {
	let component: QuotePopupModalComponent;
	let fixture: ComponentFixture<QuotePopupModalComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [QuotePopupModalComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(QuotePopupModalComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
