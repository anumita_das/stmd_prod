import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { AutoService } from '../../services/auto.service';
import { ContentService } from '../../../services/content.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgForm }   from '@angular/forms';


@Component({
	selector: 'app-contactform-modal',
	templateUrl: './contactform-modal.component.html',
	styleUrls: ['./contactform-modal.component.scss']
})
export class ContactFormModalComponent implements OnInit {

	title: string;
	closeBtnName: string;
	firstName: any;
	lastName: any;
	contactno:any;
	email: any;	
	userId: any;
	formSubmitted : boolean;
	selectedFile :any = [];

	constructor(
		public bsModalRef: BsModalRef,
		public auto : AutoService,
		public content : ContentService,
		public router : Router,
		public toastr : ToastrService,
		public spinner : NgxSpinnerService,
	) { }

	ngOnInit() {
		
	}

	onSubmit(form : NgForm) {
		let formData = new FormData();
		this.spinner.show();
		this.formSubmitted = true;

		if(this.selectedFile != null)
		{
			formData.append('fileName', this.selectedFile, this.selectedFile.name);
		}
		
      	for(let eachField in form.value)
      	{

      		formData.append(eachField,form.value[eachField]);
      		
      	}		
		
	      	
		this.content.saveContact(formData).subscribe((data:any) => {
			if(data.status == '1'){
				this.toastr.success("Request has been successfully submited.");
				this.bsModalRef.hide();
				this.router.navigate(['my-warehouse','overview']);
			}

			setTimeout(() => {
			        /** spinner ends after 3 seconds */
			        this.spinner.hide();
			    }, 2000);
		});
		
	}

	fileUpload(event) {

			let fileItem = event.target.files[0];
			const file: File = fileItem;
			this.selectedFile = file;
	
		
	}
}
