import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { AutoService } from '../../services/auto.service';
import { RegistrationService } from '../../../services/registration.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { ContentService } from '../../../services/content.service';
import { ShopformeService } from '../../../services/shopforme.service';
import { MywarehouseService } from '../../../services/mywarehouse.service';
import { FillshipService } from '../../../services/fillship.service';

@Component({
	selector: 'app-paystack-modal',
	templateUrl: './paystack-modal.component.html',
	styleUrls: ['./paystack-modal.component.scss']
})
export class PaystackModalComponent implements OnInit {

	title: string;
	closeBtnName: string;
	list: any[] = [];
	autoCart : any = '';
	paystackRef : any = '';
	userId : any = '';
	warehouseId : any = '';
	userEmail : string = '';
	payAmount : any = '';
	key : any = '';
	userCart : any = '';
	invoiceData : any;
	routeUrl : any;

	constructor(
		private shopformeService :  ShopformeService,
		public bsModalRef: BsModalRef,
		public auto : AutoService,
		public regService : RegistrationService,
		public router : Router,
		public toastr : ToastrService,
		public spinner : NgxSpinnerService,
		public content : ContentService,
		public mywarehouseService : MywarehouseService,
		public fillshipService : FillshipService,
	) { }

	ngOnInit() {
            this.userId = this.regService.userId;

            if("registrationdetail" in localStorage) {
                 let registrationDetails = JSON.parse(localStorage.getItem('registrationdetail'));
                 this.userEmail = registrationDetails.email;
          }  else
                this.userEmail = this.regService.item.user.email;

            const routeUrl = this.router.url.split('/');

            this.content.getPaystackSettings().subscribe((data:any) => {		
                    this.key = data.results.key;
            });		
	}

	ngAfterViewInit() {
		this.getPaystackReference(100000,999999);
	}

	paystackPaymentDone(event) {
		if(event.status == 'success')
		{
			this.spinner.show();
			const routeUrl = this.router.url.split('/');
			if(routeUrl[1] == 'auto')
			{
				this.autoCart = JSON.parse(localStorage.getItem('userAutoCart'));
				this.autoCart['paystackData'] = event;
				this.autoCart['paystackCreatedReference'] = this.paystackRef;
	 			if(this.autoCart.type == 'ship_my_car')
				{
					this.autoCart['userId'] = this.regService.userId;
					this.autoCart['warehouseId'] = this.warehouseId;
					this.auto.saveShipMyCar(this.autoCart).subscribe((data:any) => {
						this.spinner.hide();
						this.bsModalRef.hide();
						if(data.status == '1')
						{
							this.toastr.success("Data successfully submited.");
							localStorage.removeItem('userAutoCart');
							this.router.navigate(['auto','my-warehouse']);
						}
						else if(data.status == '-1')
							this.toastr.error("Something went wrong");
						else if(data.status == '-2')
							this.toastr.error("Payment Failed");

					});
				}
				else
				{
					this.auto.saveBuycarData(this.autoCart, this.warehouseId,this.userId).subscribe((data:any) => {
						this.spinner.hide();
						this.bsModalRef.hide();
						if(data.status == '1')
						{
							this.toastr.success("Data successfully submited.");
							localStorage.removeItem('userAutoCart');
							this.router.navigate(['auto','car-i-have-bought']);
						}
						else if(data.status == '-1')
							this.toastr.error("Something went wrong");
						else if(data.status == '-2')
							this.toastr.error("Payment Failed");
					});
				}
			} else if(routeUrl[1] == 'shop-for-me')
			{
				this.userCart = JSON.parse(localStorage.getItem('userShopForMeCart'));
				this.userCart['paystackData'] = event;
				this.userCart['paystackCreatedReference'] = this.paystackRef;
				this.userCart['userId'] = this.regService.userId;
				this.userCart['warehouseId'] = this.warehouseId;

				this.shopformeService.submitOrder(this.userCart).subscribe((data:any) => {
					this.spinner.hide();
					this.bsModalRef.hide();
					if(data.status == '1')
					{
						this.toastr.success('Your order has been placed successfully!!');
						localStorage.removeItem('userShopForMeCart');
						this.router.navigate(['shop-for-me','order-history']);
					} else {
			             	localStorage.removeItem('userWarehouseCart');  

			             	if(data.results == 'payment_failed')
			             		this.toastr.error(data.msg);
			             	else
			             		this.toastr.error('There seems to be some while processing the order. Please contact site admin.');	

			             	setTimeout(() => {
						        /** spinner ends after 3 seconds */
						        this.spinner.hide();
						    }, 3000);
						    this.router.navigate(['my-warehouse','overview']);   
			          }
				});

			} 
			else if(routeUrl[1] == 'auto-parts')
			{
				
				this.userCart = JSON.parse(localStorage.getItem('cartData'));
				this.userCart['paystackData'] = event;
				this.userCart['paystackCreatedReference'] = this.paystackRef;
	 			
					this.userCart['userId'] = this.regService.userId;
					this.userCart['warehouseId'] = this.warehouseId;
					this.shopformeService.placeOrder(this.userCart).subscribe((data:any) => {
						this.spinner.hide();
						this.bsModalRef.hide();
						if(data.status == '1')
						{
							this.toastr.success("Data successfully submited.");
							localStorage.removeItem('cartData');
							this.router.navigate(['auto-parts','order-history']);
						}
						else if(data.status == '-1')
							this.toastr.error("Something went wrong");
						else if(data.status == '-2')
							this.toastr.error("Payment Failed");

					});

			} else if(routeUrl[1] == 'my-warehouse')
			{			
				if(routeUrl[2] == "payment")
				{
					const returnData = JSON.parse(localStorage.getItem('returnItemDetails'));
					let paymentMethodKey = {"paymentMethodKey" : "paystack_checkout"};
					this.userCart = {"returnData" : returnData, "userId" : this.regService.userId, "paystackData" : event, "paystackCreatedReference":this.paystackRef, "paymentMethod":paymentMethodKey};

						//this.userCart['paystackData'] = event;
						//this.userCart['paystackCreatedReference'] = this.paystackRef;
			 			
							//this.userCart['userId'] = this.regService.userId;
							this.regService.payreturn(this.userCart).subscribe((data:any) => {
								this.spinner.hide();
								this.bsModalRef.hide();
								if(data.status == '1')
								{
									this.toastr.success("Return payment proccessed successfully.");
									this.spinner.show();
									this.router.navigate(['my-warehouse','overview']);
								} else {
					             	localStorage.removeItem('returnItemDetails');  

					             	if(data.results == 'payment_failed')
					             		this.toastr.error(data.msg);
					             	else
					             		this.toastr.error('There seems to be some while processing the order. Please contact site admin.');	

					             	setTimeout(() => {
								        /** spinner ends after 3 seconds */
								        this.spinner.hide();
								    }, 3000);
								    this.router.navigate(['my-warehouse','overview']);   
					             }

							});

				}else{

						this.userCart = JSON.parse(localStorage.getItem('userWarehouseCart'));
						this.userCart['paystackData'] = event;
						this.userCart['paystackCreatedReference'] = this.paystackRef;
			 			
							this.userCart['userId'] = this.regService.userId;
							this.mywarehouseService.submitOrder(this.userCart).subscribe((data:any) => {
								this.spinner.hide();
								this.bsModalRef.hide();
								if(data.status == '1')
								{
									this.toastr.success("Data successfully submited.");
									localStorage.removeItem('userWarehouseCart');
									this.router.navigate(['my-account','order-history']);
								} else {
					             	localStorage.removeItem('userWarehouseCart');  

					             	if(data.results == 'payment_failed')
					             		this.toastr.error(data.msg);
					             	else
					             		this.toastr.error('There seems to be some while processing the order. Please contact site admin.');	

					             	setTimeout(() => {
								        /** spinner ends after 3 seconds */
								        this.spinner.hide();
								    }, 3000);
								    this.router.navigate(['my-warehouse','overview']);   
					             }

							});

				}
			

			} else if(routeUrl[1] == 'fill-and-ship' && routeUrl[2] != 'extracost')
			{				
				this.userCart = JSON.parse(localStorage.getItem('userFillShipCart'));
				this.userCart['paystackData'] = event;
				this.userCart['paystackCreatedReference'] = this.paystackRef;
	 			
					this.userCart['userId'] = this.regService.userId;
					this.fillshipService.submitOrder(this.userCart).subscribe((data:any) => {
						this.spinner.hide();
						this.bsModalRef.hide();
						if(data.status == '1')
						{
							this.toastr.success("Your order has been placed successfully!!");
							localStorage.removeItem('userFillShipCart');
							this.router.navigate(['fill-and-ship','overview']);
						} else {
			             	localStorage.removeItem('userFillShipCart');  

			             	if(data.results == 'payment_failed')
			             		this.toastr.error(data.msg);
			             	else
			             		this.toastr.error('There seems to be some while processing the order. Please contact site admin.');	

			             	setTimeout(() => {
						        /** spinner ends after 3 seconds */
						        this.spinner.hide();
						    }, 3000);
						    this.router.navigate(['fill-and-ship','order-form']);
			             }

					});

			}else if(routeUrl[1] == 'fill-and-ship' && routeUrl[2] == 'extracost') {

				this.invoiceData['paystackData'] = event;
				this.invoiceData['paystackCreatedReference'] = this.paystackRef;
				this.fillshipService.payextracharge(this.invoiceData).subscribe((response:any) => {
					this.spinner.hide();
					this.bsModalRef.hide();
				if(response.status == 1){
					localStorage.removeItem('extraCostPaidAmount');
					this.router.navigate(['fill-and-ship','overview']);
					this.toastr.success("Extracharge payment proccessed successfully.");
				}else if(response.status == '-1') {
					this.toastr.error(response.results);
				} 
				else {
					//window.location.reload();
					this.toastr.error("Unable to proccess request. Please try again.");
				}
			});
			}
			 else if(routeUrl[1] == 'payment')
			{
                            this.invoiceData['paystackData'] = event;
                            this.invoiceData['paystackCreatedReference'] = this.paystackRef;
                            this.regService.payinvoice(this.invoiceData).subscribe	((data:any) => {
                                this.spinner.hide();
                                this.bsModalRef.hide();
                                if(data.status == '1')
                                {
                                        this.toastr.success("Invoice payment proccessed successfully.");
                                        this.router.navigate(['payment']);
                                }
                                else if(data.status == '-1') {
                                        this.toastr.error(data.results);
                                }
                                 else {
                                        //window.location.reload();
                                        this.toastr.error("Unable to proccess request. Please try again.");
                                }

                         });
			} else if(routeUrl[1] == 'join' && routeUrl[2] == 'payment'){
                            this.userCart = JSON.parse(localStorage.getItem('subscriptionFormData'));
                            console.log(this.userCart);
                            this.userCart['paystackData'] =  event;
                            this.userCart['paystackCreatedReference'] = this.paystackRef;
                            this.regService.subscriptionpayment(this.userCart).subscribe((data:any) => {
                                this.spinner.hide();
                                this.bsModalRef.hide();
                                if(data.status == '1')
                                {
                                    localStorage.removeItem('registrationdetail');
                                    localStorage.removeItem('subscribedetail');
                                    localStorage.setItem('paymentsuccess',  JSON.stringify({'paymentsuccess' : true}));
                                    this.router.navigate(['join']);
                                }
                                else if(data.status == '-1') {
                                        this.toastr.error(data.results);
                                }
                                 else {
                                        //window.location.reload();
                                        this.toastr.error("Unable to proccess request. Please try again.");
                                }

                          });
                        } 

		}
		else
		{
			this.toastr.error("Payment Failed");
			this.bsModalRef.hide();
		}

	}

	getPaystackReference(max,min) {
    	let random = Math.floor(Math.random() * (max - min + 1)) + min;
    	this.paystackRef = random;
    	let routeUrl =  this.router.url.split('/');
    	let paymentUrl = '';
    	let paymentData = '';
    	if(routeUrl[1] == 'auto')
    	{
    		this.autoCart = JSON.parse(localStorage.getItem('userAutoCart'));
    		paymentUrl = this.autoCart.type;
    	}
    	else if(routeUrl[1] == 'my-warehouse' && routeUrl[2] == 'payment')
    		paymentUrl = routeUrl[1]+'-'+routeUrl[2];
    	else
    		paymentUrl = routeUrl[1];

    	if(routeUrl[1] == 'auto')
		{
			paymentData = localStorage.getItem('userAutoCart');
		}
		else if(routeUrl[1] == 'shop-for-me')
		{
			paymentData = localStorage.getItem('userShopForMeCart');
		}
		else if(routeUrl[1] == 'auto-parts')
		{		
			paymentData = localStorage.getItem('cartData');
		}
		else if(routeUrl[1] == 'my-warehouse' && routeUrl[2] != 'payment')
		{				
			paymentData = localStorage.getItem('userWarehouseCart');
		}
		else if(routeUrl[1] == 'my-warehouse' && routeUrl[2] == 'payment')
		{
			const returnData = localStorage.getItem('returnItemDetails');
			let paymentMethodKey = {"paymentMethodKey" : "paystack_checkout"};
			this.userCart = {"returnData" : returnData, "userId" : this.regService.userId, "paystackData" : event, "paystackCreatedReference":this.paystackRef, "paymentMethod":paymentMethodKey};
			paymentData = this.userCart;
		}
		else if(routeUrl[1] == 'fill-and-ship' && routeUrl[2] != 'extracost')
		{				
			paymentData = localStorage.getItem('userFillShipCart');
		}
		else if(routeUrl[1] == 'fill-and-ship' && routeUrl[2] == 'extracost')
		{				
			paymentData = JSON.stringify(this.invoiceData);
		}
		else if(routeUrl[1] == 'payment')
		{
			paymentData = localStorage.getItem('extraInvoiceData');
		}
        else if(routeUrl[1] == 'join' && routeUrl[2] == 'payment')
		{

			paymentData = localStorage.getItem('subscriptionFormData');
		}
		else if(routeUrl[1] == 'my-account' && routeUrl[2] == 'payment')
		{

			paymentData = localStorage.getItem('subscriptionArray');
		}
    	
                if(paymentData!="")
                {
                        this.content.savePaystackReference(this.paystackRef,paymentData,paymentUrl).subscribe((data:any) => {

                        });
                }
  	}

  	paymentCancel() {
  		this.bsModalRef.hide();
  	}
}
