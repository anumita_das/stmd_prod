import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { MywarehouseService } from '../../../services/mywarehouse.service';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-wronginventory-modal',
	templateUrl: './wronginventory-modal.component.html',
	styleUrls: ['./wronginventory-modal.component.scss']
})
export class WronginventoryModalComponent implements OnInit {

	title: string;
	closeBtnName: string;
	deliveryId: any;
	shipmentId: any;
	wrongInventoryMessage: any;
	shipment: any;
	isWrongInventoryDisabled: any ='N';

	constructor(
		public bsModalRef: BsModalRef,
		public mywarehouseService : MywarehouseService,
		private toastr: ToastrService,
	) {}


	ngOnInit() {
		
	}

	sendWrongInventoryNotification(deliveryId, shipmentId){
		const formData = {deliveryId : deliveryId, shipmentId : shipmentId, wrongInventoryMessage : this.wrongInventoryMessage};
		this.mywarehouseService.sendwronginventorynotification(formData).subscribe((data:any) => {
			this.isWrongInventoryDisabled = 'Y';
			this.bsModalRef.hide();
			if(data.satus == 1)
				this.toastr.success("Email notification sent successfully to admin!!");
			else
				this.toastr.error("Failed to send email notification. Please try again!!");

		});
	}
}
