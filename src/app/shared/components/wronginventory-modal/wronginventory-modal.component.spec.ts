import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WronginventoryModalComponent } from './wronginventory-modal.component';

describe('WronginventoryModalComponent', () => {
	let component: WronginventoryModalComponent;
	let fixture: ComponentFixture<WronginventoryModalComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [WronginventoryModalComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WronginventoryModalComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
