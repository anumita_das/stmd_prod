import { Component, OnInit, HostListener, Renderer2 } from '@angular/core';
import { Router } from '@angular/router';
import { AppGlobals } from '../../../app.globals';
import { RegistrationService } from '../../../services/registration.service';
import { ContentService } from '../../../services/content.service';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import { ActivatedRoute } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { AddressbookModalComponent } from '../addressbook-modal/addressbook-modal.component';
import { StaticinfoModalComponent } from '../staticinfo-modal/staticinfo-modal.component';
import { ShipmentSettingsModalComponent } from '../shipment-settings/shipmentsettings-modal.component';
import { TermsModalComponent } from '../terms-modal/terms-modal.component';
import { NgxSpinnerService } from 'ngx-spinner';

const MINUTES_UNITL_AUTO_LOGOUT = 30; // in mins
const CHECK_INTERVAL = 15000; // in ms
const STORE_KEY =  'lastAction';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
	showAccountButtons: boolean;
	showMenuButtons: boolean;
	showHelpMenu:boolean = false;
	showSideNavButtons: boolean;
	showAccountDropdown: boolean;
	showNavigation: boolean;
	tourbuttonDisplay : boolean = false;
	url: any;
	text: string;
	phones: Array<object>;
	selectedItem: any;
	private _opened = false;
	isCollapsed = true;
	isSidenavCollapsed = true;
	contactEmail : string = '';
	contactNumber : any;
	currentpageRooturl : any = '';
	isMediumScreen: boolean;
	pageType : any = '';
	homepageAdBanner : any = "";
	homepageAdLink  : any = "";
	bsModalRef: BsModalRef;
	hideHeaderMenu : any = ['my-account','my-warehouse','auto-parts','shop-for-me','auto','payment','modify-account','addressbook','refer-a-friend','help'];
	onOpenChange(data: boolean): void {
		this.text = data ? 'opened' : 'closed';
	}
	constructor(
		private router: Router,
		private renderer: Renderer2,
		public auth : RegistrationService,
		public content : ContentService,
		private route: ActivatedRoute,
		private modalService: BsModalService,
		public _global: AppGlobals,
		public spinner : NgxSpinnerService,
	) { }

	ngOnInit() {
		const routeUrl = this.router.url.split('/');
		this.currentpageRooturl = routeUrl[1];
		if(this.auth.isLoggedIn && (this.auth.userId==null || this.auth.userId==''))
		{
			location.reload();
		}

		/* Take tour navigation */

		if(routeUrl[1]=='addressbook' || routeUrl[1]=='shop-for-me' || routeUrl[1]=='auto' || routeUrl[1]=='my-warehouse' || this.router.url=='/my-account/account-detail' || this.router.url=='/my-account/order-history')
		{
			this.tourbuttonDisplay = true;
			/*this.auth.checkVirtualtourFinished(routeUrl[1]).subscribe((data:any) => {
				if(data.status == '1' && data.results=='0')
				{
					this.tourbuttonDisplay = true;
				}
				else
				{
					this.tourbuttonDisplay = false;
				}
				
			});*/
		}
		else
		{
			this.tourbuttonDisplay = false;
		}

		//console.log(this.auth.item);
		//console.log(new Date().getTime());
		//console.log(new Date(this.auth.item.tokenExpires.date).getTime());
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

		this.checkWindowSize(width);
		/** Get Current Route */
		const i = this.router.url.indexOf('?');
		const index = (i > -1) ? i : this.router.url.length;
		this.url = this.router.url.substring(0, index);
		/** Disable Navigation */
		if (this.hideHeaderMenu.indexOf(this.currentpageRooturl)!='-1') {
			this.showNavigation = true;
			this.showMenuButtons = false;
			//this.showAccountButtons = false;
			//this.showSideNavButtons = false;
		} else {
			this.showNavigation = true;
			this.showMenuButtons = true;
			//this.showAccountButtons = true;
			//this.showSideNavButtons = true;
		}
		/** Disable Account Dropdown */
		if (this.url === '/' || this.url === '/login' || this.url === '/join') {
			this.showAccountDropdown = false;
		} else {
			this.showAccountDropdown = true;
		}
		/** Disable button */
		if (this.url === '/login' || this.url === '/join' || this.url === '/my-account') {
			this.showAccountButtons = false;
		} else {
			this.showAccountButtons = true;
		}
		

		/** Disable sidenav button */
		console.log(this.url);
		if (this.url === '/login' || this.url === '/join' || this.url === '/' || this.url === '/shop-from' || this.url.indexOf('#')!='-1') {
			this.showSideNavButtons = false;
		} else {
			this.showSideNavButtons = true;
			if(this.currentpageRooturl == 'help')
			{
				this.showHelpMenu = true;
			}
			else
			{
				this.showHelpMenu = false;
			}
		}


		/* Logout if inactive initialize */
		if(this.auth.item != null)
		{
			this.check();
		    this.initListener();
		    this.initInterval();
		    localStorage.setItem(STORE_KEY,Date.now().toString());
		    if(this.auth.item.user.virtualTourVisited && this.auth.item.user.virtualTourVisited=="0")
		    {
		    	if(this._global.virtualTourEntered == '0')
		    	{
		    		this.bsModalRef = this.modalService.show(StaticinfoModalComponent, 
						{
							backdrop  : 'static',
	   						keyboard  : false
	   					}
	   				);

		    		//this.router.navigate(['virtual-tour']);
		    	}
		    }
		    else if(this.auth.item.user.setDeliveryAddress &&  this.auth.item.user.setDeliveryAddress == 0)
		    {
		    	this.getUserAddressInfo();
		    }
		    else if(this.auth.item.user.setShipmentSettings && this.auth.item.user.setShipmentSettings=="0")
		    {
		    	this.getShipmentsettingsInfo();
		    }
		    else if(this.auth.item.user.aggreedToTerms && this.auth.item.user.aggreedToTerms=="0")
		    {
		    	this.getTermsInfo();
		    }
		   
		    
		}

		this.getContactInfo();
		this.getAdBanner();

		//fetch page type
		this.route.params.subscribe((params)=> {
		  //check lead Id here
		  if(params['page']){
		  console.log('here');

		   this.content.getpagetype(params['page']).subscribe((data:any) => {
				 this.pageType = data.pageType;
				 console.log(data.pageType);
				 if(this.pageType == 2){
				 	this.showAccountButtons = false;
					this.showMenuButtons = false;
					this.showSideNavButtons = false;
					this.showAccountDropdown  = false;
				 	this.showNavigation = false;
				 } else{
				 	this.showAccountButtons = true;
					this.showMenuButtons = true;
					this.showSideNavButtons = true;
					this.showAccountDropdown  = true;
					this.showNavigation = true;
				 }
			});
		  }
		});

		
		
	}

	getAdBanner() {
		this.content.getAdBanner().subscribe((data:any) =>{
			if(data.status == 1)
			{
				this.homepageAdBanner = data.results.bannerImage;
				this.homepageAdLink = data.results.pageLink;
			}
		})
	}

	getUserAddressInfo() 
	{
		this.auth.userShippingAddress('shipping').subscribe((data:any) =>{
			if(data.status == '-1')
			{
				if (document.getElementsByClassName("modal").length === 0) {
					this.bsModalRef = this.modalService.show(AddressbookModalComponent, 
						{
							backdrop  : 'static',
	   						keyboard  : false
	   					}
	   				);
				}
			}
		});
	}

	getShipmentsettingsInfo()
	{
		this.auth.userShipmentsetting().subscribe((data:any) =>{
			if(data.status == '-1')
			{
				if (document.getElementsByClassName("modal").length === 0) {
					this.bsModalRef = this.modalService.show(ShipmentSettingsModalComponent, 
						{
							class 	  : 'modal-lg',
							backdrop  : 'static',
	   						keyboard  :  false
	   					}
	   				);
				}
			}
		});
	}

	getTermsInfo()
	{
		this.auth.userTermssetting().subscribe((data:any) =>{
			if(data.status == '-1')
			{
				if (document.getElementsByClassName("modal").length === 0) {
					this.bsModalRef = this.modalService.show(TermsModalComponent, 
						{
							class 	  : 'modal-lg',
							backdrop  : 'static',
	   						keyboard  :  false
	   					}
	   				);
				}
			}
		});
	}

	getContactInfo(){
		this.content.contactInfo().subscribe((data:any) => {

			let info = JSON.parse(data.results);
			if(info.email)
				this.contactEmail=info.email;
			if(info.number)
			{
				this.phones = info.number;
			}
		})
	}

	logout() {
		//console.log('home comp');
		this.auth.logout().subscribe((data:any) => {
			if(data.status == '1')
			{
				localStorage.clear();
				this.auth.isLoggedIn = false;
				location.reload();
				//this.router.navigate(['login']);
			}
		});
	}

	public getLastAction() {
		//console.log(localStorage.getItem(STORE_KEY));
    	return parseInt(localStorage.getItem(STORE_KEY));
	}
	public setLastAction(lastAction: number) {
	    localStorage.setItem(STORE_KEY, lastAction.toString());
	}

	initListener() {
	    document.body.addEventListener('click', () => this.reset());
	    document.body.addEventListener('mouseover',()=> this.reset());
	    document.body.addEventListener('mouseout',() => this.reset());
	    document.body.addEventListener('keydown',() => this.reset());
	    document.body.addEventListener('keyup',() => this.reset());
	    document.body.addEventListener('keypress',() => this.reset());
  	}

  	reset() {
	    this.setLastAction(Date.now());
	}

  	initInterval() {
	    setInterval(() => {
	      this.check();
	    }, CHECK_INTERVAL);
	}

  	check() {
	    /*const now = Date.now();
	    const timeleft = this.getLastAction() + MINUTES_UNITL_AUTO_LOGOUT * 60 * 1000;
	    const diff = timeleft - now;
	    const isTimeout = diff < 0;
	    //console.log(diff);
	    if (isTimeout)  {
	    	localStorage.clear();
			this.auth.isLoggedIn = false;
			location.reload();
	    }*/
	}

	checkLoginExpiry() {

		/*if(this.auth.item!=null)
		{
			if(this.auth.item.tokenExpires!=undefined)
			{
				let expireTime = new Date(this.auth.item.tokenExpires.date).getTime();
				let now = new Date().getTime();
				if(now > expireTime)
				{
					localStorage.clear();
					this.auth.isLoggedIn = false;
					location.reload();
				}
			}
		}*/
	}


	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		//console.log('Width: ' + event.target.innerWidth);
		this.checkWindowSize(event.target.innerWidth);
	}

	openVirtualTour()
	{
		let currentUrl = this.router.url;
		console.log(currentUrl);
		localStorage.setItem('virtualTourResumeUrl', JSON.stringify(currentUrl));
		if(this.currentpageRooturl=='shop-for-me')
		{
			this.router.navigateByUrl('/virtual-tour/shop-for-me');
		}
		else if(this.currentpageRooturl == 'addressbook')
		{
			this.router.navigateByUrl('/virtual-tour/my-account/addressbook');
		}
		else if(this.currentpageRooturl == 'auto')
		{
			this.router.navigateByUrl('/virtual-tour/auto');
		}
		else if(this.currentpageRooturl == 'my-warehouse')
		{
			this.router.navigateByUrl('/virtual-tour/my-warehouse');
		}
		else if(currentUrl == '/my-account/account-detail')
		{
			this.router.navigateByUrl('/virtual-tour/my-account/account-detail');
		}
		else if(currentUrl == '/my-account/order-history')
		{
			this.router.navigateByUrl('/virtual-tour/my-account/order-history');
		}
	}

}
