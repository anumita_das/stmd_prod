import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoImagesModalComponent } from './auto-images-modal.component';

describe('AutoImagesModalComponent', () => {
	let component: AutoImagesModalComponent;
	let fixture: ComponentFixture<AutoImagesModalComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [AutoImagesModalComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(AutoImagesModalComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
