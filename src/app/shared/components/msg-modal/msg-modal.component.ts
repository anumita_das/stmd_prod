import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgForm }   from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { RegistrationService } from '../../../services/registration.service';
import { ContentService } from '../../../services/content.service';

@Component({
	selector: 'app-msg-modal',
	templateUrl: './msg-modal.component.html',
	styleUrls: ['./msg-modal.component.scss']
})
export class MsgModalComponent implements OnInit {

	userId : number;
	inputPassword:any;
	code:any;
	wcodePattern = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$_!%*?&-]{8,20}$"; 
	dataContent:any=[];
	constructor(
		public regService : RegistrationService,
		public bsModalRef: BsModalRef,
		public toastr : ToastrService,
		private content: ContentService,
		) {
			this.userId = this.regService.item.user.id;
		}

	ngOnInit() {

		const msgText =  localStorage.getItem('msgReply');
		this.dataContent.message = msgText;
		this.dataContent.subject = "File a Claim";
		if(this.dataContent.message == '')
		{
			this.content.getMessageContent(this.userId).subscribe((data:any) => { 

			this.dataContent = data.results;
			//console.log(this.dataContent);

		});
		}
		


	}



}
