import { Component, OnInit } from '@angular/core';
import { GetquoteService } from '../../services/getquote.service';
import { NgForm }   from '@angular/forms';
import { AppGlobals } from '../../../app.globals';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { QuotePopupModalComponent } from '../../../shared/components/quote-popup-modal/quote-popup-modal.component';

@Component({
	selector: 'app-get-a-quote',
	templateUrl: './get-a-quote.component.html',
	styleUrls: ['./get-a-quote.component.scss']
})
export class GetAQuoteComponent implements OnInit {
    
    bsModalRef: BsModalRef;
    showShipping: boolean;
    packageCount = 1;
    shippingTypes : any;
    currencySymbol : any = '';
    getQuoteImg : any;

	getquote =  {
			from : {
				countryId : "",
				stateId : "",
				cityId : "",
				stateName : "",
				cityName : "",
			}, 
			to : {
				countryId : "",
				stateId : "",
				cityId : "",
				stateName : "",
				cityName : "",
			}, 
			viewCurrency : "USD",
			packages : [{
				length : "",
				width : "",
				height : "",
				unit : "P",
				weight : "",
				declaredValue : "",
			}]
		};

	public countryList : any;
	public currencyList : any;
	public stateList : any;
	public cityList : any;
	public tostateList : any;
	public tocityList : any;
	public warehouseList : any;
	
	constructor(
		protected getquoteService : GetquoteService,
		protected _global: AppGlobals,
		protected spinner : NgxSpinnerService,
		protected toastr : ToastrService,
		protected modalService: BsModalService,
	) { }

	ngOnInit() {
	  this.getCountryList();
	  this.getCurrencyList();
	  this.getWarehouseList();
	  this.currencySymbol = this._global.defaultCurrencySymbol;

	  this.getQuoteImg="assets/imgs/car-get-a-quote.png";
	}

	getCountryList() {

		this.getquoteService.countrylist().subscribe((data:any) => {
				this.countryList = data;
		});
	}

	getCurrencyList() {

		this.getquoteService.currencylist().subscribe((data:any) => {
				this.currencyList = data;
		});
	}

	getWarehouseList() {

		this.getquoteService.warehouselist().subscribe((data:any) => {
				this.warehouseList = data;
		});
	}

	countryChanged(countryId){
		this.getquoteService.statecitylist(countryId).subscribe((data:any) => {
			if(data.stateList.length > 0)
				this.stateList = data.stateList;
			else
				this.stateList = false;
	
			if(data.cityList.length > 0)	
				this.cityList = data.cityList;
			else
				this.cityList = false;
		});
	}

	stateChanged(stateId){
		this.getquoteService.citylist(stateId).subscribe((data:any) => {
			if(data.cityList.length > 0)
				this.cityList = data.cityList;
			else
				this.cityList = false;
		});
	}

	tocountryChanged(countryId){
		this.getquoteService.statecitylist(countryId).subscribe((data:any) => {
				if(data.stateList.length > 0)
					this.tostateList = data.stateList;
				else
					this.tostateList = false;
				
				if(data.cityList.length > 0)
					this.tocityList = data.cityList;
				else
					this.tocityList = false;				
		});
	}

	tostateChanged(stateId){
		this.getquoteService.citylist(stateId).subscribe((data:any) => {
			if(data.cityList.length > 0)
				this.tocityList = data.cityList;
			else
	  			this.tocityList = false;				
		});
	}

	addPackage() {
		this.getquote.packages.push({
    		length : "",
			width : "",
			height : "",
			unit : "P",
			weight : "",
			declaredValue : "",
      });
      this.packageCount = this.packageCount+1;
	}

	// deletePackageRow
	deletePackageRow(index) {
		this.getquote.packages.splice(index, 1);
	}


	setTwoNumberDecimal($event) {
		if(!isNaN(parseFloat($event.target.value))){
    	 	$event.target.value = Math.abs(parseFloat($event.target.value)).toFixed(2);
		}
     	else {
     		$event.target.value = parseFloat("0").toFixed(2);
     	}
  	}

	onSubmit(form: any){
		//console.log(form);
		this.spinner.show();
		this.showShipping = false;
		this.getquoteService.submitquote(form).subscribe((data:any) => {
			this.bsModalRef = this.modalService.show(QuotePopupModalComponent,
				{
					backdrop  : 'static',
						keyboard  : false
					}
			);
			this.spinner.hide();
			if(data.status == '1')
			{
				this.bsModalRef.content.dataFound = true;
				this.showShipping = true;
				this.shippingTypes = data.results;
			}
			else
			{
				this.toastr.error(data.results);
			}
		});
	}


}
