import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetAQuoteItemComponent } from './get-a-quote-item.component';


describe('GetAQuoteItemComponent', () => {
	let component: GetAQuoteComponent;
	let fixture: ComponentFixture<GetAQuoteItemComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [GetAQuoteItemComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(GetAQuoteItemComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
