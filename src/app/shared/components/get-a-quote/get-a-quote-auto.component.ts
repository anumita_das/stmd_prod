import { Component, OnInit } from '@angular/core';
import { GetAQuoteComponent } from './get-a-quote.component';
import { GetquoteService } from '../../services/getquote.service';
import { AutoService } from '../../../shared/services/auto.service';
import { NgForm }   from '@angular/forms';
import { AppGlobals } from '../../../app.globals';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { QuotePopupModalComponent } from '../../../shared/components/quote-popup-modal/quote-popup-modal.component';

@Component({
	selector: 'app-get-a-quote-auto',
	templateUrl: './get-a-quote-auto.component.html',
	styleUrls: ['./get-a-quote.component.scss']
})

export class GetAQuoteAutoComponent extends GetAQuoteComponent  {
   
   bsModalRef: BsModalRef;
   countryList : any;
   destinationCountryList : any;
   yearList : any = [];
	getquoteauto =  {
		from : {
			countryId : "",
			stateId : "",
			cityId : "",
			stateName : "",
			cityName : "",
		}, 
		to : {
			countryId : "",
			stateId : "",
			cityId : "",
			stateName : "",
			cityName : "",
		}, 
		viewCurrency : "USD",
		warehouse : "",
		items : { 
			makeId : "",
			modelId : "",
			price : "",
		}
	};
	autoContactName : any;
	autoContactEmail : any;
	autoContactPhone : any;
	item : any = "";
	public makeList : any;
	public modelList : any;
	

	constructor(
		protected getquoteService : GetquoteService,
		protected _global: AppGlobals,
		protected spinner : NgxSpinnerService,
		protected toastr : ToastrService,
		public auto: AutoService,
		protected modalService: BsModalService,
	) { super(getquoteService,_global,spinner,toastr,modalService); }


	ngOnInit() {
		let currentdate = new Date();
	  this.getMakeList();
	  this.getAllCountry();
	  //super.getCountryList();
	  super.getCurrencyList();
	  super.getWarehouseList();
	  this.generateYearList(currentdate.getFullYear(),'1950');
	}

	generateYearList(max,min){
		for (var i=max; i>=min; i--) {
	      this.yearList.push(i);
	    }
	}

	countryChanged(countryId){
		super.countryChanged(countryId);
	}

	stateChanged(stateId){
		super.stateChanged(stateId);
	}

	tocountryChanged(countryId){
		super.tocountryChanged(countryId);
	}

	tostateChanged(stateId){
		super.tostateChanged(stateId);
	}

	getMakeList(){
		this.getquoteService.makelist().subscribe((data:any) => {
			this.makeList = data;
		});
	}

	makeChanged(makeId) {
		this.modelList = [];
		this.getquoteauto.items.modelId = "";

		this.getquoteService.modellist(makeId).subscribe((data:any) => {
			if(data.length > 0) 
				this.modelList = data;
		});
	}

	getAllCountry(){
		this.auto.allCountry().subscribe((data:any)=>{
			this.countryList = data;
		});

		this.auto.allDestinationCountry().subscribe((data:any) => {
			this.destinationCountryList = data;
		});
	}
	dataSubmit(form: any){
		this.spinner.show();
		if(!this.showShipping)
		{
			this.showShipping = false;
			this.getquoteService.getAutoQuote(form.value).subscribe((data:any) => {
				this.spinner.hide();
				this.bsModalRef = this.modalService.show(QuotePopupModalComponent,
				{
					backdrop  : 'static',
					keyboard  : false
				});

				if(data.status == 1)
				{
					this.bsModalRef.content.dataFound = true;
					this.showShipping = true;
					this.item = data.results;
					form.submitted = false;
				}
				else
				{
					this.toastr.error(data.results);
				}
			});
		}
		else
		{
			this.getquoteService.submitContact(form.value).subscribe((data:any) => {
				this.spinner.hide();
				if(data.status == 1)
				{
					this.toastr.success(data.results);
					setTimeout(() => {
						window.location.reload();
	          		}, 2000);
				}
				else
				{
					this.toastr.error(data.results);
				}
			});
		}
	}

}
