import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetAQuoteAutoComponent } from './get-a-quote-auto.component';


describe('GetAQuoteAutoComponent', () => {
	let component: GetAQuoteComponent;
	let fixture: ComponentFixture<GetAQuoteAutoComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [GetAQuoteAutoComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(GetAQuoteAutoComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
