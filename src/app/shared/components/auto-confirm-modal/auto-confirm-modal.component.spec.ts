import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoConfirmModalComponent } from './auto-confirm-modal.component';

describe('AutoConfirmModalComponent', () => {
	let component: AutoConfirmModalComponent;
	let fixture: ComponentFixture<AutoConfirmModalComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [AutoConfirmModalComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(AutoConfirmModalComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
