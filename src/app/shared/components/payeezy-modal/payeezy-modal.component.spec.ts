import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayeezyModalComponent } from './payeezy-modal.component';

describe('PayeezyModalComponent', () => {
	let component: PayeezyModalComponent;
	let fixture: ComponentFixture<PayeezyModalComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [PayeezyModalComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(PayeezyModalComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
