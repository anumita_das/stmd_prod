import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgForm } from '@angular/forms';
import { RegistrationService } from '../../../services/registration.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { MywarehouseService } from '../../../services/mywarehouse.service';
import { ShopformeService } from '../../../services/shopforme.service';
import { AutoService } from '../../services/auto.service';
import { FillshipService } from '../../../services/fillship.service';


@Component({
	selector: 'app-payeezy-modal',
	templateUrl: './payeezy-modal.component.html',
	styleUrls: ['./payeezy-modal.component.scss']
})
export class PayeezyModalComponent implements OnInit {
	title: string;
	closeBtnName: string;
	list: any[] = [];
	warehouseId : any;
	payAmount : any;
	paySymbol : any;
	amountDisplay : any;
	ccardNumber: any = '';
	expiryMonth: any = '';
	expiryYear: any = '';
	cvvCode: any = '';
	cardHolderName : any = '';
	payCardType: any = '';
	formSubmit:boolean = false;
	userCart : any = '';
	autoCart : any = '';
	invoiceData : any = [];
	allCardTypeIcons : any = {'VISA':'visa.svg'};
	cardTypeIcon : any = "";
	taxAmount : any = '';
	tempDataId : any = '';

	cardInvalid:  {
    	ccardNumber : boolean,
    	expiryMonth : boolean,
    	expiryYear : boolean,
    	cvvCode : boolean,
    	payCardType : boolean,
    	cardHolderName : boolean,
	};

	constructor(
		public bsModalRef: BsModalRef,
		private regService:RegistrationService,
		public toastr : ToastrService,
		private router: Router,
		public spinner : NgxSpinnerService,
		public mywarehouseService : MywarehouseService,
		public shopformeService :  ShopformeService,
		public auto : AutoService,
		public fillshipService : FillshipService,
		) { }

	ngOnInit() {

		this.cardInvalid =  {
	    	ccardNumber : false,
	    	expiryMonth : false,
	    	expiryYear : false,
	    	cvvCode : false,
	    	payCardType : false,
	    	cardHolderName : false,
		};
	}

	createRangeYear(number){
		var d = new Date();
	    var n = d.getFullYear();

		var expiryYear: number[] = [];
		for(var i = n; i <= n+20; i++){
		 	expiryYear.push(i);
		}
		return expiryYear;
	}

	onSearchChange(searchValue : string ) {  
	
		let regexMap = [
	      {regEx: /^4[0-9]{5}/ig,cardType: "VISA"},
	      {regEx: /^5[1-5][0-9]{4}/ig,cardType: "MASTERCARD"},
	      {regEx: /^(6(011|5[0-9][0-9])[0-9]{12})/ig,cardType: "DISCOVER"},
	      {regEx: /^(?:3[47][0-9]{13})$/ig,cardType: "American Express"}
	    ];

	    this.payCardType = '';
	    this.cardTypeIcon = '';
		for (let j = 0; j < regexMap.length; j++) {
		  if (searchValue.match(regexMap[j].regEx)) {
		    this.payCardType = regexMap[j].cardType;
		    if(this.payCardType!='American Express')
		    	this.cardTypeIcon = this.payCardType.toLowerCase()+'.png';
		    else
		    	this.cardTypeIcon = 'american-express.png';
		    //console.log(this.payCardType);
		    break;
		  }
		}
	}

	dataSubmit(form) {
		if(this.ccardNumber == '')
			this.cardInvalid.ccardNumber = true;
		else 
			this.cardInvalid.ccardNumber = false;

		if(this.expiryMonth == '')
			this.cardInvalid.expiryMonth = true;
		else 
			this.cardInvalid.expiryMonth = false;
		
		if(this.expiryYear == '')
			this.cardInvalid.expiryYear = true;
		else 
			this.cardInvalid.expiryYear = false;	
		
		if(this.cvvCode == '')
			this.cardInvalid.cvvCode = true;
		else 
			this.cardInvalid.cvvCode = false;

		if(this.cardHolderName == '')
			this.cardInvalid.cardHolderName = true;
		else
			this.cardInvalid.cardHolderName = false;

		if(this.ccardNumber == '' || this.expiryMonth== '' || this.expiryYear == '' || this.cvvCode  == '' || this.cardHolderName == ''){
	        this.formSubmit = false;
		} else {

			this.spinner.show();
			const routeUrl = this.router.url.split('/');

			if(routeUrl[1] == 'my-warehouse')
			{			
				if(routeUrl[2] == "payment")
				{
					
					const returnData = JSON.parse(localStorage.getItem('returnItemDetails'));
					
					this.userCart = {"data" : {"paymentMethodKey" : "payeezy","taxAmount":this.taxAmount,"paidAmount":this.amountDisplay,"paymentMethodId":11,"paymentMethodName":"Payeezy"},"returnData" : returnData, "userId" : this.regService.userId};
					this.userCart['payeezyData'] = form.value;
					this.userCart['payeezyData']['paymentType'] = "credit_card";
					this.userCart['payeezyData']['cardType'] = this.payCardType;
					this.userCart['payeezyData']['paidAmount'] = this.payAmount;
					this.userCart['payeezyData']['userId'] = this.regService.userId;

					this.regService.payreturn(this.userCart).subscribe((data:any) => {
						this.spinner.hide();
						this.bsModalRef.hide();
						if(data.status == '1')
						{
							this.toastr.success("Return payment proccessed successfully.");
							this.spinner.show();
							this.router.navigate(['my-warehouse','overview']);
						} else {
			             	localStorage.removeItem('returnItemDetails');  

			             	if(data.results == 'payment_failed')
			             		this.toastr.error(data.msg);
			             	else
			             		this.toastr.error('There seems to be some while processing the order. Please contact site admin.');	

			             	setTimeout(() => {
						        /** spinner ends after 3 seconds */
						        this.spinner.hide();
						    }, 3000);
						    this.router.navigate(['my-warehouse','overview']);   
			             }

					});

				}else{

					this.userCart = JSON.parse(localStorage.getItem('userWarehouseCart'));
					this.userCart['payeezyData'] = form.value;
					this.userCart['payeezyData']['paymentType'] = "credit_card";
					this.userCart['payeezyData']['cardType'] = this.payCardType;
					this.userCart['payeezyData']['paidAmount'] = this.payAmount;
					this.userCart['payeezyData']['userId'] = this.regService.userId;
		 			
					this.userCart['userId'] = this.regService.userId;
					this.mywarehouseService.submitOrder(this.userCart).subscribe((data:any) => {
						this.spinner.hide();
						this.bsModalRef.hide();
						if(data.status == '1')
						{
							this.toastr.success("Data successfully submited.");
							localStorage.removeItem('userWarehouseCart');
							this.router.navigate(['my-account','order-history']);
						} else {
			             	//localStorage.removeItem('userWarehouseCart');  

			             	if(data.results == 'payment_failed')
			             		this.toastr.error(data.msg);
			             	else
			             		this.toastr.error('There seems to be some while processing the order. Please contact site admin.');	

			             	setTimeout(() => {
						        /** spinner ends after 3 seconds */
						        this.spinner.hide();
						    }, 3000);
						    //this.router.navigate(['my-warehouse','overview']);   
			             }

					});

				}		
			}

			else if(routeUrl[1] == 'shop-for-me')
			{
				this.userCart = JSON.parse(localStorage.getItem('userShopForMeCart'));
				this.userCart['payeezyData'] = form.value;
				this.userCart['payeezyData']['paymentType'] = "credit_card";
				this.userCart['payeezyData']['cardType'] = this.payCardType;
				this.userCart['payeezyData']['paidAmount'] = this.payAmount;
				this.userCart['payeezyData']['userId'] = this.regService.userId;
				this.userCart['userId'] = this.regService.userId;
				this.userCart['warehouseId'] = this.warehouseId;
				this.userCart['tempDataId'] = this.tempDataId;

				this.shopformeService.submitOrder(this.userCart).subscribe((data:any) => {
					this.spinner.hide();
					this.bsModalRef.hide();
					if(data.status == '1')
					{
						this.toastr.success('Your order has been placed successfully!!');
						localStorage.removeItem('userShopForMeCart');
						this.router.navigate(['shop-for-me','order-history']);
					} else {
			             	//localStorage.removeItem('userWarehouseCart');  

			             	if(data.results == 'payment_failed')
			             		this.toastr.error(data.msg);
			             	else
			             		this.toastr.error('There seems to be some while processing the order. Please contact site admin.');	

			             	setTimeout(() => {
						        /** spinner ends after 3 seconds */
						        this.spinner.hide();
						    }, 3000);
						    //this.router.navigate(['my-warehouse','overview']);   
			          }
				});

			} 

			else if(routeUrl[1] == 'auto-parts')
			{
				
				this.userCart = JSON.parse(localStorage.getItem('cartData'));
				this.userCart['payeezyData'] = form.value;
				this.userCart['payeezyData']['paymentType'] = "credit_card";
				this.userCart['payeezyData']['cardType'] = this.payCardType;
				this.userCart['payeezyData']['paidAmount'] = this.payAmount;
				this.userCart['payeezyData']['userId'] = this.regService.userId;
				this.userCart['tempDataId'] = this.tempDataId;
	 			
					this.userCart['userId'] = this.regService.userId;
					this.userCart['warehouseId'] = this.warehouseId;
					this.shopformeService.placeOrder(this.userCart).subscribe((data:any) => {
						this.spinner.hide();
						this.bsModalRef.hide();
						if(data.status == '1')
						{
							this.toastr.success("Data successfully submited.");
							localStorage.removeItem('cartData');
							this.router.navigate(['auto-parts','order-history']);
						}
						else {
							if(data.results == 'payment_failed')
			             		this.toastr.error(data.msg);
			             	else
			             		this.toastr.error('There seems to be some while processing the order. Please contact site admin.');		
						}

					});

			}

			else if(routeUrl[1] == 'auto')
			{
				this.autoCart = JSON.parse(localStorage.getItem('userAutoCart'));
				this.autoCart['payeezyData'] = form.value;
				this.autoCart['payeezyData']['paymentType'] = "credit_card";
				this.autoCart['payeezyData']['cardType'] = this.payCardType;
				this.autoCart['payeezyData']['paidAmount'] = this.payAmount;
				this.autoCart['tempDataId'] = this.tempDataId;
				
	 			if(this.autoCart.type == 'ship_my_car')
				{
					this.autoCart['userId'] = this.regService.userId;
					this.autoCart['warehouseId'] = this.warehouseId;

					this.auto.saveShipMyCar(this.autoCart).subscribe((data:any) => {
						this.spinner.hide();
						this.bsModalRef.hide();
						if(data.status == '1')
						{
							this.toastr.success("Data successfully submited.");
							localStorage.removeItem('userAutoCart');
							this.router.navigate(['auto','my-warehouse']);
						}
						else if(data.status == '-1')
							this.toastr.error("Something went wrong");
						else if(data.status == '-2')
						{
							this.toastr.error(data.results);
							localStorage.removeItem('userAutoCart');
							this.router.navigate(['auto','my-warehouse']);
						}

					});
				}
				else
				{
					this.auto.saveBuycarData(this.autoCart, this.warehouseId,this.regService.userId).subscribe((data:any) => {
						this.spinner.hide();
						this.bsModalRef.hide();
						if(data.status == '1')
						{
							this.toastr.success("Data successfully submited.");
							localStorage.removeItem('userAutoCart');
							this.router.navigate(['auto','car-i-have-bought']);
						}
						else if(data.status == '-1')
							this.toastr.error("Something went wrong");
						else if(data.status == '-2')
						{
							this.toastr.error(data.results);
							localStorage.removeItem('userAutoCart');
							this.router.navigate(['auto','car-i-have-bought']);
						}
					});
				}
			}

			else if(routeUrl[1] == 'fill-and-ship' && routeUrl[2] != 'extracost')
			{				
				this.userCart = JSON.parse(localStorage.getItem('userFillShipCart'));
				this.userCart['payeezyData'] = form.value;
				this.userCart['payeezyData']['paymentType'] = "credit_card";
				this.userCart['payeezyData']['cardType'] = this.payCardType;
				this.userCart['payeezyData']['paidAmount'] = this.payAmount;
				this.userCart['tempDataId'] = this.tempDataId;
	 			
				this.userCart['userId'] = this.regService.userId;
				this.fillshipService.submitOrder(this.userCart).subscribe((data:any) => {
					this.spinner.hide();
					this.bsModalRef.hide();
					if(data.status == '1')
					{
						this.toastr.success("Your order has been placed successfully!!");
						localStorage.removeItem('userFillShipCart');
						this.router.navigate(['fill-and-ship','overview']);
					} else {
		             	//localStorage.removeItem('userFillShipCart');  

		             	if(data.results == 'payment_failed')
		             		this.toastr.error(data.msg);
		             	else
		             		this.toastr.error('There seems to be some while processing the order. Please contact site admin.');	

		             	setTimeout(() => {
					        /** spinner ends after 3 seconds */
					        this.spinner.hide();
					    }, 3000);
					    localStorage.removeItem('userFillShipCart');
					    this.router.navigate(['fill-and-ship','order-form']);
		             }

				});

			}

			else if(routeUrl[1] == 'fill-and-ship' && routeUrl[2] == 'extracost') {
				//console.log(form.value);
				this.invoiceData['payeezyData'] = form.value;
				this.invoiceData['payeezyData']['paymentType'] = "credit_card";
				this.invoiceData['payeezyData']['cardType'] = this.payCardType;
				this.invoiceData['payeezyData']['paidAmount'] = this.payAmount;
				
				//console.log(this.invoiceData);
				this.fillshipService.payextracharge(this.invoiceData).subscribe((response:any) => {
						this.spinner.hide();
						this.bsModalRef.hide();
					if(response.status == 1){
						localStorage.removeItem('extraCostPaidAmount');
						this.router.navigate(['fill-and-ship','overview']);
						this.toastr.success("Extracharge payment proccessed successfully.");
					}else if(response.status == '-1') {
						this.toastr.error(response.results);
					} 
					else {
						//window.location.reload();
						this.toastr.error("Unable to proccess request. Please try again.");
					}
				});
			}

			else if(routeUrl[1] == 'payment')
			{

				this.invoiceData['payeezyData'] = form.value;
				this.invoiceData['payeezyData']['paymentType'] = "credit_card";
				this.invoiceData['payeezyData']['cardType'] = this.payCardType;
				this.invoiceData['payeezyData']['paidAmount'] = this.payAmount;
				this.userCart['payeezyData']['userId'] = this.regService.userId;

				this.regService.payinvoice(this.invoiceData).subscribe	((data:any) => {
						this.spinner.hide();
						this.bsModalRef.hide();
						if(data.status == '1')
						{
							this.toastr.success("Invoice payment proccessed successfully.");
							this.router.navigate(['payment']);
						}
						else if(data.status == '-1') {
							this.toastr.error(data.results);
						}
						 else {
							//window.location.reload();
							this.toastr.error("Unable to proccess request. Please try again.");
						}

					});
			}
		}
	}
}
