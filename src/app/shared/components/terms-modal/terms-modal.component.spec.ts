import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShipmentSettingsModalComponent } from './shipmentsettings-modal.component';

describe('ShipmentSettingsModalComponent', () => {
	let component: ShipmentSettingsModalComponent;
	let fixture: ComponentFixture<ShipmentSettingsModalComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ShipmentSettingsModalComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ShipmentSettingsModalComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
