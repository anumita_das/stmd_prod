import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Response } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { config } from '../../app.config';


@Injectable({
  providedIn: 'root'
})

export class SubscribeService {
	private rootUrl = config.api;
	private httpOptions: any;


	constructor(private http: HttpClient) { 
			this.httpOptions = {
	        headers: new HttpHeaders({ 'Content-Type': 'application/json'})
	    };
	}

	getsubscriptiondetails() {
	   return this.http.get(this.rootUrl+'getsubscriptiondetails',this.httpOptions);
	}
}
