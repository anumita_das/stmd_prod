import { TestBed, inject } from '@angular/core/testing';

import { GetquoteService } from './getquote.service';

describe('GetquoteService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetquoteService]
    });
  });

  it('should be created', inject([GetquoteService], (service: GetquoteService) => {
    expect(service).toBeTruthy();
  }));
});
