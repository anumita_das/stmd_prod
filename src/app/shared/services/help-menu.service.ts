import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import 'rxjs/add/operator/toPromise';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { config } from '../../app.config';


@Injectable({
  providedIn: 'root'
})
export class HelpMenuService {

	private rootUrl = config.api;
	private httpOptions: any;
  	constructor(private http: HttpClient, private sanitizer: DomSanitizer) {

  		this.httpOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/json'})
        };
  	}

  	getAllCustomerHelpCategory() {
  		return this.http.get(this.rootUrl+'getallcustomerhelpcategory',this.httpOptions);
  	}

	helpNavList = [
		{ name: 'Getting Started', link: 'get-start' },
		{
			name: 'Tutorial', link: '', children: [
				{ name: 'Introduction', link: 'tutorial/introduction', subChildren: [] },
				{ name: 'Introduction', link: 'tutorial/introduction', subChildren: [] },
				{ name: 'Introduction', link: 'tutorial/introduction', subChildren: [] },
				{ name: 'Introduction', link: 'tutorial/introduction', subChildren: [] },
				{ name: 'Introduction', link: 'tutorial/introduction', subChildren: [] }
			]
		},
		{
			name: 'Tutorial', link: '', children: [
				{ name: 'Introduction', link: 'tutorial/introduction', subChildren: [] },
				{ name: 'Introduction', link: 'tutorial/introduction', subChildren: [] },
				{ name: 'Introduction', link: 'tutorial/introduction', subChildren: [] },
				{ name: 'Introduction', link: 'tutorial/introduction', subChildren: [] },
				{ name: 'Introduction', link: 'tutorial/introduction', subChildren: [] }
			]
		}
	];

	getData() {
		return this.helpNavList;
	}

	

}
