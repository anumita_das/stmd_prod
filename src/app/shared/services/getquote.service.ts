import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Response } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { config } from '../../app.config';


@Injectable({
  providedIn: 'root'
})
export class GetquoteService {

	private rootUrl = config.api;
	private httpOptions: any;


	constructor(private http: HttpClient) { 
			this.httpOptions = {
	        headers: new HttpHeaders({ 'Content-Type': 'application/json'})
	    };
	}

	countrylist() {
	   return this.http.get(this.rootUrl+'getcountry',this.httpOptions);
	}

	
	statecitylist(data:any) {
	   return this.http.get(this.rootUrl+'getstatecity/'+data,this.httpOptions);
	}


	citylist(data:any) {
	   return this.http.get(this.rootUrl+'getcity/'+data,this.httpOptions);
	}

	categorylist() {
	   return this.http.post(this.rootUrl+'getcategory',this.httpOptions);
	}

	subcategorylist(data:any) {
	   return this.http.get(this.rootUrl+'getsubcategory/'+data,this.httpOptions);
	}

	productlist(data:any) {
	   return this.http.get(this.rootUrl+'getproduct/'+data,this.httpOptions);
	}

	makelist() {
	   return this.http.get(this.rootUrl+'getmakelist',this.httpOptions);
	}

	modellist(data:any) {
	   return this.http.get(this.rootUrl+'getmodellist/'+data,this.httpOptions);
	}

	currencylist() {
	   return this.http.get(this.rootUrl+'getcurrency',this.httpOptions);
	}

	warehouselist() {
		 return this.http.get(this.rootUrl+'getwarehouselist',this.httpOptions);
	}

 	submitquote(data:any): Observable<any> {
      //localStorage.clear();
      const url = this.rootUrl+'getquote/fetchquote';
      return this.http.post(url, data);
    }

    getItemQuote(data:any): Observable<any> {
      const url = this.rootUrl+'getquote/fetchitemquote';
      return this.http.post(url, data);
    }

    getAutoQuote(data:any): Observable<any> {
      const url = this.rootUrl+'getquote/fetchautoquote';
      return this.http.post(url, data);
    }

    submitContact(formValue)
    {  
      return this.http.post(this.rootUrl+'submitContact',formValue,this.httpOptions);

    }

    saveItemQuoteContact(formData) {
    	return this.http.post(this.rootUrl+'getquote/saveitemquotecontact',formData);
    }

}
