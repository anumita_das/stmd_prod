import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class WindowWidthService {
	// private mediumScreen: Subject<boolean> = new Subject<boolean>();
	private mediumScreen: BehaviorSubject<boolean>;
	// mediumScreenStatus = this.mediumScreen.asObservable();

	constructor() {
		this.mediumScreen = new BehaviorSubject<boolean>(false);
	}

	public getMediumScreen(): Observable<boolean> {
		return this.mediumScreen.asObservable();
	}

	public setMediumScreen(newValue: boolean): void {
		this.mediumScreen.next(newValue);
	}

	// isMediumScreen(value: boolean) {
	// 	console.log('Value in Service =>', value);
	// 	this.mediumScreen.next(value);
	// }
}
