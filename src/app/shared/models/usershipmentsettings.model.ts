export class UserShipmentSettings {
	remove_shoe_box : string;
	original_box : string;
	magazine_letter : string;
	include_original_invoice : string;
	declare_items_personal : string;
	declare_items_gifts : string;
	scan_invoice : string;
	take_photo : string;
	special_instruction : string;
}