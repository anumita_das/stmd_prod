export class User {
	unit: any;
	title : string;
	firstName : string;
	lastName : string;
	email : string;
	confirmEmail : string;	
	phcode : string;
	number : string;
	password : string;
	confPassword : string;
	address : string;
	alternateaddress : string;	
	country : string;
	state : string;
	city : string;
	company : string;
	referralCode : string;
}
