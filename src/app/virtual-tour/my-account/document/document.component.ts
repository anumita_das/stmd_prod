import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-document',
	templateUrl: './document.component.html',
	styleUrls: ['./document.component.scss']
})
export class DocumentComponent implements OnInit {
	dtOptions: DataTables.Settings = {};
	documents: Array<any> = [];
	constructor() { }

	ngOnInit() {

		// documents
		this.documents = [
			{
				document: 'demo-product.png',
				des: 'Aenean pharetra tortor sed neque'
			},
			{
				document: 'demo-product.png',
				des: 'Aenean pharetra tortor sed neque'
			},
			{
				document: 'demo-product.png',
				des: 'Aenean pharetra tortor sed neque'
			}
		];

		// datatable
		this.dtOptions = {
			paging: false,
			ordering: false,
			searching: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};
	}

	// deleteDocumentRow
	deleteDocumentRow(index) {
		this.documents.splice(index, 1);
	}
}
