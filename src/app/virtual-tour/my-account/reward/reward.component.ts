import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-reward',
	templateUrl: './reward.component.html',
	styleUrls: ['./reward.component.scss']
})
export class RewardComponent implements OnInit {
	dtOptions: DataTables.Settings = {};
	activities: Array<any>;
	constructor() { }

	ngOnInit() {
		// datatable
		this.dtOptions = {
			paging: false,
			ordering: false,
			searching: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};

		// activities
		this.activities = [
			{
				date: '06-01-2018 11:42',
				reason: 'Registration',
				point: '0'
			},
			{
				date: '05-31-2018 10:47',
				reason: 'Registration',
				point: '0'
			},
			{
				date: '05-31-2018 10:47',
				reason: 'Registration',
				point: '0'
			},
			{
				date: '05-31-2018 10:46',
				reason: 'Registration',
				point: '0'
			},
			{
				date: '05-28-2018 08:58',
				reason: 'Registration',
				point: '0'
			},
			{
				date: '05-28-2018 08:58',
				reason: 'Registration',
				point: '0'
			},
			{
				date: '04-13-2018 13:33',
				reason: 'Converted to E-Wallet',
				point: '-167'
			}
		];
	}
}
