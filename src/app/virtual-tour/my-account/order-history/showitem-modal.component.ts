import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgForm } from '@angular/forms';
import { RegistrationService } from '../../../services/registration.service';
import { ShopformeService } from '../../../services/shopforme.service';
import { Router } from "@angular/router";
import { AppGlobals } from '../../../app.globals';

@Component({
	selector: 'app-showitem-modal',
	templateUrl: './showitem-modal.component.html',
	styleUrls: ['./showitem-modal.component.scss'],
	providers: [AppGlobals]
})
export class ShowItemDetailModalComponent implements OnInit {

	title: string;
	closeBtnName: string;
	shippingTypes: Array<any>;
	paymentDetails: Array<any>;
	defaultCurrencySymbol: string;
	procurementId: any;
	dtOptions: DataTables.Settings = {};
	bagList: any;

	constructor(
		public bsModalRef: BsModalRef,
		public userInfo: RegistrationService,
		private shopformeService: ShopformeService,
		private router: Router,
		private _global: AppGlobals,
	) {
		this.defaultCurrencySymbol = this._global.defaultCurrencySymbol;
	}

	ngOnInit() {

		// datatable
		this.dtOptions = {
			paging: false,
			// bInfo: false,
			ordering: false,
			searching: false,
			// bSort: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};

	}




	hack(value) {
		return Object.values(value)
	}



}
