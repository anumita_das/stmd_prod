import { Component, OnInit, NgZone, HostListener, AfterViewInit, ViewChild } from '@angular/core';
import { WalkthroughText, WalkthroughComponent, WalkthroughFlowComponent } from 'angular-walkthrough';
import { Router } from '@angular/router';
import { RegistrationService } from '../../../services/registration.service';

@Component({
	selector: 'app-order-history',
	templateUrl: './order-history.component.html',
	styleUrls: ['./order-history.component.scss']
})
export class OrderHistoryComponent implements OnInit, AfterViewInit {
	dtOptions: DataTables.Settings = {};
	orders: Array<any>;
	check_all;
	config = {
		ignoreBackdropClick: false
	};

	constructor(
		private router: Router,
		public regService : RegistrationService,
	) {
		this.check_all = {
			type: false
		};
	}


	@ViewChild('walkOrderHistory') walkRef: WalkthroughFlowComponent;

	// WalkthroughText
	navText: WalkthroughText = {
		previous: 'Previous',
		next: 'Next',
		close: 'Finish'
	};

	lastNavText: WalkthroughText = {
		previous: 'Previous',
		next: 'Next',
		close: 'Next'
	};

	ngAfterViewInit(): void {
		WalkthroughComponent.walkthroughContinue();
		setTimeout(() => {
			this.walkRef.start();
		}, 500);
	}

	ngOnInit() {
		this.orders = [
			{
				selected: false,
				shipment: '3',
				order: 'OD-1384369',
				shipping_cost: '5962.32',
				total_cost: '516.60',
				status: 'Shipment Submitted',
				payment_status: 'Paid',
				date: '10/18/2018'
			},
			{
				selected: false,
				shipment: '3',
				order: 'OD-1384369',
				shipping_cost: '5962.32',
				total_cost: '516.60',
				status: 'Shipment Submitted',
				payment_status: 'Paid',
				date: '10/18/2018'
			},
			{
				selected: false,
				shipment: '3',
				order: 'OD-1384369',
				shipping_cost: '5962.32',
				total_cost: '516.60',
				status: 'Shipment Submitted',
				payment_status: 'Paid',
				date: '10/18/2018'
			},
			{
				selected: false,
				shipment: '3',
				order: 'OD-1384369',
				shipping_cost: '5962.32',
				total_cost: '516.60',
				status: 'Shipment Submitted',
				payment_status: 'Paid',
				date: '10/18/2018'
			}
		];



		// datatable
		this.dtOptions = {
			paging: false,
			// bInfo: false,
			ordering: false,
			searching: false,
			// bSort: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};
	}

	walkOrderHistoryFinished() {
		this.regService.updateVirtualtourlog('finish','order-history','1').subscribe((data:any) => {
			if(data.status == '1')
			{
				let resumeUrl = JSON.parse(localStorage.getItem('virtualTourResumeUrl'));
				this.router.navigateByUrl(resumeUrl);
			}
		});
	}

	pause() {
		//console.log('skipped');
		this.regService.updateVirtualtourlog('skip','order-history','0').subscribe((data:any) => {
    		if(data.status == 1)
    		{
    			WalkthroughComponent.walkthroughStop();
    			let resumeUrl = JSON.parse(localStorage.getItem('virtualTourResumeUrl'));
				this.router.navigateByUrl(resumeUrl);
    		}


    	});
	}



}
