import { Component, OnInit, NgZone, HostListener, ViewChild } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { AddressbookModalComponent } from './addressbook-modal/addressbook-modal.component';
import { WalkthroughText, WalkthroughComponent, WalkthroughFlowComponent } from 'angular-walkthrough';
import { Router } from '@angular/router';

@Component({
	selector: 'app-my-account',
	templateUrl: './my-account.component.html',
	styleUrls: ['./my-account.component.scss']
})
export class MyAccountComponent implements OnInit {
	isMediumScreen: boolean;
	sidebar: boolean;
	// bsModalRef: BsModalRef;

	modalRef: BsModalRef;
	modalRef2: BsModalRef;
	constructor(
		private ngZone: NgZone,
		private modalService: BsModalService,
		private router: Router,
	) {
		// this.openAddressModal();
	}

	// @ViewChild('modalRef') modalService: BsModalService;

	ngOnInit() {
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		console.log('Width: ' + width);
		this.checkWindowSize(width);
	}

	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		console.log('Width: ' + event.target.innerWidth);
		this.checkWindowSize(event.target.innerWidth);
	}


	// openAddressModal() {
	// 	this.bsModalRef = this.modalService.show(AddressbookModalComponent, {
	// 		class: 'modal-sm modal-dialog-centered',
	// 		animated: true

	// 	});
	// 	this.bsModalRef.content.closeBtnName = 'Close';
	// }

	walkthrougnSkip() {
		WalkthroughComponent.walkthroughStop();
		this.modalRef.hide();
		// this.modalRef2.hide();
		this.router.navigateByUrl('/my-account/account-detail');
	}


}
