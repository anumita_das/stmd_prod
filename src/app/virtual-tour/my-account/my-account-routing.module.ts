import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyAccountComponent } from './my-account.component';
import { AccountDetailComponent } from './account-detail/account-detail.component';
import { OrderHistoryComponent } from './order-history/order-history.component';
import { AddressbookComponent } from './addressbook/addressbook.component';
import { RewardComponent } from './reward/reward.component';
import { FileClaimComponent } from './file-claim/file-claim.component';
import { DocumentComponent } from './document/document.component';
import { GalleryComponent } from './gallery/gallery.component';
import { ModifyProfileComponent } from './modify-profile/modify-profile.component';

const routes: Routes = [
	{
		path: '', redirectTo: 'account-detail', pathMatch: 'full'
	},
	{
		path: '',
		component: MyAccountComponent,
		children: [
			{ path: 'account-detail', component: AccountDetailComponent },
			{ path: 'order-history', component: OrderHistoryComponent },
			{ path: 'addressbook', component: AddressbookComponent },
			{ path: 'modify-profile', component: ModifyProfileComponent },
			{ path: 'reward', component: RewardComponent },
			{ path: 'file-claim', component: FileClaimComponent },
			{ path: 'my-documents', component: DocumentComponent },
			{ path: 'gallery', component: GalleryComponent }
		]
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class MyAccountRoutingModule {

}
