import { Component, OnInit, NgZone, HostListener, AfterViewInit, ViewChild } from '@angular/core';
import { WalkthroughText, WalkthroughComponent, WalkthroughFlowComponent } from 'angular-walkthrough';
import { Router } from '@angular/router';
import { RegistrationService } from '../../../services/registration.service';


@Component({
	selector: 'app-addressbook',
	templateUrl: './addressbook.component.html',
	styleUrls: ['./addressbook.component.scss']
})
export class AddressbookComponent implements OnInit, AfterViewInit {
	isMediumScreen: boolean;
	sidebar: boolean;
	isCollapsed: boolean;
	userShippingAddress: Array<object>;
	addressId: any = '';
	phcode: any;
	altphcode: any;
	constructor(
		private ngZone: NgZone,
		private router: Router,
		public regService : RegistrationService,
	) { }


	@ViewChild('walkAddressBook') walkRef: WalkthroughFlowComponent;

	// WalkthroughText
	navText: WalkthroughText = {
		previous: 'Previous',
		next: 'Next',
		close: 'Finish'
	};

	lastNavText: WalkthroughText = {
		previous: 'Previous',
		next: 'Next',
		close: 'Next'
	};

	ngAfterViewInit(): void {
		WalkthroughComponent.walkthroughContinue();
		setTimeout(() => {
			this.walkRef.start();
		}, 500);
	}

	ngOnInit() {
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		// console.log('Width: ' + width);
		this.checkWindowSize(width);

		// account isCollapsed
		this.isCollapsed = false;
		this.getUserAddressInfo();
	}

	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		//console.log('Width: ' + event.target.innerWidth);
		this.checkWindowSize(event.target.innerWidth);
	}

	getUserAddressInfo() {
		this.userShippingAddress = [{ "id": 6, "userId": 3, "isDefaultShipping": "0", "isDefaultBilling": "0", "title": "Mr", "firstName": "John", "lastName": "Doe", "email": "somnath.pahari@indusnet.co.in", "address": "211 South Meadows Court", "alternateAddress": null, "cityId": 639, "countyId": null, "stateId": 2, "countryId": 163, "locationId": 0, "zipcode": "77479", "phone": "713-530-1160", "isdCode": "", "alternatePhone": null, "altIsdCode": null, "modifiedOn": "2018-10-09 15:16:46", "modifiedBy": 0, "deleted": "0", "deletedOn": null, "deletedBy": null, "country": { "id": 163, "code": "NG", "codeA3": "NGA", "codeN3": 566, "isdCode": "234", "region": "AF", "name": "Nigeria", "status": "1", "priority": 2, "modifiedOn": null, "modifiedBy": null }, "state": { "id": 2, "countryCode": "NG", "code": "LG", "name": "Lagos", "status": "1", "deleted": "0", "createdBy": 0, "createdOn": "0000-00-00 00:00:00", "modifiedBy": null, "modifiedOn": null, "deletedBy": null, "deletedOn": null }, "city": { "id": 639, "name": " Lagos Island", "stateCode": "LG", "countryCode": "NG", "status": "1", "deleted": "0", "createdBy": 0, "createdOn": "0000-00-00 00:00:00", "modifiedBy": null, "modifiedOn": null, "deletedBy": null, "deletedOn": null } }, { "id": 8, "userId": 3, "isDefaultShipping": "0", "isDefaultBilling": "0", "title": "Mrs.", "firstName": "Marie", "lastName": "Curie", "email": "paulami.sarkar@indusnet.co.in", "address": "KM 87 Benin Auchi Rd, Irrua", "alternateAddress": null, "cityId": 638, "countyId": null, "stateId": 15, "countryId": 163, "locationId": 0, "zipcode": "67676", "phone": "123-456-7890", "isdCode": "", "alternatePhone": null, "altIsdCode": null, "modifiedOn": "2018-10-09 15:34:16", "modifiedBy": 0, "deleted": "0", "deletedOn": null, "deletedBy": null, "country": { "id": 163, "code": "NG", "codeA3": "NGA", "codeN3": 566, "isdCode": "234", "region": "AF", "name": "Nigeria", "status": "1", "priority": 2, "modifiedOn": null, "modifiedBy": null }, "state": { "id": 15, "countryCode": "NG", "code": "ED", "name": "Edo", "status": "1", "deleted": "0", "createdBy": 0, "createdOn": "0000-00-00 00:00:00", "modifiedBy": null, "modifiedOn": null, "deletedBy": null, "deletedOn": null }, "city": { "id": 638, "name": "Akoko-Edo", "stateCode": "ED", "countryCode": "NG", "status": "1", "deleted": "0", "createdBy": 0, "createdOn": "0000-00-00 00:00:00", "modifiedBy": null, "modifiedOn": null, "deletedBy": null, "deletedOn": null } }, { "id": 18, "userId": 3, "isDefaultShipping": "1", "isDefaultBilling": "1", "title": "Mr.", "firstName": "Enrico", "lastName": "Fermi", "email": "tathagata.sur@indusnet.co.uk", "address": "39XF VV", "alternateAddress": "KOLKATA jjjjj kkk lll", "cityId": 378, "countyId": null, "stateId": 5, "countryId": 163, "locationId": 0, "zipcode": "700091", "phone": "987-654-3210", "isdCode": "", "alternatePhone": "921234567899", "altIsdCode": null, "modifiedOn": "2018-09-21 07:36:37", "modifiedBy": 0, "deleted": "0", "deletedOn": null, "deletedBy": null, "country": { "id": 163, "code": "NG", "codeA3": "NGA", "codeN3": 566, "isdCode": "234", "region": "AF", "name": "Nigeria", "status": "1", "priority": 2, "modifiedOn": null, "modifiedBy": null }, "state": { "id": 5, "countryCode": "NG", "code": "AB", "name": "Abia", "status": "1", "deleted": "0", "createdBy": 0, "createdOn": "0000-00-00 00:00:00", "modifiedBy": null, "modifiedOn": null, "deletedBy": null, "deletedOn": null }, "city": { "id": 378, "name": "Aba", "stateCode": "AB", "countryCode": "NG", "status": "1", "deleted": "0", "createdBy": 0, "createdOn": "0000-00-00 00:00:00", "modifiedBy": null, "modifiedOn": null, "deletedBy": null, "deletedOn": null } }]

	}

	walkAddressBookFinished() {
		this.regService.updateVirtualtourlog('finish','addressbook','1').subscribe((data:any) => {
			if(data.status == '1')
			{
				let resumeUrl = JSON.parse(localStorage.getItem('virtualTourResumeUrl'));
				this.router.navigateByUrl(resumeUrl);
			}
		});
	}

	pause() {
		//console.log('skipped');
		this.regService.updateVirtualtourlog('skip','addressbook','0').subscribe((data:any) => {
    		if(data.status == 1)
    		{
    			WalkthroughComponent.walkthroughStop();
    			let resumeUrl = JSON.parse(localStorage.getItem('virtualTourResumeUrl'));
				this.router.navigateByUrl(resumeUrl);
    		}


    	});
	}
}
