import { Component, OnInit } from '@angular/core';
// import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { GalleryModalComponent } from '@app/shared/components/gallery-modal/gallery-modal.component';
// import { GalleryModalComponent } from '../../shared/components/gallery-modal/gallery-modal.component';
@Component({
	selector: 'app-gallery',
	templateUrl: './gallery.component.html',
	styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {
	galleryImages: Array<any>;
	bsModalRef: BsModalRef;
	constructor(
		private modalService: BsModalService
	) { }

	ngOnInit() {
		this.galleryImages = [
			{
				img: 'assets/imgs/gallery/full-size/img-1.jpeg',
				description: 'Some caption'
			},
			{
				img: 'assets/imgs/gallery/full-size/img-2.jpeg',
				description: 'Some caption'
			},
			{
				img: 'assets/imgs/gallery/full-size/img-3.jpeg',
				description: 'Some caption'
			},
			{
				img: 'assets/imgs/gallery/full-size/img-4.jpg',
				description: 'Some caption'
			},
			{
				img: 'assets/imgs/gallery/full-size/img-1.jpeg',
				description: 'Some caption'
			},
			{
				img: 'assets/imgs/gallery/full-size/img-2.jpeg',
				description: 'Some caption'
			},
			{
				img: 'assets/imgs/gallery/full-size/img-3.jpeg',
				description: 'Some caption'
			},
			{
				img: 'assets/imgs/gallery/full-size/img-4.jpg',
				description: 'Some caption'
			}
		];
	}

	openGalleryModal() {
		this.bsModalRef = this.modalService.show(GalleryModalComponent, {
			class: 'modal-sm modal-dialog-centered'

		});
		this.bsModalRef.content.closeBtnName = 'Close';
	}
}
