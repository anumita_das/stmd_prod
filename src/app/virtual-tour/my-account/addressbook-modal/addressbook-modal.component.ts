import { Component, OnInit, NgZone, HostListener, AfterViewInit, ViewChild } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { WalkthroughText, WalkthroughComponent } from 'angular-walkthrough';
import { Router } from '@angular/router';
// import { Router } from '@angular/router';


@Component({
	selector: 'app-addressbook-modal',
	templateUrl: './addressbook-modal.component.html',
	styleUrls: ['./addressbook-modal.component.scss']
})
export class AddressbookModalComponent implements OnInit, AfterViewInit {

	title: string;
	closeBtnName: string;
	// stmdAddress: boolean = false;
	// userAddressSelection: boolean = true;
	list: any[] = [];

	constructor(public bsModalRef: BsModalRef, private router: Router) { }
	@ViewChild('walk1') walkRef: WalkthroughComponent;


	// WalkthroughText
	navText: WalkthroughText = {
		previous: 'Previous',
		next: 'Next',
		close: 'Finish'
	};

	lastNavText: WalkthroughText = {
		previous: 'Previous',
		next: 'Next',
		close: 'Next'
	};

	ngAfterViewInit(): void {
		setTimeout(() => {
			this.walkRef.open();
		}, 500);


	}


	ngOnInit() {
		this.list.push('PROFIT!!!');
	}

	walkFinished() {
		this.bsModalRef.hide();
		// this.router.navigateByUrl('/virtual-tour/account-detail');
	}


}
