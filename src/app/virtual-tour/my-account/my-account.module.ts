import { NgModule } from '@angular/core';
import { MyAccountRoutingModule } from './my-account-routing.module';
import { MyAccountComponent } from './my-account.component';
import { SharedModule } from '@app/shared';
import { AccountDetailComponent } from './account-detail/account-detail.component';
import { AddressbookComponent } from './addressbook/addressbook.component';
import { RewardComponent } from './reward/reward.component';
import { FileClaimComponent } from './file-claim/file-claim.component';
import { DocumentComponent } from './document/document.component';
import { GalleryComponent } from './gallery/gallery.component';
import { ModifyProfileComponent } from './modify-profile/modify-profile.component';
import { AddressbookModalComponent } from './addressbook-modal/addressbook-modal.component';
import { OrderHistoryComponent } from './order-history/order-history.component';
import { ShowItemDetailModalComponent } from './order-history/showitem-modal.component';


@NgModule({
	imports: [
		MyAccountRoutingModule,
		SharedModule
	],
	declarations: [
		MyAccountComponent,
		AccountDetailComponent,
		AddressbookComponent,
		RewardComponent,
		FileClaimComponent,
		DocumentComponent,
		GalleryComponent,
		ModifyProfileComponent,
		AddressbookModalComponent,
		OrderHistoryComponent,
		ShowItemDetailModalComponent
	],
	// providers: [AddressbookModalComponent]
	entryComponents: [AddressbookModalComponent]
})
export class MyAccountModule {

}
