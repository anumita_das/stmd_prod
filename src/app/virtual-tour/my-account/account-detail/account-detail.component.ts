// import { Component, OnInit, NgZone, ViewChild } from '@angular/core';
import { Component, OnInit, NgZone, HostListener, AfterViewInit, ViewChild, TemplateRef } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { AppGlobals } from '../../../app.globals';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { AddressbookModalComponent } from '../../my-account/addressbook-modal/addressbook-modal.component';
import { WalkthroughText, WalkthroughComponent, WalkthroughFlowComponent } from 'angular-walkthrough';
import { Router } from '@angular/router';
import { RegistrationService } from '../../../services/registration.service';


@Component({
	selector: 'app-how-it-work',
	templateUrl: './account-detail.component.html',
	styleUrls: ['./account-detail.component.scss']
})
export class AccountDetailComponent implements OnInit, AfterViewInit {
	@ViewChild('template') templateRef: any;
	@ViewChild('template2') templateRef2: any;
	@ViewChild('autoShownModal') autoShownModal: ModalDirective;
	isModalShown: boolean;


	modalRef: BsModalRef;
	modalRef2: BsModalRef;
	constructor(
		private ngZone: NgZone,
		private modalService: BsModalService,
		private router: Router,
		public regService: RegistrationService,
		public _global: AppGlobals,
	) {

	}

	@ViewChild('walk1') walkRef: WalkthroughComponent;
	@ViewChild('walk2') walkRef2: WalkthroughComponent;
	@ViewChild('walk5') walkRef5: WalkthroughFlowComponent;

	// WalkthroughText
	navText: WalkthroughText = {
		previous: '',
		next: 'Next',
		close: 'Finish'
	};

	lastNavText: WalkthroughText = {
		previous: 'Previous',
		next: 'Next',
		close: 'Next'
	};

	ngAfterViewInit(): void {
		WalkthroughComponent.walkthroughContinue();
		setTimeout(() => {
			this.walkRef.open();
		}, 500);


	}

	ngOnInit() {
		this.isModalShown = true;
		this.openModal(this.templateRef);
		this._global.virtualTourEntered = '1';
		let userData = JSON.parse(localStorage.getItem('tokens'));
		userData.user.virtualTourVisited = "1";
		localStorage.setItem('tokens', JSON.stringify(userData));
		this.regService.item.user.virtualTourVisited = "1";
		this.regService.virtualTouFinished().subscribe((data:any) =>{

		})
	}

	walkFinished() {
		this.modalRef.hide();
		this.openModal2(this.templateRef2);
		setTimeout(() => {
			this.walkRef2.open();
		}, 500);
	}

	walk2Finished() {
		this.modalRef2.hide();

		setTimeout(() => {
			// this.walkRef5.open();
			this.walkRef5.start();
		}, 500);
	}

	// walk3Finished() {

	// 	this.regService.virtualTouFinished().subscribe((data: any) => {
	// 		if (data.status == '1') {
	// 			let userData = JSON.parse(localStorage.getItem('tokens'));
	// 			userData.user.virtualTourVisited = "1";
	// 			localStorage.setItem('tokens', JSON.stringify(userData));
	// 			this.regService.item.user.virtualTourVisited = "1";

	// 			this.router.navigateByUrl('/my-account/account-detail');
	// 		}
	// 	});

	// }

	walk3Finished() {
		this.regService.updateVirtualtourlog('finish','my-account','1').subscribe((data:any) => {
			if(data.status == '1')
			{
				let resumeUrl = JSON.parse(localStorage.getItem('virtualTourResumeUrl'));
				this.router.navigateByUrl('/my-account/account-detail');
			}
		});
	}

	pause() {
		//console.log('skipped');
		this.modalRef.hide();
		this.regService.updateVirtualtourlog('skip','my-account','0').subscribe((data:any) => {
    		if(data.status == 1)
    		{
    			WalkthroughComponent.walkthroughStop();
				this.router.navigateByUrl('/my-account/account-detail');
    		}


    	});
	}



	openModal(template: TemplateRef<any>) {
		this.modalRef = this.modalService.show(template, {
			animated: false,
			backdrop: false
		});
	}
	openModal2(template: TemplateRef<any>) {
		this.modalRef2 = this.modalService.show(template, {
			animated: false,
			backdrop: false
		});
	}
	// openPickup(template: TemplateRef<any>) {
	// 	this.modalRef = this.modalService.show(template, {
	// 		animated: false,
	// 		backdrop: false
	// 	});
	// }

	// openAddressModal() {
	// 	this.bsModalRef = this.modalService.show(AddressbookModalComponent, {
	// 		class: 'modal-sm modal-dialog-centered',
	// 		show: true

	// 	});
	// 	this.bsModalRef.content.closeBtnName = 'Close';
	// }

	showModal(): void {
		this.isModalShown = true;
	}

	hideModal(): void {
		this.autoShownModal.hide();
	}

	onHidden(): void {
		this.isModalShown = false;
	}

	// deliveryFinished() {
	// 	this.bsModalRef.hide();
	// 	// this.router.navigateByUrl('/virtual-tour/account-detail');
	// }

	// deliveryNext() {
	// 	this.bsModalRef.hide();
	// 	// this.router.navigateByUrl('/virtual-tour/account-detail');
	// }
	// walkthroughNext() {
	// 	this.bsModalRef.hide();
	// }



}
