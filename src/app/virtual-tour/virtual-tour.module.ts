import { NgModule } from '@angular/core';
import { VirtualTourRoutingModule } from './virtual-tour-routing.module';
import { VirtualTourComponent } from './virtual-tour.component';
import { SharedModule } from '@app/shared';
// import { AddressbookModalComponent } from './addressbook-modal/addressbook-modal.component';

@NgModule({
	imports: [
		VirtualTourRoutingModule,
		// SharedModule
	],
	declarations: [
		VirtualTourComponent,
		// AddressbookModalComponent
	]
})
export class VirtualTourModule {

}
