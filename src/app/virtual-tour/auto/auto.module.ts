import { NgModule } from '@angular/core';
import { AutoRoutingModule } from './auto-routing.module';
import { AutoComponent } from './auto.component';
import { SharedModule } from '@app/shared';
import { BuyACarForMeComponent } from './buy-a-car-for-me/buy-a-car-for-me.component';
import { ShipMyCarComponent } from './ship-my-car/ship-my-car.component';
import { CarIHaveBoughtComponent } from './car-i-have-bought/car-i-have-bought.component';
import { AutoCheckoutComponent } from './auto-checkout/auto-checkout.component';
import { AutoMyWarehouseComponent } from './auto-my-warehouse/auto-my-warehouse.component';

@NgModule({
	imports: [
		AutoRoutingModule,
		SharedModule
	],
	declarations: [
		AutoComponent,
		BuyACarForMeComponent,
		ShipMyCarComponent,
		CarIHaveBoughtComponent,
		AutoCheckoutComponent,
		AutoMyWarehouseComponent
	]
})
export class AutoModule {

}
