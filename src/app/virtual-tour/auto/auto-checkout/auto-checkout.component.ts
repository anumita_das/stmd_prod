import { Component, OnInit, NgZone, HostListener, AfterViewInit, ViewChild } from '@angular/core';
import { WalkthroughText, WalkthroughComponent, WalkthroughFlowComponent } from 'angular-walkthrough';
import { Router } from '@angular/router';
import { RegistrationService } from '../../../services/registration.service';

@Component({
	selector: 'app-auto-checkout',
	templateUrl: './auto-checkout.component.html',
	styleUrls: ['./auto-checkout.component.scss']
})
export class AutoCheckoutComponent implements OnInit, AfterViewInit {
	dtOptions: DataTables.Settings = {};
	constructor(
		private router: Router,
		public regService: RegistrationService,
	) { }
	@ViewChild('walkAutoCheckout') walkRef: WalkthroughFlowComponent;
	// WalkthroughText
	navText: WalkthroughText = {
		previous: 'Previous',
		next: 'Next',
		close: 'Finish'
	};

	lastNavText: WalkthroughText = {
		previous: 'Previous',
		next: 'Next',
		close: 'Next'
	};

	ngAfterViewInit(): void {
		setTimeout(() => {
			this.walkRef.start();
		}, 500);
	}

	ngOnInit() {
		// datatable
		this.dtOptions = {
			paging: false,
			// bInfo: false,
			ordering: false,
			searching: false,
			// bSort: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};
	}

	walkAutoCheckoutFinished() {
		this.router.navigateByUrl('/virtual-tour/auto/car-i-have-bought');
	}

	pause() {
		//console.log('skipped');
		this.regService.updateVirtualtourlog('skip','auto','0').subscribe((data:any) => {
    		if(data.status == 1)
    		{
    			WalkthroughComponent.walkthroughStop();
    			let resumeUrl = JSON.parse(localStorage.getItem('virtualTourResumeUrl'));
				this.router.navigateByUrl(resumeUrl);
    		}


    	});
	}
}
