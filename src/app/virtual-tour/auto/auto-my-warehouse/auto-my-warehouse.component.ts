import { Component, OnInit, Renderer2, HostListener } from '@angular/core';

@Component({
	selector: 'app-auto-my-warehouse',
	templateUrl: './auto-my-warehouse.component.html',
	styleUrls: ['./auto-my-warehouse.component.scss']
})
export class AutoMyWarehouseComponent implements OnInit {
	isMediumScreen: boolean;
	links: Array<any>;
	constructor(
		private renderer: Renderer2
	) { }

	ngOnInit() {
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		console.log('Width: ' + width);
		this.checkWindowSize(width);

		// links
		this.links = [
			{
				title: 'Cars I Have Bought',
				url: 'auto/car-i-have-bought'
			},
			{
				title: 'My Warehouse',
				url: 'auto/my-warehouse'
			}
		];
	}

	// check window size
	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		console.log('Width: ' + event.target.innerWidth);
		this.checkWindowSize(event.target.innerWidth);
	}

}
