import { Component, OnInit, Renderer2, NgZone, HostListener, AfterViewInit, ViewChild } from '@angular/core';
import { WalkthroughText, WalkthroughComponent, WalkthroughFlowComponent } from 'angular-walkthrough';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { RegistrationService } from '../../../services/registration.service';

@Component({
	selector: 'app-buy-a-car-for-me',
	templateUrl: './buy-a-car-for-me.component.html',
	styleUrls: ['./buy-a-car-for-me.component.scss']
})
export class BuyACarForMeComponent implements OnInit, AfterViewInit {
	isMediumScreen: boolean;
	constructor(
		private toastr: ToastrService,
		private renderer: Renderer2,
		private router: Router,
		public regService: RegistrationService,
	) { }
	@ViewChild('walkCar') walkRef: WalkthroughFlowComponent;

	// WalkthroughText
	navText: WalkthroughText = {
		previous: 'Previous',
		next: 'Next',
		close: 'Finish'
	};

	lastNavText: WalkthroughText = {
		previous: 'Previous',
		next: 'Next',
		close: 'Next'
	};

	ngAfterViewInit(): void {
		WalkthroughComponent.walkthroughContinue();
		setTimeout(() => {
			this.walkRef.start();
		}, 500);
	}

	ngOnInit() {
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		console.log('Width: ' + width);
		this.checkWindowSize(width);
	}

	showSuccess() {
		// this.toastr.success('Hello world!', 'Toastr fun!');
	}

	// check window size
	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		console.log('Width: ' + event.target.innerWidth);
		this.checkWindowSize(event.target.innerWidth);
	}

	walkCarFinished() {
		this.router.navigateByUrl('/virtual-tour/auto/checkout');
	}

	pause() {
		//console.log('skipped');
		this.regService.updateVirtualtourlog('skip','auto','0').subscribe((data:any) => {
    		if(data.status == 1)
    		{
    			WalkthroughComponent.walkthroughStop();
    			let resumeUrl = JSON.parse(localStorage.getItem('virtualTourResumeUrl'));
				this.router.navigateByUrl(resumeUrl);
    		}


    	});
	}
}
