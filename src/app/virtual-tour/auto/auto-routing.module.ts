import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AutoComponent } from './auto.component';
import { BuyACarForMeComponent } from './buy-a-car-for-me/buy-a-car-for-me.component';
import { ShipMyCarComponent } from './ship-my-car/ship-my-car.component';
import { CarIHaveBoughtComponent } from './car-i-have-bought/car-i-have-bought.component';
import { AutoCheckoutComponent } from './auto-checkout/auto-checkout.component';
import { AutoMyWarehouseComponent } from './auto-my-warehouse/auto-my-warehouse.component';


const routes: Routes = [
	{
		path: '', redirectTo: 'buy-a-car-for-me', pathMatch: 'full'
	},
	{
		path: '',
		component: AutoComponent,
		children: [
			{ path: 'buy-a-car-for-me', component: BuyACarForMeComponent },
			{ path: 'ship-my-car', component: ShipMyCarComponent },
			{ path: 'car-i-have-bought', component: CarIHaveBoughtComponent },
			{ path: 'my-warehouse', component: AutoMyWarehouseComponent },
			{ path: 'checkout', component: AutoCheckoutComponent }
		]
	}

];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class AutoRoutingModule {

}
