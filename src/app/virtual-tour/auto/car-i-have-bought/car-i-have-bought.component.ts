import { Component, OnInit, Renderer2, NgZone, HostListener, AfterViewInit, ViewChild } from '@angular/core';
import { WalkthroughText, WalkthroughComponent, WalkthroughFlowComponent } from 'angular-walkthrough';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { RegistrationService } from '../../../services/registration.service';

@Component({
	selector: 'app-car-i-have-bought',
	templateUrl: './car-i-have-bought.component.html',
	styleUrls: ['./car-i-have-bought.component.scss']
})
export class CarIHaveBoughtComponent implements OnInit, AfterViewInit {
	tracks: Array<any>;
	isMediumScreen: boolean;
	links: Array<any>;
	constructor(
		private toastr: ToastrService,
		private renderer: Renderer2,
		private router: Router,
		public regService : RegistrationService,
	) { }
	@ViewChild('walkCarBought') walkRef: WalkthroughFlowComponent;

	// WalkthroughText
	navText: WalkthroughText = {
		previous: 'Previous',
		next: 'Next',
		close: 'Finish'
	};

	lastNavText: WalkthroughText = {
		previous: 'Previous',
		next: 'Next',
		close: 'Next'
	};

	ngAfterViewInit(): void {
		setTimeout(() => {
			this.walkRef.start();
		}, 500);
	}
	ngOnInit() {
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		console.log('Width: ' + width);
		this.checkWindowSize(width);

		this.tracks = [
			{
				title: 'Submitted',
				date: '07-02-2018',
				completed: true,
				visited: false,
			},
			{
				title: 'Order Placed',
				date: '07-02-2018',
				completed: true,
				visited: false,
			},
			{
				title: 'Picked Up',
				date: '07-02-2018',
				completed: true,
				visited: false,
			},
			{
				title: 'Received',
				date: '07-02-2018',
				completed: false,
				visited: true,
			}
		];

		// links
		this.links = [
			{
				id: 'carIHave',
				title: 'Cars I Have Bought',
				url: 'virtual-tour/auto/car-i-have-bought'
			},
			{
				id: 'myWarehouse',
				title: 'My Warehouse',
				url: 'virtual-tour/auto/my-warehouse'
			}
		];
	}

	walkCarBoughtFinished() {
		/*this.regService.virtualTouFinished().subscribe((data: any) => {
	 		if (data.status == '1') {
	 			let userData = JSON.parse(localStorage.getItem('tokens'));
	 			userData.user.virtualTourVisited = "1";
	 			localStorage.setItem('tokens', JSON.stringify(userData));
	 			this.regService.item.user.virtualTourVisited = "1";

	 			this.router.navigateByUrl('/my-account/account-detail');
	 		}
	 	});*/
	 	this.regService.updateVirtualtourlog('finish','auto','1').subscribe((data:any) => {
			if(data.status == '1')
			{
				let resumeUrl = JSON.parse(localStorage.getItem('virtualTourResumeUrl'));
				this.router.navigateByUrl(resumeUrl);
			}
		});
	}

	// check window size
	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		console.log('Width: ' + event.target.innerWidth);
		this.checkWindowSize(event.target.innerWidth);
	}

	showSuccess() {
		this.toastr.success('Hello world!', 'Toastr fun!');
	}

	pause() {
		//console.log('skipped');
		this.regService.updateVirtualtourlog('skip','auto','0').subscribe((data:any) => {
    		if(data.status == 1)
    		{
    			WalkthroughComponent.walkthroughStop();
    			let resumeUrl = JSON.parse(localStorage.getItem('virtualTourResumeUrl'));
				this.router.navigateByUrl(resumeUrl);
    		}


    	});
	}
}
