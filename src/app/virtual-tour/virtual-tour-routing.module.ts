import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { VirtualTourComponent } from './virtual-tour.component';
// import { MyWarehouseComponent } from './my-warehouse/my-warehouse.component';

const routes: Routes = [
	{ path: '', redirectTo: 'my-account', pathMatch: 'full' },
	{ path: 'my-warehouse', loadChildren: './my-warehouse/my-warehouse.module#MyWarehouseModule' },
	{ path: 'my-account', loadChildren: './my-account/my-account.module#MyAccountModule' },
	{ path: 'auto', loadChildren: './auto/auto.module#AutoModule' },
	{ path: 'shop-for-me', loadChildren: './shop-for-me/sfm.module#SFMModule' },
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class VirtualTourRoutingModule {

}
