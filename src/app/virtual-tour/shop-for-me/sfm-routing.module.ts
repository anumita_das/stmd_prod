import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SFMComponent } from './sfm.component';
import { HowItWorkComponent } from './how-it-work/how-it-work.component';
import { OrderFormComponent } from './order-form/order-form.component';
import { MyCartComponent } from './my-cart/my-cart.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { CartComponent } from './checkout/cart/cart.component';
import { PersonalDetailComponent } from './checkout/personal-detail/personal-detail.component';
import { ShippingAndPaymentComponent } from './checkout/shipping-and-payment/shipping-and-payment.component';
import { PlaceOrderComponent } from './checkout/place-order/place-order.component';
import { OrderHistoryComponent } from './order-history/order-history.component';


const routes: Routes = [
	{
		path: '', redirectTo: 'how-it-work'
	},
	{
		path: '',
		component: SFMComponent,
		children: [
			{ path: 'how-it-work', component: HowItWorkComponent },
			{ path: 'order-form', component: OrderFormComponent },
			{ path: 'my-cart', component: MyCartComponent },
			{
				path: 'checkout',
				component: CheckoutComponent,
				children: [
					{ path: '', redirectTo: 'cart', pathMatch: 'full' },
					{ path: 'cart', component: CartComponent },
					{ path: 'personal-detail', component: PersonalDetailComponent },
					{ path: 'shipping-and-payment', component: ShippingAndPaymentComponent },
					{ path: 'place-order', component: PlaceOrderComponent }
				]
			},
			{ path: 'order-history', component: OrderHistoryComponent }
		]
	}

];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class SFMRoutingModule {

}
