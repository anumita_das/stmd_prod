import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-my-cart',
	templateUrl: './my-cart.component.html',
	styleUrls: ['./my-cart.component.scss']
})
export class MyCartComponent implements OnInit {
	isActive: boolean;
	selectedAll: any;
	carts: Array<any>;
	names: any;
	check_all;
	constructor() {
		this.check_all = {
			type: false
		};
	}

	ngOnInit() {
		this.carts = [
			{ selected: false },
			{ selected: true },
			{ selected: false },
			{ selected: false },
			{ selected: false }
		];
		// console.log(this.selectedAll);

	}
	// checkUncheckAll(ev) {
	// 	if (ev.target.checked) {
	// 		console.log('true');
	// 	} else {
	// 		console.log('false');
	// 	}
	// }

	// selectAll() {
	// 	for (let i = 0; i < this.carts.length; i++) {
	// 		this.carts[i].selected = this.selectedAll;
	// 	}
	// }
	// checkIfAllSelected() {
	// 	this.selectedAll = this.carts.every(function (item: any) {
	// 		return item.selected = true;
	// 	});
	// 	this.isActive = !this.isActive;
	// }

	selectAll(space) {
		if (space === 'type') {
			if (this.check_all.type === true) {
				this.carts.forEach(type => {
					type.selected = false;
				});
			} else {
				this.carts.forEach(type => {
					type.selected = true;
				});
			}
		}
		this.filterSearch('');
	}

	filterSearch(selected) {
		for (const type of this.carts) {
			if (!type.selected) {
				this.check_all.type = false;
				break;
			} else {
				this.check_all.type = true;
			}
		}
	}

}
