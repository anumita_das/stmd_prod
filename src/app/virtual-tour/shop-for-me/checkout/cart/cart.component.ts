import { Component, OnInit, NgZone, HostListener, AfterViewInit, ViewChild } from '@angular/core';
import { WalkthroughText, WalkthroughComponent, WalkthroughFlowComponent } from 'angular-walkthrough';
import { RegistrationService } from '../../../../services/registration.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-cart',
	templateUrl: './cart.component.html',
	styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit, AfterViewInit {
	dtOptions: DataTables.Settings = {};
	constructor(
		private router: Router,
		public regService: RegistrationService,
	) { }
	
	@ViewChild('walkYourCart') walkRef: WalkthroughFlowComponent;

	// WalkthroughText
	navText: WalkthroughText = {
		previous: 'Previous',
		next: 'Next',
		close: 'Finish'
	};

	lastNavText: WalkthroughText = {
		previous: 'Previous',
		next: 'Next',
		close: 'Next'
	};


	ngAfterViewInit(): void {
		setTimeout(() => {
			this.walkRef.start();
		}, 500);
	}

	ngOnInit() {
		// datatable
		this.dtOptions = {
			paging: false,
			// bInfo: false,
			ordering: false,
			searching: false,
			// bSort: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};
	}

	walkYourCartFinished() {
		this.router.navigateByUrl('/virtual-tour/shop-for-me/checkout/personal-detail');
	}


	pause() {
		this.regService.updateVirtualtourlog('skip','shop-for-me','0').subscribe((data:any) => {
    		if(data.status == 1)
    		{
    			WalkthroughComponent.walkthroughStop();
    			let resumeUrl = JSON.parse(localStorage.getItem('virtualTourResumeUrl'));
				this.router.navigateByUrl(resumeUrl);
    		}

    	});
	}

}
