import { Component, OnInit, NgZone, HostListener, AfterViewInit, ViewChild } from '@angular/core';
import { WalkthroughText, WalkthroughComponent, WalkthroughFlowComponent } from 'angular-walkthrough';
import { RegistrationService } from '../../../../services/registration.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-shipping-and-payment',
	templateUrl: './shipping-and-payment.component.html',
	styleUrls: ['./shipping-and-payment.component.scss']
})
export class ShippingAndPaymentComponent implements OnInit, AfterViewInit {
	constructor(
		private router: Router,
		public regService: RegistrationService,
	) { }

	@ViewChild('walkShippingAndPayment') walkRef: WalkthroughFlowComponent;
	// WalkthroughText
	navText: WalkthroughText = {
		previous: 'Previous',
		next: 'Next',
		close: 'Finish'
	};

	lastNavText: WalkthroughText = {
		previous: 'Previous',
		next: 'Next',
		close: 'Next'
	};

	ngAfterViewInit(): void {
		setTimeout(() => {
			this.walkRef.start();
		}, 500);
	}
	ngOnInit() {

	}

	walkShippingAndPaymentFinished() {
		this.router.navigateByUrl('/virtual-tour/shop-for-me/checkout/place-order');
	}

	pause() {
		this.regService.updateVirtualtourlog('skip','shop-for-me','0').subscribe((data:any) => {
    		if(data.status == 1)
    		{
    			WalkthroughComponent.walkthroughStop();
    			let resumeUrl = JSON.parse(localStorage.getItem('virtualTourResumeUrl'));
				this.router.navigateByUrl(resumeUrl);
    		}

    	});
	}
}
