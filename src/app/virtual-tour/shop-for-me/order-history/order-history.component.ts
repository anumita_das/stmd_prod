import { Component, OnInit, NgZone, HostListener, AfterViewInit, ViewChild } from '@angular/core';
import { WalkthroughText, WalkthroughComponent, WalkthroughFlowComponent } from 'angular-walkthrough';
import { Router } from '@angular/router';
import { RegistrationService } from '../../../services/registration.service';
import { AppGlobals } from '../../../app.globals';

@Component({
	selector: 'app-order-history',
	templateUrl: './order-history.component.html',
	styleUrls: ['./order-history.component.scss']
})
export class OrderHistoryComponent implements OnInit, AfterViewInit {
	isMediumScreen: boolean;
	dtOptions: DataTables.Settings = {};
	showMoreOptions = false;
	selectedAll: any;
	orders: Array<any>;
	names: any;
	isPeriodDisabled: boolean;
	check_all;
	showHistory: boolean;
	constructor(
		private router: Router,
		public regService : RegistrationService,
		public _global: AppGlobals,
	) {
		this.check_all = {
			type: false
		};
	}


	@ViewChild('walkOrderHistory') walkRef: WalkthroughFlowComponent;

	// WalkthroughText
	navText: WalkthroughText = {
		previous: 'Previous',
		next: 'Next',
		close: 'Finish'
	};

	lastNavText: WalkthroughText = {
		previous: 'Previous',
		next: 'Next',
		close: 'Next'
	};

	ngAfterViewInit(): void {
		setTimeout(() => {
			this.walkRef.start();
		}, 500);
	}

	ngOnInit() {
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		console.log('Width: ' + width);
		this.checkWindowSize(width);

		this.orders = [
			{
				selected: false,
				id: '114593',
				urgent: 'N',
				shop_costg: '54.00',
				shipping_cost: '11.34',
				total_cost: '65.91',
				status: 'Submitted',
				payment_status: 'Paid',
				date: '10/18/2018'
			},
			{
				selected: false,
				id: '114593',
				urgent: 'N',
				shop_costg: '54.00',
				shipping_cost: '11.34',
				total_cost: '65.91',
				status: 'Submitted',
				payment_status: 'Paid',
				date: '10/18/2018'
			},
			{
				selected: false,
				id: '114593',
				urgent: 'N',
				shop_costg: '54.00',
				shipping_cost: '11.34',
				total_cost: '65.91',
				status: 'Submitted',
				payment_status: 'Paid',
				date: '10/18/2018'
			},
			{
				selected: false,
				id: '114593',
				urgent: 'N',
				shop_costg: '54.00',
				shipping_cost: '11.34',
				total_cost: '65.91',
				status: 'Submitted',
				payment_status: 'Paid',
				date: '10/18/2018'
			},
			{
				selected: false,
				id: '114593',
				urgent: 'N',
				shop_costg: '54.00',
				shipping_cost: '11.34',
				total_cost: '65.91',
				status: 'Submitted',
				payment_status: 'Paid',
				date: '10/18/2018'
			},
			{
				selected: false,
				id: '114593',
				urgent: 'N',
				shop_costg: '54.00',
				shipping_cost: '11.34',
				total_cost: '65.91',
				status: 'Submitted',
				payment_status: 'Paid',
				date: '10/18/2018'
			},
			{
				selected: false,
				id: '114593',
				urgent: 'N',
				shop_costg: '54.00',
				shipping_cost: '11.34',
				total_cost: '65.91',
				status: 'Submitted',
				payment_status: 'Paid',
				date: '10/18/2018'
			}
		];

		// datatable
		this.dtOptions = {
			paging: false,
			// bInfo: false,
			ordering: false,
			searching: false,
			// bSort: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};
	}

	showMore(event) {
		this.showMoreOptions = !this.showMoreOptions;
	}

	// openHistory
	openHistory() {
		this.showHistory = true;
	}

	walkOrderHistoryFinished() {
		//this.router.navigateByUrl('/virtual-tour/my-warehouse');
		this.regService.updateVirtualtourlog('finish','shop-for-me','1').subscribe((data:any) => {
			if(data.status == '1')
			{
				let resumeUrl = JSON.parse(localStorage.getItem('virtualTourResumeUrl'));
				//this.router.navigateByUrl(resumeUrl);
				this.router.navigateByUrl('shop-from');
			}
		});
	}


	selectAll(space) {
		if (space === 'type') {
			if (this.check_all.type === true) {
				this.orders.forEach(type => {
					type.selected = false;
				});
			} else {
				this.orders.forEach(type => {
					type.selected = true;
				});
			}
		}
		this.filterSearch('');
	}

	filterSearch(selected) {
		for (const type of this.orders) {
			if (!type.selected) {
				this.check_all.type = false;
				break;
			} else {
				this.check_all.type = true;
			}
		}
	}

	// modal photo shot
	enableRange(event) {
		if (event === 'FR') {
			this.isPeriodDisabled = true;
		} else {
			this.isPeriodDisabled = false;
		}
	}

	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		// console.log('Width: ' + event.target.innerWidth);
		this.checkWindowSize(event.target.innerWidth);
	}

	pause() {
		//console.log('skipped');
		this.regService.updateVirtualtourlog('skip','shop-for-me','0').subscribe((data:any) => {
    		if(data.status == 1)
    		{
    			WalkthroughComponent.walkthroughStop();
    			let resumeUrl = JSON.parse(localStorage.getItem('virtualTourResumeUrl'));
				this.router.navigateByUrl(resumeUrl);
    		}


    	});
	}


}
