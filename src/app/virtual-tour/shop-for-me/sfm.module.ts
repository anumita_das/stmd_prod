import { NgModule } from '@angular/core';
import { SFMRoutingModule } from './sfm-routing.module';
import { SFMComponent } from './sfm.component';
import { SharedModule } from '@app/shared';
import { HowItWorkComponent } from './how-it-work/how-it-work.component';
import { OrderFormComponent } from './order-form/order-form.component';
import { MyCartComponent } from './my-cart/my-cart.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { CartComponent } from './checkout/cart/cart.component';
import { PersonalDetailComponent } from './checkout/personal-detail/personal-detail.component';
import { ShippingAndPaymentComponent } from './checkout/shipping-and-payment/shipping-and-payment.component';
import { PlaceOrderComponent } from './checkout/place-order/place-order.component';
import { OrderHistoryComponent } from './order-history/order-history.component';

@NgModule({
	imports: [
		SFMRoutingModule,
		SharedModule
	],
	declarations: [
		SFMComponent,
		HowItWorkComponent,
		OrderFormComponent,
		MyCartComponent,
		CheckoutComponent,
		CartComponent,
		PersonalDetailComponent,
		ShippingAndPaymentComponent,
		PlaceOrderComponent,
		OrderHistoryComponent
	]
})
export class SFMModule {

}
