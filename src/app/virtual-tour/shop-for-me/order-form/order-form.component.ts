import { Component, OnInit, HostListener, AfterViewInit, ViewChild } from '@angular/core';
import { WalkthroughText, WalkthroughComponent, WalkthroughFlowComponent } from 'angular-walkthrough';
import { Router } from '@angular/router';
import { RegistrationService } from '../../../services/registration.service';

@Component({
	selector: 'app-order-form',
	templateUrl: './order-form.component.html',
	styleUrls: ['./order-form.component.scss']
})
export class OrderFormComponent implements OnInit, AfterViewInit {
	isMediumScreen: boolean;
	sidebar: boolean;
	showShipping: boolean;
	shippingTypes: Array<any>;
	dtOptions: DataTables.Settings = {};
	rowArray: Array<any> = [];
	private newAttribute: any = {};
	row: any = 1;
	constructor(
		private router: Router,
		public regService: RegistrationService,
	) {
		this.rowArray.push({ row: this.row });
	}
	
	@ViewChild('walkShopForMe') walkRef: WalkthroughFlowComponent;

	// WalkthroughText
	navText: WalkthroughText = {
		previous: 'Previous',
		next: 'Next',
		close: 'Finish'
	};

	lastNavText: WalkthroughText = {
		previous: 'Previous',
		next: 'Next',
		close: 'Next'
	};

	ngAfterViewInit(): void {
		setTimeout(() => {
			this.walkRef.start();
		}, 500);
	}

	ngOnInit() {
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		this.checkWindowSize(width);


		// shippingTypes
		this.shippingTypes = [
			{
				id: 'standard',
				icon: 'logo.png',
				title: 'Standard Shipping',
				time: '6 - 11 Busi Days',
				date: 'Get it by Thu, Aug 2nd',
				shipping_cost: '$22.00',
				clearing: '$8.05',
				price: '$33.27',
				details: 'Shoptomydoor on behalf of its partners will handle all clearing, documentation and delivery to your door. Shipment departs every Thursday from the US and delivery time counts from the Thursday when shipments departs. Payment must be made by 12 noon on Wednesday for the shipment to meet delivery for that week.'
			},
			{
				id: 'standardSea',
				icon: 'ship.png',
				title: 'Standard Shipping (Sea)',
				time: '6 - 10 Weeks',
				date: 'Get it by Tue, Sep 25th',
				shipping_cost: '$22.00',
				clearing: '$8.05',
				price: '$33.27',
				details: 'Shoptomydoor on behalf of its partners will handle all clearing, documentation and delivery to your door.'
			},
			{
				id: 'express',
				icon: 'dhl.jpg',
				title: 'Express Shipping (DHL)',
				time: '2 - 4 Busi Days',
				date: 'Get it by Mon, Jul 23rd',
				shipping_cost: '$22.00',
				clearing: '$8.05',
				price: '$33.27',
				details: 'Delivery, clearing and documentation is handled by DHL directly, and by using this option, you authorize them to handle your shipment. DHL will contact you directly if needed and you will relate directly with them.'
			}
		];

		// datatable
		this.dtOptions = {
			paging: false,
			// bInfo: false,
			ordering: false,
			searching: false,
			// bSort: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};
	}


	// show shipping type
	showShippingType() {
		this.showShipping = true;
	}

	// addRow
	addRow() {
		this.row = this.row + 1;
		this.rowArray.push({ row: this.row });
		// this.newAttribute = {};
		console.log(this.rowArray);

	}

	// deleteRow
	deleteRow(index) {
		this.rowArray.splice(index, 1);
		console.log(this.rowArray, index);
	}

	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		// console.log('Width: ' + event.target.innerWidth);
		this.checkWindowSize(event.target.innerWidth);
	}


	walkShopForMeFinished() {
		this.router.navigateByUrl('/virtual-tour/shop-for-me/checkout');
	}

	pause() {
		//console.log('skipped');
		this.regService.updateVirtualtourlog('skip','shop-for-me','0').subscribe((data:any) => {
    		if(data.status == 1)
    		{
    			WalkthroughComponent.walkthroughStop();
    			let resumeUrl = JSON.parse(localStorage.getItem('virtualTourResumeUrl'));
				this.router.navigateByUrl(resumeUrl);
    		}


    	});
	}



}
