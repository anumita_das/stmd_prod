import { Component, OnInit, HostListener, AfterViewInit, ViewChild } from '@angular/core';
import { WalkthroughText, WalkthroughComponent } from 'angular-walkthrough';
import { Router } from '@angular/router';
import { RegistrationService } from '../../../services/registration.service';

@Component({
	selector: 'app-how-it-work',
	templateUrl: './how-it-work.component.html',
	styleUrls: ['./how-it-work.component.scss']
})
export class HowItWorkComponent implements OnInit, AfterViewInit {
	isMediumScreen: boolean;
	constructor(
		private router: Router,
		public regService: RegistrationService,
	) { }

	@ViewChild('walkHSW') walkRef: WalkthroughComponent;

	// WalkthroughText
	navText: WalkthroughText = {
		previous: 'Previous',
		next: 'Next',
		close: 'Finish'
	};

	lastNavText: WalkthroughText = {
		previous: 'Previous',
		next: 'Next',
		close: 'Next'
	};

	ngAfterViewInit(): void {
		WalkthroughComponent.walkthroughContinue();
		setTimeout(() => {
			this.walkRef.open();
		}, 500);
	}

	ngOnInit() {
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		this.checkWindowSize(width);
	}

	walkHSWFinished() {
		this.router.navigateByUrl('/virtual-tour/shop-for-me/order-form');
	}

	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		this.checkWindowSize(event.target.innerWidth);
	}

	aaa() {
		WalkthroughComponent.walkthroughStop();
		console.log('aaa');

		this.router.navigateByUrl('/my-account/account-detail');
	}

	pause() {
		//console.log('skipped');
		this.regService.updateVirtualtourlog('skip','shop-for-me','0').subscribe((data:any) => {
    		if(data.status == 1)
    		{
    			WalkthroughComponent.walkthroughStop();
    			let resumeUrl = JSON.parse(localStorage.getItem('virtualTourResumeUrl'));
				this.router.navigateByUrl(resumeUrl);
    		}


    	});
	}
}
