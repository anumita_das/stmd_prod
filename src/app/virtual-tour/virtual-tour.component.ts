import { Component, OnInit, NgZone, HostListener } from '@angular/core';
// import { BsModalService } from 'ngx-bootstrap/modal';
// import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
// import { AddressbookModalComponent } from './addressbook-modal/addressbook-modal.component';
// import { AddressbookModalComponent } from 'addre';

@Component({
	selector: 'app-virtual-tour',
	templateUrl: './virtual-tour.component.html',
	styleUrls: ['./virtual-tour.component.scss']
})
export class VirtualTourComponent implements OnInit {
	isMediumScreen: boolean;
	sidebar: boolean;
	isCollapsed: boolean;
	// bsModalRef: BsModalRef;
	constructor(
		private ngZone: NgZone,
		// private modalService: BsModalService
	) { }

	ngOnInit() {
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		console.log('Width: ' + width);
		this.checkWindowSize(width);

		// account isCollapsed
		this.isCollapsed = false;
	}

	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		console.log('Width: ' + event.target.innerWidth);
		this.checkWindowSize(event.target.innerWidth);
	}

	// openAddressModal() {
	// 	this.bsModalRef = this.modalService.show(AddressbookModalComponent, {
	// 		class: 'modal-xs modal-dialog-centered'

	// 	});
	// 	this.bsModalRef.content.closeBtnName = 'Close';
	// }

}
