import { Component, OnInit, NgZone, HostListener, AfterViewInit, ViewChild } from '@angular/core';
import { WalkthroughText, WalkthroughComponent, WalkthroughFlowComponent } from 'angular-walkthrough';
import { Router } from '@angular/router';
import { RegistrationService } from '../../../../services/registration.service';

@Component({
	selector: 'app-place-order',
	templateUrl: './place-order.component.html',
	styleUrls: ['./place-order.component.scss']
})
export class PlaceOrderComponent implements OnInit, AfterViewInit {
	dtOptions: DataTables.Settings = {};
	constructor(
		private router: Router,
		public regService : RegistrationService,
	) { }
	@ViewChild('walkWarehousePlaceOrder') walkRef: WalkthroughFlowComponent;
	// WalkthroughText
	navText: WalkthroughText = {
		previous: 'Previous',
		next: 'Next',
		close: 'Finish'
	};

	lastNavText: WalkthroughText = {
		previous: 'Previous',
		next: 'Next',
		close: 'Next'
	};

	ngAfterViewInit(): void {
		setTimeout(() => {
			this.walkRef.start();
		}, 500);
	}

	ngOnInit() {
		// datatable
		this.dtOptions = {
			paging: false,
			// bInfo: false,
			ordering: false,
			searching: false,
			// bSort: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};
	}

	walkWarehousePlaceOrderFinished() {
		this.regService.updateVirtualtourlog('finish','my-warehouse','1').subscribe((data:any) => {
			if(data.status == '1')
			{
				let resumeUrl = JSON.parse(localStorage.getItem('virtualTourResumeUrl'));
				this.router.navigateByUrl(resumeUrl);
			}
		});
		//this.router.navigateByUrl('/virtual-tour/my-account/order-history');
	}

	pause() {
		this.regService.updateVirtualtourlog('skip','shop-for-me','0').subscribe((data:any) => {
    		if(data.status == 1)
    		{
    			WalkthroughComponent.walkthroughStop();
    			let resumeUrl = JSON.parse(localStorage.getItem('virtualTourResumeUrl'));
				this.router.navigateByUrl(resumeUrl);
    		}
    	});
	}
}
