import { NgModule } from '@angular/core';
import { MyWarehouseRoutingModule } from './my-warehouse-routing.module';
import { MyWarehouseComponent } from './my-warehouse.component';
import { SharedModule } from '@app/shared';
import { OverviewComponent } from './overview/overview.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { CartComponent } from './checkout/cart/cart.component';
import { PersonalDetailComponent } from './checkout/personal-detail/personal-detail.component';
import { ShippingAndPaymentComponent } from './checkout/shipping-and-payment/shipping-and-payment.component';
import { PlaceOrderComponent } from './checkout/place-order/place-order.component';

@NgModule({
	imports: [
		MyWarehouseRoutingModule,
		SharedModule
	],
	declarations: [
		MyWarehouseComponent,
		OverviewComponent,
		CheckoutComponent,
		CartComponent,
		PersonalDetailComponent,
		ShippingAndPaymentComponent,
		PlaceOrderComponent
	]
})
export class MyWarehouseModule {

}
