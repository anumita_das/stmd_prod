import { Component, OnInit, NgZone, HostListener, AfterViewInit, ViewChild } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { RecipientModalComponent } from '../../../shared/components/recipient-modal/recipient-modal.component';
import { PhotoShotModalComponent } from '../../../shared/components/photo-shot-modal/photo-shot-modal.component';
import { RecountModalComponent } from '../../../shared/components/recount-modal/recount-modal.component';
import { ReweighModalComponent } from '../../../shared/components/reweigh-modal/reweigh-modal.component';
import { WalkthroughText, WalkthroughComponent, WalkthroughFlowComponent } from 'angular-walkthrough';
import { Router } from '@angular/router';
import { RegistrationService } from '../../../services/registration.service';

@Component({
	selector: 'app-overview',
	templateUrl: './overview.component.html',
	styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit, AfterViewInit {
	isMediumScreen: boolean;
	sidebar: boolean;
	isCollapsed: boolean;
	bsModalRef: BsModalRef;
	rows: Array<any>;
	columns: Array<any>;
	dtOptions: DataTables.Settings = {};
	isPhotoShotDisabled: boolean;
	isRecountDisabled: boolean;
	isReweighDisabled: boolean;
	showShippingCost: boolean;
	shippingTypes: Array<any>;
	constructor(
		private ngZone: NgZone,
		private modalService: BsModalService,
		private router: Router,
		public regService: RegistrationService,
	) { }

	@ViewChild('walkWarehouseOverview') walkRef: WalkthroughFlowComponent;

	// WalkthroughText
	navText: WalkthroughText = {
		previous: 'Previous',
		next: 'Next',
		close: 'Finish'
	};

	lastNavText: WalkthroughText = {
		previous: 'Previous',
		next: 'Next',
		close: 'Next'
	};

	ngAfterViewInit(): void {
		WalkthroughComponent.walkthroughContinue();
		setTimeout(() => {
			this.walkRef.start();
		}, 500);
	}

	walkWarehouseOverviewFinished() {
		this.router.navigateByUrl('/virtual-tour/my-warehouse/checkout');
	}

	ngOnInit() {
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		console.log('Width: ' + width);
		this.checkWindowSize(width);

		// account isCollapsed
		this.isCollapsed = false;

		this.rows = [
			{ name: 'Austin', gender: 'Male', company: 'Swimlane' },
			{ name: 'Dany', gender: 'Male', company: 'KFC' },
			{ name: 'Molly', gender: 'Female', company: 'Burger King' },
		];
		this.columns = [
			{ prop: 'name' },
			{ name: 'Gender' },
			{ name: 'Company' }
		];

		// datatable
		this.dtOptions = {
			paging: false,
			// bInfo: false,
			ordering: false,
			searching: false,
			// bSort: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};

		// shippingTypes
		this.shippingTypes = [
			{
				icon: 'logo.png',
				title: 'Standard Shipping',
				time: '6 - 11 Busi Days',
				date: 'Get it by Thu, Aug 2nd',
				shipping_cost: '$22.00',
				clearing: '$8.05',
				price: '$33.27',
				delivery_cost: '$53.06/pound',
				details: 'Shoptomydoor on behalf of its partners will handle all clearing, documentation and delivery to your door. Shipment departs every Thursday from the US and delivery time counts from the Thursday when shipments departs. Payment must be made by 12 noon on Wednesday for the shipment to meet delivery for that week.'
			},
			{
				icon: 'ship.png',
				title: 'Standard Shipping (Sea)',
				time: '6 - 10 Weeks',
				date: 'Get it by Tue, Sep 25th',
				shipping_cost: '$22.00',
				clearing: '$8.05',
				price: '$33.27',
				delivery_cost: '$53.06/pound',
				details: 'Shoptomydoor on behalf of its partners will handle all clearing, documentation and delivery to your door.'
			},
			{
				icon: 'dhl.jpg',
				title: 'Express Shipping (DHL)',
				time: '2 - 4 Busi Days',
				date: 'Get it by Mon, Jul 23rd',
				shipping_cost: '$22.00',
				clearing: '$8.05',
				price: '$33.27',
				delivery_cost: '$53.06/pound',
				details: 'Delivery, clearing and documentation is handled by DHL directly, and by using this option, you authorize them to handle your shipment. DHL will contact you directly if needed and you will relate directly with them.'
			},
			{
				icon: 'ship.png',
				title: 'Ocean Priority',
				time: '6 - 10 Weeks',
				date: 'Get it by Tue, Sep 25th',
				shipping_cost: '$22.00',
				clearing: '$8.05',
				price: '$33.27',
				delivery_cost: '$53.06/pound',
				details: 'Shoptomydoor on behalf of its partners will handle all clearing, documentation and delivery to your door.'
			}
		];
	}

	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		console.log('Width: ' + event.target.innerWidth);
		this.checkWindowSize(event.target.innerWidth);
	}

	// modal
	openRecipientModal() {
		this.bsModalRef = this.modalService.show(RecipientModalComponent, {
			class: 'modal-xs modal-dialog-centered'

		});
		this.bsModalRef.content.closeBtnName = 'Close';
	}
	// modal photo shot
	openPhotoShotModal(event) {
		if (event === '1') {
			this.bsModalRef = this.modalService.show(PhotoShotModalComponent, {
				class: 'modal-sm modal-dialog-centered'

			});
			this.bsModalRef.content.closeBtnName = 'Close';
			console.log('open');
			this.isPhotoShotDisabled = true;

		}
	}

	// modal recount
	openRecountModal(event) {
		if (event === '1') {
			this.bsModalRef = this.modalService.show(RecountModalComponent, {
				class: 'modal-sm modal-dialog-centered'

			});
			this.bsModalRef.content.closeBtnName = 'Close';
			console.log('open');
			this.isRecountDisabled = true;

		}
	}

	// modal reweigh
	openReweighModal(event) {
		if (event === '1') {
			this.bsModalRef = this.modalService.show(ReweighModalComponent, {
				class: 'modal-sm modal-dialog-centered'

			});
			this.bsModalRef.content.closeBtnName = 'Close';
			console.log('open');
			this.isReweighDisabled = true;

		}
	}

	pause() {
		this.regService.updateVirtualtourlog('skip','shop-for-me','0').subscribe((data:any) => {
    		if(data.status == 1)
    		{
    			WalkthroughComponent.walkthroughStop();
    			let resumeUrl = JSON.parse(localStorage.getItem('virtualTourResumeUrl'));
				this.router.navigateByUrl(resumeUrl);
    		}
    	});
	}


}
