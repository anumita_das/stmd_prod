import { Component, OnInit, NgZone, HostListener } from '@angular/core';
import { NgForm }   from '@angular/forms';
import { AppGlobals } from '../../../app.globals';
import { Meta, Title } from '@angular/platform-browser';
import { AutoService } from '../../../shared/services/auto.service';
import { ContentService } from '../../../services/content.service';
import { RegistrationService } from '../../../services/registration.service';
import { ShopformeService } from '../../../services/shopforme.service';
import { Router } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { StorefromModalComponent } from '../../../shared/components/storefrom-modal/storefrom-modal.component';

@Component({
	selector: 'app-order-form',
	templateUrl: './order-form.component.html',
	styleUrls: ['./order-form.component.scss'],
	providers: [AppGlobals]
})
export class OrderFormComponent implements OnInit {
	isMediumScreen: boolean;
	sidebar: boolean;
	showShipping: boolean;
	warehouseCode : any;
	shippingTypes: Array<any>;
	dtOptions: DataTables.Settings = {};
	bagList : Array<any> = [];
	rowArray: Array<any> = [];

	row: any = 1;
	formSubmit : boolean;
	productAdded : boolean;
	userId : number;
	paymentDetails : Array<any>;
	defaultCurrencySymbol : any;
	selectedFile : any;
	isStoreAutoParts: boolean = false;
	bsModalRef: BsModalRef;


	autoparts =  {
		warehouseId : "",
		urgent : "N",
		storeId : "",
		siteCategoryId : "",
		siteSubCategoryId : "",
		siteProductId : "",
		siteProductImage : "",
		websiteUrl : "",
		itemName : "",
		itemDescription : "",
		itemYear: 0,
		itemMake:"",
		itemModel:"",
		itemPrice : "",
		itemQuantity : "",
		itemShippingCost : "",
		itemImage : "",
	};

	private newAttribute: any = {};
	public warehouseList : any;
	public categoryList : any;
	public subcategoryList : any;
	public productList : any;
	public yearList : any;
	public storeList : any;
	

	constructor(
		private ngZone: NgZone,
		private _global: AppGlobals,
		private meta: Meta, 
		private title: Title,
		public auto: AutoService,
		public content : ContentService,
		public regService : RegistrationService,
		public shopformeService : ShopformeService,
		private router: Router,
		private toastr: ToastrService,
		public spinner: NgxSpinnerService,
		private modalService: BsModalService,
	) {
	     this.userId  = this.regService.item.user.id;
	     this.defaultCurrencySymbol = this._global.defaultCurrencySymbol;


		 /* Remove Local Storage Data */
		 localStorage.removeItem('cartData');
	}

	ngOnInit() {
		this.spinner.show();
		let currentdate = new Date();
		this.formSubmit = false;
		this.productAdded = false;
		
		
		this.yearList = [];
		this.getWarehouseList();
		this.generateYearList(currentdate.getFullYear(),'1950');

		setTimeout(() => {
			this.getStoreList();
			this.getCategoryList();
			this.spinner.hide();
		}, 6000);


		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		this.checkWindowSize(width);

		const currentDate = new Date();
		const currentYear = currentDate.getFullYear();
		this.autoparts['itemYear'] = currentYear;
		// datatable
		this.dtOptions = {
			paging: false,
			// bInfo: false,
			ordering: false,
			searching: false,
			// bSort: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};
	}

	// show shipping type
	showShippingType() {
		this.showShipping = true;
	}

	// addRow
	addRow() {
		this.row = this.row + 1;
		this.rowArray.push({ row: this.row });
	}

	// deleteRow
	deleteRow(index) {
		this.rowArray.splice(index, 1);
		console.log(this.rowArray, index);
	}

	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}
	
	//Fetch Warehouselist by User Id
	getWarehouseList(){
		this.content.warehouselist(this.userId).subscribe((data:any) => {
			if(data.length > 0){
				this.warehouseList = data;
				this.autoparts.warehouseId = data[0].id;
				this.warehouseCode = data[0].code;

				this.getUserCartList();
			}
		});
	}

	//Fetch Store list 

	getStoreList() {


		const formdata = {type : 'autopart', warehouseId : this.autoparts.warehouseId};
		this.content.storelist(formdata).subscribe((data:any) => {
			this.storeList = data;

			if(this.isStoreAutoParts){
				const storeAutoParts = JSON.parse(localStorage.getItem('storeAutoParts'));
				if(storeAutoParts)
				{
					this.autoparts.storeId = storeAutoParts.storeId;
				}
				
			}
			else
				this.autoparts.storeId = '';
		});
	}


	//Fetch Category List on Store Change
	storeChanged(storeId){
		this.categoryList = [];

		if(storeId!='')
		{
			const formdata = {type : 'autoparts', storeId : storeId};
			this.content.categorylist(formdata).subscribe((data:any) => {
			if(data.length > 0){
				this.categoryList = data;
				this.autoparts.siteCategoryId = '';
			}else{
				this.autoparts.siteCategoryId = '';
			}

			});
		}
	}



	//Fetch Category List
	getCategoryList(){

		if(this.autoparts.storeId!='')
		{
			const formdata = {type : 'autoparts', storeId : this.autoparts.storeId};
			this.content.categorylist(formdata).subscribe((data:any) => {
				this.categoryList = data;

				if(this.isStoreAutoParts){
					const storeAutoParts = JSON.parse(localStorage.getItem('storeAutoParts'));
					if(storeAutoParts)
					{
						this.autoparts.siteCategoryId = storeAutoParts.categoryId;
						this.categoryChanged(this.autoparts.siteCategoryId);
					}
				}
				else
					this.autoparts.siteCategoryId = '';
			});

		}else{
			
			this.content.categorylistwithoutstore('autoparts').subscribe((data:any) => {
				this.categoryList = data;

				if(this.isStoreAutoParts){
					const storeAutoParts = JSON.parse(localStorage.getItem('storeAutoParts'));
					if(storeAutoParts)
					{
						this.autoparts.siteCategoryId = storeAutoParts.categoryId;
						this.categoryChanged(this.autoparts.siteCategoryId);
					}
				}
				else
					this.autoparts.siteCategoryId = '';
			});

		}
		
	}


	//Fetch Sub Category List
	categoryChanged(categoryId){
		this.subcategoryList = [];
		this.productList = [];
		if(categoryId != ''){
			this.content.subcategorylist(categoryId).subscribe((data:any) => {
				if(data.length > 0){
					this.subcategoryList = data;
					this.autoparts.siteSubCategoryId  = "";
					this.autoparts.siteProductId = "";
				} else {
					this.autoparts.siteSubCategoryId  = "";
					this.autoparts.siteProductId = "";
				}
			});
		}
	}

	//Fetch Product List
	subCategoryChanged(subCategoryId){
		this.productList = [];

		if(subCategoryId != '') {
			this.content.productlist(subCategoryId).subscribe((data:any) => {
				if(data.length > 0){
					this.productList = data;
					this.autoparts.siteProductId  = "";
				} else {
					this.autoparts.siteProductId  = "";
				}
			});
		}
	}

	//Set Product Image
	productChanged($event){
		this.autoparts.siteProductImage = $event.target.getAttribute('data-image');
	}

	warehouseChanged($event) {
		 let selectElement = $event.target;
	     var optionIndex = selectElement.selectedIndex;
	   
		 this.warehouseCode = selectElement.options[optionIndex].getAttribute('data-code');
	}

	generateYearList(max,min){
		for (var i=max; i>=min; i--) {
	      this.yearList.push(i);
	    }
	}


	fileUpload(event) {
		let fileItem = event.target.files[0];
		const file: File = fileItem;
		this.selectedFile = file;
		//console.log(this.selectedFile);
	}

	//Fetch Cart List 
  	getUserCartList() {
  		let showCart = true;

  	    const formData = {
  	    	id : this.userId,
  	    	warehouseId : this.autoparts.warehouseId,
  	    	urgent : this.autoparts.urgent,
  	    	type: "autoparts"
  	    }
  	    this.shopformeService.getUserCartList(formData).subscribe((data:any) => {
			const storeAutoParts = JSON.parse(localStorage.getItem('storeAutoParts'));

			if(storeAutoParts){
				if(data.warehouseId > 0  && data.warehouseId != storeAutoParts.warehouseId){
					showCart = false;
				    this.isStoreAutoParts = true;
				  	this.autoparts.warehouseId = data.warehouseId;
					this.bsModalRef = this.modalService.show(StorefromModalComponent, {backdrop: 'static', keyboard: false});
					this.bsModalRef.content.warehouseCode = this.warehouseCode;
					this.bsModalRef.content.bagList = data.items;
				} else {
				  this.isStoreAutoParts = true;
				  this.autoparts.warehouseId = storeAutoParts.warehouseId;
				  this.warehouseCode = storeAutoParts.warehouseCode;
				  this.autoparts.storeId = storeAutoParts.storeId;
				  this.autoparts.siteCategoryId = storeAutoParts.categoryId;
				}
		  	  
			} else {
				if(data.warehouseId){
					this.isStoreAutoParts = true;
					this.autoparts.warehouseId = data.warehouseId;
				} else {
					this.isStoreAutoParts = false;
				}
			}

			if(showCart){
				this.bagList = data.items;
				this.paymentDetails = data.details;
			}

		});
	}

    //Save Cart Items For Later 
	saveCartItems($event) {
		const formData =  {items : this.bagList };
		this.shopformeService.savecartlist(formData).subscribe((data:any) => {
			if(data.results == 'success'){
				this.toastr.success('Items have been saved for later!');
	 			this.router.navigate(['/auto-parts','my-wishlist']);
	 		}
		});

	}

	//Recalculate Cost
  	reCalculateCart($event) {
  	    const formData = {
  	    	userId : this.regService.item.user.id,
  	    	urgent : this.autoparts.urgent,
  	        warehouseId : this.autoparts.warehouseId,
  	        type: 'autoparts'
  	    }
		this.shopformeService.reCalculateCart(formData).subscribe((data:any) => {
		if(data.status == 1){
				this.paymentDetails = data.results.details;

				if(!this.paymentDetails['urgentCostCharged'] && $event.target.name == 'urgent' && this.autoparts.urgent == 'Y')
				{
					this.autoparts.urgent = "N";
					this.toastr.error('Urgent cost can only be chared for minimum item cost of '+this.paymentDetails['minUrgentCost']);
					
				}
			}
		});
	}




	//Save Cart Items For Procurement
	submitProcurement($event, shippingMethodId) {
	  console.log(this.bagList);
		const formData =  {
			items : this.bagList, 
			shippingMethodId : shippingMethodId,
			warehouseId : this.autoparts.warehouseId, 
			urgent :  this.autoparts.urgent,
			paymentDetails :  this.paymentDetails
		};
		this.shopformeService.submitautopartsdata(formData).subscribe((data:any) => {
			if(data.results == 'success'){
				this.bagList = [];
	 			this.showShipping = false;

				if(data.procurementId !='')
				{
					if(shippingMethodId == -1)
	 				{
	 					this.toastr.success('YYour order has been received and we will send you the shipping cost within 24 business hours. To see details of this order at any time, go to you Auto Parts order history, scroll to the bottom and click on details.');
	 				}else{
	 					this.toastr.success('Auto Parts Data Added Successfully');
	 					
	 				}
	 				setTimeout(() => {
			        	/** spinner ends after 5 seconds */
			       		window.location.reload();
		    		}, 13000);
					
				}
				else{
					localStorage.removeItem('cartData');
	 				localStorage.setItem('cartData', JSON.stringify(data.procurement));
	 				this.router.navigate(['/auto-parts','checkout']);

				}

	 			
	 		}
			
		});
	}

	//Remove Item From Cart List 
	deleteItem(index, id){
	  this.shopformeService.removeCartItem(id).subscribe((data:any) => {
	    if(data.results == 'success'){
	 		this.getUserCartList();
	 		this.showShipping = false;
	 	}
	  });
	}

	calculateShippingCost() {
	
		const formData =  {
			userId : this.regService.item.user.id,
			warehouseId : this.autoparts.warehouseId, 
			urgent :  this.autoparts.urgent,
			type: 'autoparts'
		};

		this.shopformeService.calculateShippingCost(formData).subscribe((response:any) => {
			if(response.status == 1) {
				this.shippingTypes = response.data;
				this.showShipping = true;
			} else {
				this.shippingTypes = [];
				this.showShipping = true;
			}
			
		});

	}


	//Add Items to Bag On Submit

  	onSubmit(form: NgForm){
		this.formSubmit = true;
		this.spinner.show();
		const formData = new FormData();

		if(this.selectedFile != null)
	  		formData.append('itemImage', this.selectedFile, this.selectedFile.name);
	  	for(let eachField in form.value)
	  	{
	  		formData.append(eachField,form.value[eachField]);
	  		//console.log(formPost[eachField]);
	  	}
	  	formData.append('warehouseId',this.autoparts.warehouseId);

    	this.shopformeService.updateUserCart(formData, 'autoparts').subscribe((data:any) => {    	
    		if(data.status == '1'){
    			this.formSubmit = false;
    			this.productAdded = true;
    			this.getUserCartList();

			const formData = {
		    	id : this.userId,
		    	urgent : this.autoparts.urgent,
		        warehouseId : this.autoparts.warehouseId,
		        type : 'autoparts'
			}

			this.shopformeService.getUserCartList(formData).subscribe((data:any) => {
				this.bagList = data.items;
				this.paymentDetails = data.details;
				if(!this.paymentDetails['urgentCostCharged'] && this.autoparts.urgent == 'Y'){
					this.autoparts.urgent = 'N';
					this.toastr.error('Urgent cost can only be charged for minimum item cost of '+this.paymentDetails['minUrgentCost']);
				}
			});

    			this.autoparts =  {
					warehouseId : this.autoparts.warehouseId,
					urgent : this.autoparts.urgent,
					storeId : "",
					siteCategoryId : "",
					siteSubCategoryId : "",
					siteProductId : "",
					siteProductImage : "",
					websiteUrl : "",
					itemName : "",
					itemDescription : "",
					itemYear: 0,
					itemMake : "",
					itemModel : "",
					itemPrice : "",
					itemQuantity : "",
					itemShippingCost : "",
					itemImage : "",
				};

				this.selectedFile = null;
    			
    			form.resetForm(this.autoparts);
    			setTimeout(() => {
			        /** spinner ends after 3 seconds */
			        this.spinner.hide();
		    	}, 3000);
    		}else {
				setTimeout(() => {
			        /** spinner ends after 3 seconds */
			        this.spinner.hide();
			    }, 3000);
			}
    		
    	 });


	}


	setTwoNumberDecimal($event) {
		if(!isNaN(parseFloat($event.target.value))){
    	 	$event.target.value = Math.abs(parseFloat($event.target.value)).toFixed(2);
		}
     	else {
     		$event.target.value = parseFloat("0").toFixed(2);
     	}
  	}

  	setInteger($event)
	{
		$event.target.value = Math.abs(Math.floor($event.target.value));
	}

  	@HostListener('window: resize', ['$event'])
	onResize(event) {
		this.checkWindowSize(event.target.innerWidth);
	}

}
