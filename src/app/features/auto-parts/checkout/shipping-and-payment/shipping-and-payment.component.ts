import { Component, OnInit } from '@angular/core';
import { ShopformeService } from '../../../../services/shopforme.service';
import {Router} from "@angular/router";
import { ToastrService } from 'ngx-toastr';
import { RegistrationService } from '../../../../services/registration.service';

@Component({
	selector: 'app-shipping-and-payment',
	templateUrl: './shipping-and-payment.component.html',
	styleUrls: ['./shipping-and-payment.component.scss']
})
export class ShippingAndPaymentComponent implements OnInit {
	showEdit = false;
	personalDetails : any;
	paymentMethodList : any;
	billingCountryId : any;
	paymentMethodId  : any;
	paymentMethodName : any;
	paymentMethodKey : any;
	shipmentMethodId : any;
	paymentTax : any;
	paymentTaxType : any;
	totalTax : any;
	totalCost: number;

	constructor(
            private router: Router,
            public shopformeService : ShopformeService,
            private regService : RegistrationService,
            public toastr : ToastrService) {
                if("cartData" in localStorage){
                 var usercart = localStorage.getItem('cartData');
                         if(usercart == null || usercart == ""){
                                this.router.navigate(['/auto-parts','order-form']);   
                         }
                         else {
                                if("personalDetails" in JSON.parse(localStorage.getItem('cartData'))){
                                        //do nothing
                                } else {
                                         this.router.navigate(['/auto-parts','checkout','personal-detail']);   
                                } 
                         }

                } else {
                   this.router.navigate(['/auto-parts','order-form']);   
                }
         }

	ngOnInit() {
		const usercart =  JSON.parse(localStorage.getItem('cartData'));	
		
	
		
		if(usercart.data.shippingMethodId != '')
		{

			this.showEdit = true;
		}

		if(typeof(usercart.paymentMethod) != 'undefined'){
			this.paymentMethodId = usercart.paymentMethod.paymentMethodId;
			this.paymentMethodName = usercart.paymentMethod.paymentMethodName;
			this.paymentMethodKey = usercart.paymentMethod.paymentMethodKey;			
			this.paymentTaxType = usercart.paymentMethod.paymentTaxType;
			this.paymentTax = usercart.paymentMethod.paymentTax;
		}

		this.personalDetails = usercart.personalDetails;
		this.billingCountryId  = usercart.personalDetails.billingCountry;
		this.shipmentMethodId = usercart.data.shipmentMethodId;
		this.totalCost = parseFloat(usercart.data.totalCost);


		this.getPaymentMethodList();

	}


	getPaymentMethodList() {
	
		const data = {billingCountryId : this.billingCountryId};
		this.shopformeService.getPaymentMethodList(data).subscribe((data:any) => {
			this.paymentMethodList = data;
		});
	}

	changePaymentMethod($event, paymentMethodName, paymentMethodKey, paymentTax, paymentTaxType) {
		this.paymentMethodName = paymentMethodName;
		this.paymentMethodKey = paymentMethodKey;
		this.paymentTax = paymentTax;
		this.paymentTaxType = paymentTaxType;
	}


	proceedToPlaceorder() {
	
		if(this.paymentMethodId == undefined || typeof(this.paymentMethodId) == undefined){

			this.toastr.success("Please select a payment method to continue");
	    	return false;

		}else{
			var storageData =  JSON.parse(localStorage.getItem('cartData'));

			
	    	//Clear Storage Data
	    	delete storageData.paymentMethod;

	    	let taxAppliedAmount:any = 0;

	    	if(storageData.data.shippinMethodId != '' || storageData.data.shippinMethodId != undefined)
	    	{
	    		if(storageData.data.totalShippingCost!=undefined)
	    		{
	    			if(storageData.isCurrencyChanged == 'Y')
	    			{
	    				var totalShippingCost = parseFloat(storageData.data.defaultTotalShippingCost);
	    				var totalProcessingFee = parseFloat(storageData.data.defaultTotalProcessingFee);
	    			}
	    			else{

	    				var totalShippingCost = parseFloat(storageData.data.totalShippingCost);
	    				var totalProcessingFee = parseFloat(storageData.data.totalProcessingFee);
	    			}
	    			taxAppliedAmount = totalShippingCost + totalProcessingFee;
	    		}
	    		else
	    		{
	    			if(storageData.isCurrencyChanged == 'Y')
	    			{
	    				taxAppliedAmount = parseFloat(storageData.data.defaultTotalProcessingFee);
	    			}else{
	    				taxAppliedAmount = parseFloat(storageData.data.totalProcessingFee);
	    			}
	    			
	    		}
	    	}else{

	    		taxAppliedAmount = parseFloat(storageData.data.totalProcessingFee);
	    	}

	    	if(this.paymentTaxType == 0)
    			this.totalTax = this.paymentTax;
    		else
    			this.totalTax = (taxAppliedAmount / 100)*parseFloat(this.paymentTax);

    		var defaultTotalTax = 0;
    		if(storageData.isCurrencyChanged == 'N')			
		    	this.totalTax = parseFloat(this.totalTax);
		       else	{
		        defaultTotalTax = parseFloat(this.totalTax);
		       	this.totalTax = this.totalTax*parseFloat(storageData.exchangeRate);
	       }
		   
		    //Reassign Storage Data
		    storageData['data']['totalTax'] = this.totalTax.toFixed(2);
			if(storageData.isCurrencyChanged == 'Y'){
			    storageData['data']['defaultTotalTax'] = defaultTotalTax.toFixed(2);
		    }
			
	    	
	    	localStorage.setItem('cartData', JSON.stringify(storageData));

	    	//Reassign Storage Data

	    	const newStorageData = Object.assign({"paymentMethod":{"paymentMethodId" : this.paymentMethodId, "paymentMethodName" : this.paymentMethodName, "paymentMethodKey" : this.paymentMethodKey, "paymentTax" : this.paymentTax, "paymentTaxType" : this.paymentTaxType}}, storageData);

	    	localStorage.setItem('cartData', JSON.stringify(newStorageData));
                
                if(this.regService.item.user.subscription.couponCode){
                    if("coupondetails" in JSON.parse(localStorage.getItem('cartData'))){  
                        this.router.navigate(['/auto-parts','checkout', 'place-order']);
                    } else {
                        this.validateCoupon(this.regService.item.user.subscription.couponCode);
                    }
                } else {
                     this.router.navigate(['/auto-parts','checkout', 'place-order']);
                }
	    	
	    }

	}
        validateCoupon(couponcode){
        this.shopformeService.validatecouponcode(couponcode).subscribe((response:any) => {
                var storedData =  JSON.parse(localStorage.getItem('cartData'));
                if(response.results == 'valid'){


                    if(storedData.isCurrencyChanged == 'Y'){
                            var defaultTotalDiscount = parseFloat(response.amount_to_be_discounted);
                            var discount = defaultTotalDiscount * storedData.exchangeRate;
                            var totalCost = parseFloat(storedData.data.totalCost);
                            var totalDiscountCost = totalCost-discount;

                            storedData['data'].totalDiscount = discount;
                            storedData['data'].defaultTotalDiscount = defaultTotalDiscount;
                            storedData['data'].totalBDiscountCost = totalCost;
                            storedData['data'].defaultTotalBDiscountCost = storedData.data.defaultTotalCost; 
                            storedData['data'].totalCost = totalDiscountCost;
                            storedData['data'].defaultTotalCost = parseFloat(storedData.data.defaultTotalCost) - defaultTotalDiscount;

                    } else {
                            var discount = parseFloat(response.amount_to_be_discounted);
                            var totalCost = parseFloat(storedData.data.totalCost);
                            var totalDiscountCost = totalCost-discount;

                            storedData['data'].totalDiscount = discount;
                            storedData['data'].totalCost = totalDiscountCost;
                            storedData['data'].totalBDiscountCost = totalCost;
                    }
                    localStorage.setItem('cartData', JSON.stringify(storedData));  

                    delete storedData.coupondetails;

                    const couponStore = Object.assign({"coupondetails":{"couponCode": couponcode, "discountAmount":discount, "discountPoint":'', "discountType" : "amount"}},storedData);

                    localStorage.setItem('cartData', JSON.stringify(couponStore));
                    this.router.navigate(['/auto-parts','checkout', 'place-order']);
                } else {
                    this.router.navigate(['/auto-parts','checkout', 'place-order']);
                }

        });
    }
}
