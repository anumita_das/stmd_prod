import { Component, OnInit } from '@angular/core';
import { ShopformeService } from '../../../../services/shopforme.service';
import {Router} from "@angular/router";
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { CurrencyModalComponent } from '../../../../shared/components/currency-modal/currency-modal.component';
import { NumberOnlyDirective } from '../../../../services/number.directive';
import { RegistrationService } from '../../../../services/registration.service';

@Component({
	selector: 'app-cart',
	templateUrl: './cart.component.html',
	styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
	dtOptions: DataTables.Settings = {};
	bagList : Array<any> = [];
	cartDetails : any = [];
	shippingMethodId : number;
	showDiscount = false;
	bsModalRef: BsModalRef;
	exchangeRate: any = 1;
    currencySymbol: any = '';
    isCurrencyChanged = 'N';
    defaultCurrencySymbol: any = '';
    couponcode: string;
    couponInValidMsg: string;
    couponInValid:boolean = false;
    blankCouponCode:boolean = true;
	personaldetails : Array<any> = [];
	user : Array<any>;

	

	constructor(private shopformeService :  ShopformeService, 
				private auth: RegistrationService,
				private router: Router,
				private modalService: BsModalService,
				) { 
					this.user = this.auth.item.user;
					
				}

	ngOnInit() {
		// datatable
		this.dtOptions = {
			paging: false,
			// bInfo: false,
			ordering: false,
			searching: false,
			// bSort: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};

		this.getUserCartList();

		
	}


	//Fetch Cart List from Session
  	getUserCartList() {

  		const usercart = JSON.parse(localStorage.getItem('cartData'));
  		this.bagList = usercart.items;
  		this.cartDetails = usercart.data;


  		this.currencySymbol = usercart.currencySymbol;
  		this.isCurrencyChanged = usercart.isCurrencyChanged;
  		this.defaultCurrencySymbol = usercart.defaultCurrencySymbol; 	
  		
	}

	// Clear the Cart and send to order frorm again

	clearCart()
	{
		this.shopformeService.clearUserCart('autoparts').subscribe((response:any) => {
			if(response.results == 'success')
			{
				localStorage.removeItem('cartData');
				this.router.navigate(['/auto-parts','order-form']);
			}
			
		});
	}

	getPersonalDetail()
	{  		
  		this.router.navigate(['/auto-parts','checkout','personal-detail']);  		
	}

	showCurrency() {
		const usercart = JSON.parse(localStorage.getItem('cartData'));
		const currencyCode = usercart.currencyCode;

		this.shopformeService.getCurrencyList(currencyCode).subscribe((response:any) => {
			this.bsModalRef = this.modalService.show(CurrencyModalComponent, {});
			this.bsModalRef.content.currencyList = response;
		});
	}

	addInsurance($event) {
		const usercart = JSON.parse(localStorage.getItem('cartData'));

		if($event.target.checked == true){
			this.cartDetails['isInsuranceCharged']  = 'Y';
			this.cartDetails['totalCost'] = this.cartDetails['totalInsurance']+this.cartDetails['totalCost'];
		} else {
			this.cartDetails['isInsuranceCharged'] = 'N';	
			this.cartDetails['totalCost'] = this.cartDetails['totalCost']-this.cartDetails['totalInsurance'];
		}

		//Reassign Storage Data
		
	    usercart['data']['isInsuranceCharged'] = this.cartDetails['isInsuranceCharged'];
	    usercart['data']['totalCost'] = this.cartDetails['totalCost'];


	    localStorage.setItem('cartData', JSON.stringify(usercart));

		
	}

	
}
