import { Component, OnInit, Renderer2, HostListener } from '@angular/core';

@Component({
	selector: 'app-sfm',
	templateUrl: './autoparts.component.html',
	styleUrls: ['./autoparts.component.scss']
})
export class AutoPartsComponent implements OnInit {
	isMediumScreen: boolean;
	constructor(
		private renderer: Renderer2,
	) { }

	ngOnInit() {
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		console.log('Width: ' + width);
		this.checkWindowSize(width);
	}

	// check window size
	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		console.log('Width: ' + event.target.innerWidth);
		this.checkWindowSize(event.target.innerWidth);
	}
}
