import { NgModule } from '@angular/core';
import { AutoPartsRoutingModule } from './autoparts-routing.module';
import { AutoPartsComponent } from './autoparts.component';
import { SharedModule } from '@app/shared';
import { OrderFormComponent } from './order-form/order-form.component';
import { HowItWorkComponent } from './how-it-work/how-it-work.component';
import { MyWishlistComponent } from './my-wishlist/my-wishlist.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { CartComponent } from './checkout/cart/cart.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { PersonalDetailComponent } from './checkout/personal-detail/personal-detail.component';
import { ShippingAndPaymentComponent } from './checkout/shipping-and-payment/shipping-and-payment.component';
import { PlaceOrderComponent } from './checkout/place-order/place-order.component';
import { OrderHistoryComponent } from './order-history/order-history.component';
import { ShippingcostModalComponent } from './order-history/shippingcost-modal.component';
//import { NumberOnlyDirective } from '../../services/number.directive';
import { ShowItemDetailModalComponent } from './order-history/showitem-modal.component';

@NgModule({
	imports: [
		NgxPaginationModule,
		AutoPartsRoutingModule,
		SharedModule
	],
	exports: [
		ShippingcostModalComponent,
		ShowItemDetailModalComponent,
	],
	declarations: [
		AutoPartsComponent,
		OrderFormComponent,
		HowItWorkComponent,
		MyWishlistComponent,
		CartComponent,
		CheckoutComponent,
		PersonalDetailComponent,
		ShippingAndPaymentComponent,
		PlaceOrderComponent,
		OrderHistoryComponent,
		ShippingcostModalComponent,
		//NumberOnlyDirective,
		ShowItemDetailModalComponent,
	],
	entryComponents: [
		ShippingcostModalComponent,
		ShowItemDetailModalComponent,
	]

})
export class AutoPartsModule {

}
