import { Component, OnInit } from '@angular/core';
import { AppGlobals } from '../../app.globals';
import { Meta, Title } from '@angular/platform-browser';
import { NgForm }   from '@angular/forms';
import { ContentService } from '../../services/content.service';
import { NgxSpinnerService } from 'ngx-spinner';
import {Router} from "@angular/router";

@Component({
	selector: 'app-contact-us',
	templateUrl: './contact-us.component.html',
	styleUrls: ['./contact-us.component.scss'],
	providers: [AppGlobals]
})
export class ContactUsComponent implements OnInit {
	lat: number;
	lng: number;
	showChat: boolean;
	formSubmitted : boolean;
	formSuccess : boolean;
	countryList : any;
	stateList : any = '';
	cityList : any = '';
	fullPageContent : any;
	reasons: any = '';
	reasontype: any = '';
	constructor(
		private _global: AppGlobals,
		private meta: Meta, 
		private title: Title,
		private content : ContentService,
		public spinner : NgxSpinnerService,
		private router: Router,
		) {

		title.setTitle(this._global.siteTitle);
	    meta.addTags([ 
	      {
	        name: 'keywords', content: this._global.siteMetaKeywords
	      },
	      {
	        name: 'description', content: this._global.siteMetaDescription
	      },
	    ]);
	 }

	 items : [{
	 	add_sub_ques: "1"
		ans_type: "rating"
		chain_id: 0
		created_at: "2018-07-23 00:00:00"
		deleted_at: null
		feedback_form_id: 1
		feedback_form_name: "Default Feedback Form"
		hotel_id: 0
		id: 48
		is_admin: "1"
		is_multiselect: "0"
		lang_code: "en"
		order_number: 1
		shortcode: "overall"
		status: "1"
		sub_answer_options: null
		title: "Overall Rating"
		updated_at: null
	 },
	 {
	 	add_sub_ques: "1"
		ans_type: "rating"
		chain_id: 0
		created_at: "2018-07-23 00:00:00"
		deleted_at: null
		feedback_form_id: 1
		feedback_form_name: "Default Feedback Form"
		hotel_id: 0
		id: 48
		is_admin: "1"
		is_multiselect: "0"
		lang_code: "en"
		order_number: 1
		shortcode: "overall"
		status: "1"
		sub_answer_options: null
		title: "Overall Rating"
		updated_at: null
	 }];

	ngOnInit() {
		this.getAllCountry();
		this.getPageContent();
		this.formSuccess = false;
		this.formSubmitted = false;
		this.lat = 51.678418;
		this.lng = 7.809007;
		this.getAllContactUsReasons();
	}

	// chat
	toggleChat() {
		this.showChat = this.showChat ? false : true;
		// this.showShipment = false;
	}

	getPageContent() {
		this.content.pageContent('contact_us').subscribe((data:any) => {
			this.fullPageContent = data.pageContent[0];
		});

	}

	onSubmit(form : NgForm) {
		this.spinner.show();
		this.formSubmitted = true;
		this.content.saveContactUs(form.value).subscribe((data:any) => {
			if(data.status == '1'){
				this.formSuccess = true;
				this.router.navigate(['/contact-us','thankyou']);
			}

			setTimeout(() => {
			        /** spinner ends after 3 seconds */
			        this.spinner.hide();
			    }, 2000);
		});
		
	}

	getAllCountry(){
		this.content.allCountry().subscribe((data:any)=>{
			this.countryList = data;
		});
	}

	getStateCity(event){
		let selectedCountry = event.target.value;
		this.content.stateCityOfCountry(selectedCountry).subscribe((data:any) => {
			//console.log(data);
			if(data.stateList)
			{
				this.stateList = data.stateList;
				this.cityList = data.cityList;
			}
		});
	}

	getCity(event) {
		let selectedState = event.target.value;
		this.content.cityOfStates(selectedState).subscribe((data:any) => {
			if(data.cityList)
			{
				this.cityList = data.cityList;
			}
		});
	}

	getAllContactUsReasons(){
		this.content.getAllContactUsReasons().subscribe((data:any)=>{
			this.reasons = data.results.reasons;
		});
	}
}
