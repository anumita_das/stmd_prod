import { Component, OnInit } from '@angular/core';
import { NgForm }   from '@angular/forms';
import { User } from '../../shared/models/user.model';
import { RegistrationService } from '../../services/registration.service';
import { Router } from '@angular/router';
import { AppGlobals } from '../../app.globals';
import { Meta, Title } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ForgotPasswordModalComponent } from '../../shared/components/forgot-password-modal/forgot-password-modal.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],	
  providers: [AppGlobals]
})
export class LoginComponent implements OnInit {
	bsModalRef: BsModalRef;
	user  = new User();
	dataInvalid = false;
	formErrors = [];
	formSubmit : boolean = false;	
	forcelogin = 0;																	
	constructor(
	private regService : RegistrationService,
	public router: Router,		
	private _global: AppGlobals,
	private meta: Meta, 		
	private pagetitle: Title,
	public toastr : ToastrService,
	private modalService: BsModalService
	) {
		pagetitle.setTitle(this._global.siteTitle);
	    meta.addTags([ 
	      {
	        name: 'keywords', content: this._global.siteMetaKeywords
	      },
	      {
	        name: 'description', content: this._global.siteMetaDescription
	      },
	    ]);
	}

	ngOnInit() {

		if (this.regService.isLoggedIn) {
	      // Get the redirect URL from our auth service
	      // If no redirect has been set, use the default
	      //const redirect = this.regService.redirectUrl ? this.regService.redirectUrl : 'admin';
	      const redirect = 'my-warehouse/overview';
	      // Redirect the user
	      this.router.navigate([redirect]);
	    }
	}

	onSubmit(form: NgForm){
		this.formSubmit = true;
		this.regService.login(form.value).subscribe((data:any) => {
			this.formSubmit = false;
			//console.log(data.token);
			if(data.token!='' && data.token!='-1')
			{
				this.regService.isLoggedIn = true;
          		localStorage.setItem('tokens', JSON.stringify(data));
          		localStorage.setItem('profileImage',JSON.stringify(data.user.profileImage));
          		console.log(JSON.parse(localStorage.getItem('profileImage')));
          		//const redirect = this.regService.redirectUrl ? this.regService.redirectUrl : 'admin';
          		const redirect = 'my-warehouse/overview';
          		
          		if(this.regService.isLoggedIn && JSON.parse(localStorage.getItem('tokens'))!=null)
          			this.router.navigate([redirect]);
			}
			else
			{
				if(data.token=='-1')
				{
					if(data.user == '-1')
					{
						this.toastr.error('Username/password does not match');
					}
					if(data.user == '-2'){
						this.toastr.warning('', data.message, {
  							timeOut: 5000,
  							enableHtml:true
						});
						this.forcelogin = 1;
					}
					if(data.user == '-3'){
						this.toastr.error('', data.message, {
  							timeOut: 25000,
  							enableHtml:true
						});
						this.forcelogin = 0;
					}
				}
			}

		});
	}

	openForgotPasswordModal() {
		this.bsModalRef = this.modalService.show(ForgotPasswordModalComponent, {
			class: 'modal-xs modal-dialog-centered'

		});
		this.bsModalRef.content.closeBtnName = 'Close';
	}

	forcedLogin(){
		this.regService.loginforced(this.user.email, this.user.password).subscribe((data:any) => {
			if(data.token!='' && data.token!='-1')
			{
				this.regService.isLoggedIn = true;
          		localStorage.setItem('tokens', JSON.stringify(data));
          		localStorage.setItem('profileImage',JSON.stringify(data.user.profileImage));
          		console.log(JSON.parse(localStorage.getItem('profileImage')));
          		const redirect = this.regService.redirectUrl ? this.regService.redirectUrl : 'admin';
          		
          		if(this.regService.isLoggedIn && JSON.parse(localStorage.getItem('tokens'))!=null)
          			this.router.navigate([redirect]);
			}
			else
			{
				if(data.token=='-1')
				{
					if(data.user == '-1')
					{
						//this.dataInvalid = true;
						//this.formErrors.push('Username/password does not match');
						this.toastr.error('Username/password does not match');
					}
					if(data.user == '-2'){
						this.toastr.warning('', data.message, {
  							timeOut: 5000,
  							enableHtml:true
						});
						this.forcelogin = 1;
					}
				}
			}
		});
	}

	forcedLoginNo(){
		this.forcelogin = 0;
	}

	forceloginNoOnChange(e){
		this.forcelogin = 0;
	}

}
