import { NgModule } from '@angular/core';
import { SubscribeRoutingModule } from './subscribe-routing.module';
import { SubscribeComponent } from './subscribe.component';
import { SharedModule } from '@app/shared';
import { LazyLoadImageModule } from 'ng-lazyload-image';

@NgModule({
	imports: [
		SubscribeRoutingModule,
		SharedModule,
		LazyLoadImageModule,
	],
	declarations: [
		SubscribeComponent
	]
})
export class SubscribeModule {

}
