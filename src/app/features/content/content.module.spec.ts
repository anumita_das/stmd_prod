import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentModule } from './content.module';

describe('ContentModule', () => {
  let contentModule: ContentModule;
  let fixture: ComponentFixture<ContentModule>;

  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentModule);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
	
  it('should create an instance', () => {
    expect(contentModule).toBeTruthy();
  });
});
