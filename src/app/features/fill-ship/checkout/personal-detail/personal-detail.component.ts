import { Component, OnInit } from '@angular/core';
import { ContentService } from '../../../../services/content.service';
import { RegistrationService } from '../../../../services/registration.service';
import { SharedModule } from '@app/shared';
import { NgForm }   from '@angular/forms';
import {Router} from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
	selector: 'app-personal-detail',
	templateUrl: './personal-detail.component.html',
	styleUrls: ['./personal-detail.component.scss'],
})
export class PersonalDetailComponent implements OnInit {
	billForm = true;
	isReadOnly = false;
	isShippingReadOnly = false;
	countryList : any;
	stateList:any;
	cityList:any;
	billingstateList:any;
	billingcityList:any;
	shippingCountryName  : any;
	shippingStateName  : any;
	shippingCityName : any;
	billingCountryName : any;
	billingStateName : any;
	billingCityName : any;
	defaultBillingCountryId : any;
	phcodeList: Array<any>;	
	shippingisdCode : any;
	userDetails: any;
	userDefaultShippingAddress : Array<any> = [];
	userDefaultBillingAddress : Array<any> = [];

	constructor(
		private content:ContentService,
		private auth: RegistrationService,
		private router: Router,
		public spinner: NgxSpinnerService,	
	) {
		if("userFillShipCart" in localStorage){
		  	var usercart = localStorage.getItem('userFillShipCart');
			if(usercart == null || usercart == ""){
			 	this.router.navigate(['fill-and-ship','order-form']);   
			 } else {
			 	if("data" in JSON.parse(localStorage.getItem('userFillShipCart'))){
			 		//do nothing
			 	} else {
			 		 this.router.navigate(['fill-and-ship','checkout','cart']);   
			 	}
			}
		} else {
		   this.router.navigate(['fill-and-ship','order-form']);   
		}

		this.spinner.show();
	}

	ngOnInit() {
		const usercart =  JSON.parse(localStorage.getItem('userFillShipCart'));
		if(usercart.data.shipmentMethodId != '' && typeof(usercart.data.shipmentMethodId) != 'undefined')
			this.isReadOnly = true;

		this.userDetails = {
			firstName : '',
		};

		this.getAllCode();
		this.getUserDetails();	
		this.getUserBillingAddress();	
		this.getUserShippingAddress();	
		this.getAllCountry();


		setTimeout(() => {
			this.spinner.hide();
		}, 3000);

		//this.userDetails['phcode'] = "234";
		//this.userDefaultBillingAddress['isdCode'] = "234";
		//this.userDefaultShippingAddress['isdCode'] = "234";
	}

	getAllCode()
	{
		this.content.allCode().subscribe((data:any)=>{
			this.phcodeList = data;
		});
	}

	getUserDetails() {
		var usercart =  JSON.parse(localStorage.getItem('userFillShipCart'));
		if(typeof(usercart.personaldetails) !== 'undefined'){
			this.userDetails['title'] = usercart.personaldetails['userTitle'];
			this.userDetails['email'] = usercart.personaldetails['userEmail'];
			this.userDetails['firstName'] = usercart.personaldetails['firstName'];
			this.userDetails['lastName'] = usercart.personaldetails['lastName'];
			this.userDetails['company'] = usercart.personaldetails['company'];
			if(usercart.personaldetails['phcode']!='' || usercart.personaldetails['phcode'] != 'undefined')
			{
				this.userDetails['phcode'] = usercart.personaldetails['phcode'];
			}
			else{
				this.userDetails['phcode'] = "234";
			}
			this.userDetails['contactNumber'] = usercart.personaldetails['contactNumber'];
		} else {
			this.userDetails = this.auth.item.user;
			this.userDetails['phcode'] = "234";
		}
		
	}
	
	getUserBillingAddress() {
		var usercart =  JSON.parse(localStorage.getItem('userFillShipCart'));

		if(typeof(usercart.personaldetails) !== 'undefined'){
			//Set Data From Local Storage
			this.userDefaultBillingAddress['firstName'] = usercart.personaldetails['billingFirstName'];
			this.userDefaultBillingAddress['lastName'] = usercart.personaldetails['billingLastName'];
			this.userDefaultBillingAddress['email'] = usercart.personaldetails['billingEmail'];
			this.userDefaultBillingAddress['address'] = usercart.personaldetails['billingAddress'];
			this.userDefaultBillingAddress['alternateAddress'] = usercart.personaldetails['billingAlternateAddress'];
			this.userDefaultBillingAddress['countryId'] = usercart.personaldetails['billingCountryId'];
			this.userDefaultBillingAddress['stateId'] = usercart.personaldetails['billingStateId'];
			this.userDefaultBillingAddress['cityId'] = usercart.personaldetails['billingCityId'];
			this.userDefaultBillingAddress['zipcode'] = usercart.personaldetails['billingZipcode'];
			this.userDefaultBillingAddress['phone'] = usercart.personaldetails['billingPhone'];

			if(usercart.personaldetails['billingIsdcode']!='' || usercart.personaldetails['billingIsdcode'] != 'undefined')
			{
				this.userDefaultBillingAddress['isdCode'] = usercart.personaldetails['billingIsdcode'];
			}
			else{
				this.userDefaultBillingAddress['isdCode'] = "234";
			}

			this.defaultBillingCountryId = usercart.personaldetails['billingCountryId'];
			this.billingCountryName = usercart.personaldetails['billingCountryName'];
			this.billingStateName  = usercart.personaldetails['billingStateName'];
			this.billingCityName  = usercart.personaldetails['billingCityName'];

			this.getStateCityList();
		} else {
			this.auth.userShippingAddress('billing').subscribe((data:any) =>{
				if(data.status == 1)
				{
					this.userDefaultBillingAddress = data.results[0];
					this.defaultBillingCountryId = data.results[0].countryId;
					this.billingCountryName  = data.results[0].country.name;
					this.billingStateName  = data.results[0].state.name;
					this.billingCityName  = data.results[0].city.name;

					this.userDefaultBillingAddress['isdCode'] = "234";

					this.getStateCityList();
				}
			});
		}
	}

	getStateCityList() {
		this.content.stateCityOfCountry(this.defaultBillingCountryId).subscribe((data:any) => {
				if(data.stateList)
				{
					this.stateList = data.stateList;
					this.billingstateList = data.stateList;
				}
				if(data.cityList)
				{
					this.cityList = data.cityList;
					this.billingcityList = data.stateList;
				}
			});
	}


	getUserShippingAddress() {
		var usercart =  JSON.parse(localStorage.getItem('userFillShipCart'));
		
		if(typeof(usercart.personaldetails) !== 'undefined'){

			if(usercart.personaldetails['locationId'] != '')
					this.isShippingReadOnly = true;

			//Set Data From Local Storage
			this.userDefaultShippingAddress['firstName'] = usercart.personaldetails['shippingFirstName'];
			this.userDefaultShippingAddress['lastName'] = usercart.personaldetails['shippingLastName'];
			this.userDefaultShippingAddress['email'] = usercart.personaldetails['shippingEmail'];
			this.userDefaultShippingAddress['address'] = usercart.personaldetails['shippingAddress'];
			this.userDefaultShippingAddress['alternateAddress'] = usercart.personaldetails['shippingAlternateAddress'];
			this.userDefaultShippingAddress['countryId'] = usercart.personaldetails['shippingCountryId'];
			this.userDefaultShippingAddress['stateId'] = usercart.personaldetails['shippingStateId'];
			this.userDefaultShippingAddress['cityId'] = usercart.personaldetails['shippingCityId'];
			this.userDefaultShippingAddress['zipcode'] = usercart.personaldetails['shippingZipcode'];
			this.userDefaultShippingAddress['phone'] = usercart.personaldetails['shippingPhone'];

			if(usercart.personaldetails['shippingIsdcode']!='' || usercart.personaldetails['shippingIsdcode'] != 'undefined')
			{
				this.userDefaultShippingAddress['isdCode'] = usercart.personaldetails['shippingIsdcode'];

			}
			else{
				this.userDefaultShippingAddress['isdCode'] = "234";
			}

			this.userDefaultShippingAddress['locationId'] = usercart.personaldetails['locationId'];

			this.shippingCountryName = usercart.personaldetails['shippingCountryName'];
			this.shippingStateName  = usercart.personaldetails['shippingStateName'];
			this.shippingCityName  = usercart.personaldetails['shippingCityName'];
		} else {
			this.auth.userShippingAddress('shipping').subscribe((data:any) =>{
				if(data.status == 1)
				{
					if(data.results[0].locationId != '')
						this.isShippingReadOnly = true;

					this.userDefaultShippingAddress = data.results[0];
					this.shippingCountryName  = data.results[0].country.name;
					this.shippingStateName  = data.results[0].state.name;
					this.shippingCityName  = data.results[0].city.name;
					this.userDefaultShippingAddress['isdCode'] = "234";
					this.shippingisdCode = "234";
				}
			});
		}
	}

	
	getAllCountry(){
		this.content.allCountry().subscribe((data:any)=>{
			this.countryList = data;
		});
	}

	getStateCity(event,type){
		let selectedCountry = event.target.value;
		this.content.stateCityOfCountry(selectedCountry).subscribe((data:any) => {
			if(data.stateList)
			{
				if(type == 'billing'){
					this.userDefaultBillingAddress['stateId'] = '';
					this.userDefaultBillingAddress['cityId'] = '';
					this.billingstateList = data.stateList;
				}
				else {
					this.userDefaultShippingAddress['stateId'] = '';
					this.userDefaultShippingAddress['cityId'] = '';
					this.stateList = data.stateList;
				}
			}
		});
	}

	getCity(event,type) {
		let selectedState = event.target.value;
		this.content.cityOfStates(selectedState).subscribe((data:any) => {
			if(data.cityList)
			{
				if(type == 'billing'){
					this.userDefaultBillingAddress['cityId'] = '';
					this.billingcityList = data.cityList;
				}
				else {
					this.userDefaultShippingAddress['cityId'] = '';
					this.cityList = data.cityList;
				}
			}
		});
	}

	showForm(event) {
		this.billForm = !this.billForm;
	}


	setBillingCountryName(event) {
		this.billingCountryName = event.target.text;
	}

	setBillingStateName(event) {
		this.billingStateName = event.target.text;
	}

	setBillingCityName(event) {
		this.billingCityName = event.target.text;
	}

	setShippingCountryName(event){
		this.shippingCountryName = event.target.text;
	}

	setShippingStateName(event){
		this.shippingStateName = event.target.text;
	}

	setShippingCityName(event){
		this.shippingCityName = event.target.text;
	}

	hack(value) {
	   return Object.values(value)
	}

	onSubmit(form: NgForm){
    	var storedData =  JSON.parse(localStorage.getItem('userFillShipCart'));
    	
    	//Clear Storage Data
    	delete storedData.personaldetails ;
    	
    	//Reassign Storage Data
    	const newStoredData = Object.assign({"personaldetails":form.value},storedData);
    	localStorage.setItem('userFillShipCart', JSON.stringify(newStoredData));

    	this.router.navigate(['fill-and-ship','checkout', 'shipping-and-payment']);
    }
}
