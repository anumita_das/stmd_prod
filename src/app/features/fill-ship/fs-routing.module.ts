import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from '../../services/auth-guard.service';

import { FSComponent } from './fs.component';
import { FSOrderFormComponent } from './fs-order-form/fs-order-form.component';
import { OverviewComponent } from './overview/overview.component';
import { ArchivedComponent } from './overview/archived.component';
import { ExtracostComponent } from './overview/extracharge.component';
import { CartComponent } from './checkout/cart/cart.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { PersonalDetailComponent } from './checkout/personal-detail/personal-detail.component';
import { ShippingAndPaymentComponent } from './checkout/shipping-and-payment/shipping-and-payment.component';
import { PlaceOrderComponent } from './checkout/place-order/place-order.component';

const routes: Routes = [
	{
		path: '', redirectTo: 'order-form', pathMatch: 'full'
	},
	{
		path: '',
		component: FSComponent,
		children: [
			{ path: 'order-form', canActivateChild: [AuthGuardService], component: FSOrderFormComponent },
			{ path: 'checkout',
				component: CheckoutComponent,
				children: [
					{ path: '', redirectTo: 'cart', pathMatch: 'full' },
					{ path: 'cart', canActivateChild: [AuthGuardService], component: CartComponent },
					{ path: 'personal-detail', canActivateChild: [AuthGuardService], component: PersonalDetailComponent },
					{ path: 'shipping-and-payment', canActivateChild: [AuthGuardService], component: ShippingAndPaymentComponent },
					{ path: 'place-order', canActivateChild: [AuthGuardService], component: PlaceOrderComponent }
				]
			},
			{ path: 'overview', canActivateChild: [AuthGuardService], component: OverviewComponent },
			{ path: 'archived', canActivateChild: [AuthGuardService], component: ArchivedComponent },
			{ path: 'extracost/:id', canActivateChild: [AuthGuardService], component: ExtracostComponent },
		]
	}

];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class FSRoutingModule {

}
