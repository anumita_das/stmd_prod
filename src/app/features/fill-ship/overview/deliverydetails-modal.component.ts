import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { RegistrationService } from '../../../services/registration.service';
import {Router} from "@angular/router";
import { AppGlobals } from '../../../app.globals';
import { FillshipService } from '../../../services/fillship.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
	selector: 'app-fillshipdeliverydetails-modal',
	templateUrl: './deliverydetails-modal.component.html',
	styleUrls: ['./deliverydetails-modal.component.scss'],
	providers: [AppGlobals]
})
export class DeliverydetailsModalComponent implements OnInit {

	title: string;
	closeBtnName: string;
	deliveryInfo : any = "";

	constructor(
		public bsModalRef: BsModalRef,
		public userInfo : RegistrationService,
		private router: Router,
		private _global: AppGlobals,
		private fillshipService : FillshipService,
		private spinner: NgxSpinnerService,
		private toastr: ToastrService,
	) {
	}

	ngOnInit() {

	}
}
