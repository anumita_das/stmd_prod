import { Component, OnInit, NgZone, HostListener } from '@angular/core';
import { NgForm }   from '@angular/forms';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { RegistrationService } from '../../../services/registration.service';
import { ContentService } from '../../../services/content.service';
import { AppGlobals } from '../../../app.globals';
import { ToastrService } from 'ngx-toastr';
import { Router} from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';
import { WarehouseModalComponent } from '../../../shared/components/warehouse-modal/warehouse-modal.component';
import { FillshipService } from '../../../services/fillship.service';
import { ShopformeService } from '../../../services/shopforme.service';
import { ShowShipoutModalComponent } from './showshipout-modal.component';
import { DeliverydetailsModalComponent } from './deliverydetails-modal.component';

@Component({
	selector: 'app-fs-overview-archived',
	templateUrl: './archived.component.html',
	styleUrls: ['./archived.component.scss']
})
export class ArchivedComponent implements OnInit {
	isMediumScreen: boolean;
	showShipping: boolean;
	sidebar: boolean;
	isCollapsed: boolean;
	bsModalRef: BsModalRef;	

	userId : any;
	defaultCurrencySymbol : any;
	allWarehouse : Array<object> = [];
	userShippingAddress : Array<object> = [];
	warehouseShipmentList : any = [];
	earnedPoints: any;
	rewardName: any;
	expString: any;
	dtOptions: DataTables.Settings = {};
	selectedWarehouseDetails: any;
	selectedWarehouse : any;
	shipoutOptions : Array<object> = [];
	defaultProfile : any;
	tracks : Array<object> = [];
	links: Array<any>;

	constructor(
		private ngZone: NgZone,
		private fillshipService : FillshipService,
		private shopformeService : ShopformeService,
		public  regService : RegistrationService,
		private content: ContentService,
		private _global: AppGlobals,
		private toastr: ToastrService,
		private modalService: BsModalService,
		private spinner: NgxSpinnerService,
		private router: Router,
	) {
		this.userId = this.regService.item.user.id;
		this.defaultCurrencySymbol = this._global.defaultCurrencySymbol;
	 }


	ngOnInit() {

		this.links = [
			{
				title: 'My Active Fill and Ship',
				url: 'fill-and-ship/overview'
			},
			{
				title: 'My Archived Fill and Ship',
				url: 'fill-and-ship/archived'
			}
		];

		this.defaultProfile = JSON.parse(localStorage.getItem('profileImage'));

		this.spinner.show();
		this.getAllWarehouse();
		this.getUserAddressInfo();
		this.getRewardDetails();

		

		// datatable
		this.dtOptions = {
			paging: false,
			// bInfo: false,
			ordering: false,
			searching: false,
			// bSort: false,
			autoWidth: false,
			"language": {
		      "emptyTable": "No shipment available for now"
		    },
			dom: '<"table-responsive"t>'
		};
		
	}

	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	getAllWarehouse(){
		this.content.allWarehouse().subscribe((data:any) => {
			let index = 0;
			if(data.status == 1)
			{
				for(let eachWarehouse of data.results) {
					this.allWarehouse.push(eachWarehouse);
					if(index == JSON.parse(localStorage.getItem('selectedWarehouse')))
					{
						this.selectedWarehouseDetails = eachWarehouse;
					}
					index++;
				}

				this.getFillShipUserData();
			}
			this.selectedWarehouse = JSON.parse(localStorage.getItem('selectedWarehouse'));
			
		});
	}

	getFillShipUserData() {
		this.fillshipService.getUserFillship(this.selectedWarehouseDetails,this.userId,'archived').subscribe((data:any) => {
			this.spinner.hide();
			if(data.shipments)
			{
				this.warehouseShipmentList = data.shipments;
				for(let eachShipment of this.warehouseShipmentList)
				{
					this.shipoutOptions.push(eachShipment.shipout_extracharge);
					this.tracks.push(eachShipment.stages);
				}
			}
			else
			{
				this.warehouseShipmentList = [];
			}

		});
	}

	selectWarehose(event){
		this.spinner.show();
		let index = 0;
		var selectedDrop = event.target.value;
		localStorage.setItem('selectedWarehouse',JSON.stringify(selectedDrop));
		this.selectedWarehouse = selectedDrop;
		for(let eachWarehouse of this.allWarehouse)
		{
			if(index == selectedDrop)
			{
				this.selectedWarehouseDetails = eachWarehouse;
			}
			index++;
		}

		this.getFillShipUserData();

		
		let el = document.getElementById('warehouseAddress');
	  	el.scrollIntoView({behavior: 'smooth', block: 'start', inline: 'end'});	  	
	}

	getUserAddressInfo() {
		this.regService.userShippingAddress('shipping').subscribe((data:any) =>{
			if(data.status == 1)
			{
				this.userShippingAddress = data.results;
			}
		});
	}

	getRewardDetails() {
		this.content.getRewardDetails(this.userId).subscribe((data:any) => { 
			this.earnedPoints = data.results.earnedPoints;
			this.rewardName = data.results.rewardName;
			this.expString = data.results.expStringShort;
		});
	}

	showWarehouseAddress() {
    	this.bsModalRef = this.modalService.show(WarehouseModalComponent, {});
    	this.bsModalRef.content.warehouseAddress = this.selectedWarehouseDetails;
		this.bsModalRef.content.closeBtnName = 'Close';		
    }



	setTwoNumberDecimal($event) {
		if(!isNaN(parseFloat($event.target.value))){
		 	$event.target.value = Math.abs(parseFloat($event.target.value)).toFixed(2);
		}
	 	else {
	 		$event.target.value = parseFloat("0").toFixed(2);
	 	}
	}

	setInteger($event)
	{
		$event.target.value = Math.abs(Math.floor($event.target.value));
	}

	hack(value) {
		if(value!=undefined && value!=null)
		{
			return Object.values(value);
		}
	   
	}

	shipout($event,shipmentid,index) {
		console.log(this.shipoutOptions[index]);
		this.bsModalRef = this.modalService.show(ShowShipoutModalComponent, {});
    	this.bsModalRef.content.shipoutOptions = this.shipoutOptions[index];
    	this.bsModalRef.content.fillshipShipmentId = shipmentid;
		this.bsModalRef.content.closeBtnName = 'Close';
	}

	showItemDetail(itemId)
	{
		this.fillshipService.getDeliverydetail(itemId).subscribe((response:any) => {
			//console.log(response);
			this.bsModalRef = this.modalService.show(DeliverydetailsModalComponent, {
				class: 'modal-md modal-dialog-centered'
			});
			if(response.results)
				this.bsModalRef.content.deliveryInfo = response.results;
			
		});

	}

	print(paidForId,paidFor) {

		this.shopformeService.getPaymentInvoice(paidForId,paidFor,'').subscribe((response:any) => {

		  if(response.status == 1) {
			 	let printContents, popupWin;
				    printContents = response.data;
				    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
				    popupWin.document.open();
				    popupWin.document.write(`
				      <html>
				        <head>
				          <title>Print tab</title>
				          <style>
				          //........Customized style.......
				          </style>
				        </head>
				    <body onload="window.print();window.close()">${printContents}</body>
				      </html>`
				    );
				    popupWin.document.close();
			  } else {
				this.toastr.error('Invoice not available. Please contact site admin!!');
			  }
		});
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		console.log('Width: ' + event.target.innerWidth);
		this.checkWindowSize(event.target.innerWidth);
	}
}
