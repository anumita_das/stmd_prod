import { Component, OnInit } from '@angular/core';
import { ContentService } from '../../services/content.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute, Params, ParamMap } from '@angular/router';
import { NgForm }   from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
	selector: 'app-login',
	templateUrl: './forgot-password.component.html',
	styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

	code: string;
	password:any;
	confirmPassword:any;
	token:string;
	public barLabel: string = "Password strength:";
    public myColors = ['#DD2C00', '#FF6D00', '#FFD600', '#AEEA00', '#00C853'];
    //wcodePattern = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&^()_?+=])[A-Za-z\\d@$_!%*?&-^]{8,20}$"; 
    wcodePattern = "^.{8,}$";

    hidePwdStrength:any;
    deactiveBtn : any = true;
    color:any;
    countData:any;

	constructor(
		private content: ContentService,
		public toastr : ToastrService,
		private route: ActivatedRoute,
		public spinner : NgxSpinnerService,
	) { }

	ngOnInit() {
	}

	onSubmit(form: NgForm){
		this.spinner.show();

		this.route.params.subscribe(params => {
	      //console.log(params['token']) //log the entire params object
	      this.content.submitTokenPassword(this.password, this.confirmPassword, params['token']).subscribe((data:any) => {
				this.countData = data.countData;
				if(this.countData == 0){
					this.code = data.results;
				} else {
					this.code = 'Password has been updated. Please <a href="login">login</a> to your account';
				}
				this.color = data.color;
				setTimeout(() => {
			        /** spinner ends after 3 seconds */
			        this.spinner.hide();
			    }, 2000);
			});
	    });

		
	}

	checkEmptyPassword(event){
		if(event == ''){
			this.hidePwdStrength = true;
		} else {
			this.hidePwdStrength = false;
		}

		if(this.confirmPassword !== event){
			this.deactiveBtn = false;
		} else {
			this.deactiveBtn = true;
		}

	}


	checkMatchPasswords(event){ 

		if(this.password !== event){
			this.deactiveBtn = false;
		} else {
			this.deactiveBtn = true;
		}

	}
}
