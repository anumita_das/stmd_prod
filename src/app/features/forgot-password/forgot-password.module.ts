import { NgModule } from '@angular/core';
import { ForgotPasswordRoutingModule } from './forgot-password-routing.module';
import { ForgotPasswordComponent } from './forgot-password.component';
import { SharedModule } from '@app/shared';
import { PasswordStrengthBarModule } from 'ng2-password-strength-bar';

@NgModule({
	imports: [
		ForgotPasswordRoutingModule,
		SharedModule,
		PasswordStrengthBarModule
	],
	declarations: [ForgotPasswordComponent]
})
export class ForgotPasswordModule {

}
