import { NgModule } from '@angular/core';
import { ItemArrivedRoutingModule } from './item-arrived-routing.module';
import { ItemArrivedComponent } from './item-arrived.component';
import { SharedModule } from '@app/shared';


@NgModule({
	imports: [
		ItemArrivedRoutingModule,
		SharedModule
	],
	declarations: [
		ItemArrivedComponent
	]
})
export class ItemArrivedModule {

}
