import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params, ParamMap } from '@angular/router';
import { ContentService } from '../../services/content.service';
import { NgForm }   from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
	selector: 'app-item-arrived',
	templateUrl: './item-arrived.component.html',
	styleUrls: ['./item-arrived.component.scss']
})
export class ItemArrivedComponent implements OnInit {
	tracks: any;
	trackingNumber : string = '';
	itemarrivalDate : string = '';
	noData : boolean = false;
	myTime = new Date();
  	valid = true;
  	waitForMore = false;
  	showVerifyForm = false;
  	invoiceFile : any = "";
  	itemPhotoFile : any = "";
  	trackingDate : any = "";
  	trackingTime : any = "";
  	statusLog : any ="";
  	isPeriodDisabled: boolean;
  	results: any ="";
	rangeDate: string="";


	constructor(
		public route: ActivatedRoute,
		public content : ContentService,
		public router: Router,
		private toastr: ToastrService,
		private spinner: NgxSpinnerService,
	) { }

	ngOnInit() {
	
	  if(this.route.snapshot.paramMap.get('tracking'))
		{
			this.trackingNumber = this.route.snapshot.paramMap.get('tracking');
			this.getTrackingDetails();
		}

		this.itemarrivalDate = "TM";
		
	}

	enableRange(event) {
		if (event === 'FR') {
			this.isPeriodDisabled = true;
		} else {
			this.isPeriodDisabled = false;
		}
	}

	getTrackingDetails() {
		this.noData = false;
		this.tracks = '';
		this.statusLog = [];
		this.content.itemArrivedDetails(this.trackingNumber, this.itemarrivalDate, this.rangeDate).subscribe((data:any) => {
				if(data.status == '1')
				{
					this.tracks = data.results;
					this.noData = false;
				}
				else if(data.status == '-1')
				{
					this.noData = true;
				}
				else if(data.status == '2')
				{
					/* For quick shipout status log*/
					this.results = data.results[0];
					this.noData = false;
					if(this.results[0].packages.length == 0)
					{
						this.noData = true;
					}
				}
		});
	}

	checkItemArrived(form : NgForm) {
		if(form.value.trackingNumber!='')
		{
			this.router.navigate(['item-arrived', form.value.trackingNumber]);
			this.trackingNumber = form.value.trackingNumber;

			this.getTrackingDetails();
		}
	}

	checkItemArrivedDate(form: NgForm)
	{
		if(form.value.itemarrivalDate!="")
		{
			this.itemarrivalDate = form.value.itemarrivalDate;
			this.rangeDate = form.value.rangeDate;
		}
	}

	verifyNotificationTime(form : NgForm) {
		this.trackingDate = form.value.deliveryDate;
		this.trackingTime = "";
		this.content.verifyitemarrivedtime(form.value).subscribe((data:any) => {
			this.noData = false;
			if(data.status == '1')
			{
				this.waitForMore = false;
				this.showVerifyForm = true;
			}
			else if(data.status == '2')
			{
				this.waitForMore = true;
				this.showVerifyForm = false;
			}
		});
	}

	addInvoice(event) {

		let fileItem = event.target.files[0];
  		const file: File = fileItem;
  		this.invoiceFile = file;
	}

	addItemphoto(event) {

		let fileItem = event.target.files[0];
  		const file: File = fileItem;
  		this.itemPhotoFile = file;
	}

	onSubmit(form : NgForm) {
		this.spinner.show();
		const formData = new FormData();
		for(let eachField in form.value)
        {
           	formData.append(eachField,form.value[eachField]);
        }

        if(this.invoiceFile !='')
        	formData.append('invoiceFile', this.invoiceFile, this.invoiceFile.name);
        if(this.itemPhotoFile !='')
        	formData.append('itemPhotoFile', this.itemPhotoFile, this.itemPhotoFile.name);
        if(this.trackingDate !='')
        	formData.append('trackingDate',this.trackingDate);
        if(this.trackingTime !='')
        	formData.append('trackingTime',this.trackingTime);

        this.content.verifyTrackingDetails(formData).subscribe((data:any) => {
        	this.spinner.hide();
        	if(data.status == '1')
        	{
        		this.toastr.success('Request successfully send. We will get back to you soon.');
        		form.resetForm();
        	}
        	else
        	{
        		this.toastr.error('Something went wrong!!')
        	}
        })

	}

	isValid(event: boolean): void {
	    this.valid = event;
	}
}
