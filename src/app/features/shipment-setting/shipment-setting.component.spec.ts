import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShipmentSettingComponent } from './shipment-setting.component';

describe('ShipmentSettingComponent', () => {
	let component: ShipmentSettingComponent;
	let fixture: ComponentFixture<ShipmentSettingComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ShipmentSettingComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ShipmentSettingComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
