/*import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HelpComponent } from './help.component';
import { GetStartComponent } from './get-start/get-start.component';


const routes: Routes = [
	{
		path: '', redirectTo: 'get-start', pathMatch: 'full'
	},
	{
		path: '',
		component: HelpComponent,
		children: [
			{ path: 'get-start', component: GetStartComponent },
		]
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class HelpRoutingModule {

}*/


import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HelpComponent } from './help.component';

const routes: Routes = [
	{ path: '', component: HelpComponent }
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class HelpRoutingModule {

}