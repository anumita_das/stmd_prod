import { NgModule } from '@angular/core';
import { ReferAFriendRoutingModule } from './refer-a-friend-routing.module';
import { ReferAFriendComponent } from './refer-a-friend.component';
import { SharedModule } from '@app/shared';

@NgModule({
	imports: [
		ReferAFriendRoutingModule,
		SharedModule
	],
	declarations: [ReferAFriendComponent]
})
export class ReferAFriendModule {

}
