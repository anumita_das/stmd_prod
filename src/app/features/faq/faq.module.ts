import { NgModule } from '@angular/core';
import { FAQRoutingModule } from './faq-routing.module';
import { FAQComponent } from './faq.component';
import { SharedModule } from '@app/shared';
import { HttpModule }    from '@angular/http';

@NgModule({
	imports: [
		FAQRoutingModule,
		SharedModule,
		HttpModule
	],
	declarations: [
		FAQComponent
	]
})
export class FAQModule {

}
