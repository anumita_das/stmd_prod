import { Component, OnInit, NgZone, HostListener } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { RecipientModalComponent } from '../../../shared/components/recipient-modal/recipient-modal.component';
import { PhotoShotModalComponent } from '../../../shared/components/photo-shot-modal/photo-shot-modal.component';
import { RecountModalComponent } from '../../../shared/components/recount-modal/recount-modal.component';
import { ReweighModalComponent } from '../../../shared/components/reweigh-modal/reweigh-modal.component';
import { ItempriceModalComponent } from '../../../shared/components/itemprice-modal/itemprice-modal.component';
import { WronginventoryModalComponent } from '../../../shared/components/wronginventory-modal/wronginventory-modal.component';
import { MywarehouseService } from '../../../services/mywarehouse.service';
import { RegistrationService } from '../../../services/registration.service';
import { ContentService } from '../../../services/content.service';
import { AppGlobals } from '../../../app.globals';
import { ToastrService } from 'ngx-toastr';
import { Router } from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';
import { WarehouseModalComponent } from '../../../shared/components/warehouse-modal/warehouse-modal.component';
import { DatePipe } from '@angular/common';
import { ReturnModalComponent } from '../../../shared/components/return-modal/return-modal.component';
import { PhotoshotModalComponent } from './photoshot-modal.component';
import { ContactFormModalComponent } from '../../../shared/components/contactform-modal/contactform-modal.component';

@Component({
	selector: 'app-overview',
	templateUrl: './overview.component.html',
	styleUrls: ['./overview.component.scss'],
	providers: [DatePipe]
})
export class OverviewComponent implements OnInit {
	isMediumScreen: boolean;
	sidebar: boolean;
	isCollapsed: boolean;
	bsModalRef: BsModalRef;
	dtOptions: DataTables.Settings = {};
	showShippingCost: boolean;
	shippingTypes: Array<any>;
	warehouseShipmentList: Array<any> = [];
	showShipping: boolean = false;
	userId: number;
	defaultCurrencySymbol: any;
	shipment: any;
	shippingMethodList: Array<any>;
	allWarehouse: Array<object>;
	selectedWarehouse: number;
	selectedWarehouseDetails: any;
	defaultProfile: string;
	userShippingAddress: Array<object>;
	earnedPoints: any;
	rewardName: any;
	expString: any;
	showCalculateBtn: boolean = false;
	today: any;
	bannerImage: any;
	bannerShoptomydoor: Array<object>;
	enableCheckout: boolean = false;
	totalShipingValue: any;
	showInventorymsg: boolean = false;
	deliverySection: Array<any> = [false];
	deliveryData: Array<any> = [];
	showShippingMethod: Array<any> = [false];
	closeDelivery: Array<any> = [false];
	showSection: Array<any> = [];
	showRepresentative: boolean = false;
	representativeDetail: any;
	representativeImage: any;
	userSubscription : any = "";
	userSubscriptionData : any = "";

	constructor(
		private ngZone: NgZone,
		private modalService: BsModalService,
		public mywarehouseService: MywarehouseService,
		public regService: RegistrationService,
		private content: ContentService,
		private _global: AppGlobals,
		private toastr: ToastrService,
		private spinner: NgxSpinnerService,
		private router: Router,
		private datePipe: DatePipe
	) {
		this.userId = this.regService.item.user.id;
		this.defaultCurrencySymbol = this._global.defaultCurrencySymbol;
		this.totalShipingValue = "$0.00";

		localStorage.removeItem('userWarehouseCart');
	}

	ngOnInit() {
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		this.checkWindowSize(width);

		// account isCollapsed
		this.isCollapsed = false;

		this.defaultProfile = JSON.parse(localStorage.getItem('profileImage'));

		this.getUserAddressInfo();
		this.getRewardDetails();
		this.getAllWarehouse();
		this.getAccountManagerDetails();
		this.allWarehouse = [];
		this.bannerShoptomydoor = [];

		this.today = this.datePipe.transform(new Date(), 'yyyy-MM-dd');

		setTimeout(() => {
			this.getUserWarehouseList();

		}, 1000);

		// datatable
		this.dtOptions = {
			paging: false,
			// bInfo: false,
			ordering: false,
			searching: false,
			// bSort: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};
		localStorage.setItem('selectedWarehouse', JSON.stringify(0));
		this.userSubscription = this.regService.item.user.isSubscribed;
		this.getSubscriptionDetails();
	}

	processPayeezy() {
		this.mywarehouseService.preocessPayeezy().subscribe((data: any) => {
			console.log(data);
		})
	}

	getSubscriptionDetails () {
		if(this.regService.item.user.isSubscribed == 'Y'){
			this.regService.getusersubscriptiondetail(this.regService.item.user.id).subscribe((response:any)=>{
	            if(response.status == 1)
	             this.userSubscriptionData = response.results;
	        });
		}
	}

	getAccountManagerDetails() {
            if(this.regService.item.user.subscription.representativeId){
		this.mywarehouseService.getAccountManager(this.regService.item.user.subscription.representativeId).subscribe((data: any) => {
			if (data.status == 1) {
				this.representativeDetail = data.results.userdetails;
				this.representativeImage = data.results.userimage;
				this.showRepresentative = true;
			}
			else {
				this.showRepresentative = false;
			}
		});
           } else {
				this.showRepresentative = false;
           }
	}

	openContactForm(event, firstName, lastName, email, contactno) {
		this.bsModalRef = this.modalService.show(ContactFormModalComponent, {
			class: 'modal-lg modal-dialog-centered'

		});

		this.bsModalRef.content.firstName = firstName;
		this.bsModalRef.content.lastName = lastName;
		this.bsModalRef.content.email = email;
		this.bsModalRef.content.contactno = contactno;
		this.bsModalRef.content.userId = this.userId;
		this.bsModalRef.content.closeBtnName = 'Close';

		this.modalService.onHide.subscribe(() => {
			this.spinner.show();

			if (this.bsModalRef.content.isPhotoShotDisabled == 'Y') {

			} else {

			}

			window.location.reload();
		});
	}

	getUserAddressInfo() {
		this.regService.userShippingAddress('shipping').subscribe((data: any) => {
			if (data.status == 1) {
				this.userShippingAddress = data.results;
			}
		});
	}

	getRewardDetails() {
		this.content.getRewardDetails(this.userId).subscribe((data: any) => {
			this.earnedPoints = data.results.earnedPoints;
			this.rewardName = data.results.rewardName;
			this.expString = data.results.expStringShort;
		});
	}

	getAllWarehouse() {
		this.content.allWarehouse().subscribe((data: any) => {
			let index = 0;
			if (data.status == 1) {
				for (let eachWarehouse of data.results) {
					this.allWarehouse.push(eachWarehouse);
					if (index == JSON.parse(localStorage.getItem('selectedWarehouse'))) {
						this.selectedWarehouseDetails = eachWarehouse;
					}
					index++;
				}

				for (let bannerImage of data.banner) {
					this.bannerShoptomydoor.push({ 'imagePath': bannerImage.imagePath, 'link': bannerImage.pageLink });
				}
				/*if(data.results.warehouseBannerImage!='')
				{
					this.bannerImage = data.banner;
				}
				else{
					this.bannerImage = "";
				}*/
			}
			this.selectedWarehouse = JSON.parse(localStorage.getItem('selectedWarehouse'));
		});
	}

	selectWarehose(event) {
		let index = 0;
		var selectedDrop = event.target.value;
		localStorage.setItem('selectedWarehouse', JSON.stringify(selectedDrop));
		this.selectedWarehouse = selectedDrop;
		for (let eachWarehouse of this.allWarehouse) {
			if (index == selectedDrop) {
				this.selectedWarehouseDetails = eachWarehouse;
			}
			index++;
		}

		this.getUserWarehouseList();


		let el = document.getElementById('warehouseAddress');
		el.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'end' });
	}

	showWarehouseAddress() {
		this.bsModalRef = this.modalService.show(WarehouseModalComponent, {});
		this.bsModalRef.content.warehouseAddress = this.selectedWarehouseDetails;
		this.bsModalRef.content.closeBtnName = 'Close';
	}

	getUserWarehouseList() {
		this.spinner.show();

		this.mywarehouseService.getUserShipmentList(this.selectedWarehouseDetails).subscribe((data: any) => {
			this.spinner.hide();
			if (data.shipments) {
				console.log(data.shipments);
				this.warehouseShipmentList = data.shipments;

				var array = this.hack(this.warehouseShipmentList);
				array.forEach(function (item, index) {

					this.showSection[index] = 0;
					this.deliveryData[index] = [];
					this.closeDelivery[index] = false;
					this.deliverySection[index] = false;

					this.warehouseShipmentList[index].delivery = [];
					this.warehouseShipmentList[index].enableCheckout = false;


					this.warehouseShipmentList[index].wrongInventory = "";
					this.warehouseShipmentList[index].showInventorymsg = true;
				}, this);

			} else {
				this.warehouseShipmentList = [];
			}
		});

	}



	showDelivery(shipment, shipmentId, index) {
		this.deliveryData[index] = [];
		this.closeDelivery[index] = false;
		this.showSection[index] = [];
		const formData = { userId: this.userId, shipmentId: shipmentId, shipment: shipment };
		this.mywarehouseService.getDeliveryDetails(formData).subscribe((response: any) => {
			this.deliverySection[index] = true;
			this.closeDelivery[index] = true;
			this.showSection[index] = 1;

			this.deliveryData[index] = response.deliveryData[0];

			this.warehouseShipmentList[index].delivery = response.deliveryData[0];

			this.displayShippingCost(shipment, shipmentId, this.deliveryData[index], index);
			this.warehouseShipmentList[index].enableCheckout = false;


		});

	}

	hideDelivery(shipment, shipmentId, index) {
		this.deliverySection[index] = false;
		this.closeDelivery[index] = false;
		this.showSection[index] = 0;
	}

	displayShippingCost(shipment, shipmentId, deliveryData, index) {

		let shippingMethodNull = true;

		var array = this.hack(shipment['delivery'].deliveries);
		if (array) {
			array.forEach(function (row, index) {
				if (row['shippingMethodId'] != "") {
					shippingMethodNull = false;
				}
			});
		}


		if (shippingMethodNull) {
			const formData = { userId: this.userId, shipmentId: shipmentId, shipment: shipment, deliveries: deliveryData };
			this.mywarehouseService.allShippingMethodList(formData).subscribe((response: any) => {
				this.warehouseShipmentList[index].shippingMethodList = this.hack(response.shippingcharges);
				this.warehouseShipmentList[index].enableCheckout = false;
			});
		}
	}

	selectShippingMethod(shippngId, shipmentId, index, deliveryData, totalShipingValue) {
		this.spinner.show();

		this.warehouseShipmentList[index].selectedShippingMethod = shippngId;

		this.totalShipingValue = totalShipingValue;

		this.warehouseShipmentList[index].delivery = deliveryData;

		var array = this.hack(deliveryData.deliveries);
		array.forEach(function (row) {
			let deliveryId = row['id'];
			this.warehouseShipmentList[index].delivery.deliveries[deliveryId].shippingMethodId = shippngId;

			/*Calculate & Show Shipment Delivery Cost*/
			let totalQuantity = this.warehouseShipmentList[index].delivery.deliveries[deliveryId].totalQty;
			const formData = { userId: this.userId, shipmentId: shipmentId, deliveryId: deliveryId, totalQuantity: totalQuantity, shippingId: shippngId };
			this.mywarehouseService.calculateDeliveryShippingCost(formData).subscribe((response: any) => {
				if (response.status == 1) {
					this.warehouseShipmentList[index].delivery.deliveries[deliveryId].shippingCost = response.data.totalShippingCost;
					this.warehouseShipmentList[index].delivery.deliveries[deliveryId].totalPrevCost = this.warehouseShipmentList[index].delivery.deliveries[deliveryId].totalCost;
					this.warehouseShipmentList[index].delivery.deliveries[deliveryId].shippingCost = response.data.shippingCost;
					this.warehouseShipmentList[index].delivery.deliveries[deliveryId].clearingDutyCost = response.data.clearingDuty;
					this.warehouseShipmentList[index].delivery.deliveries[deliveryId].isDutyCharged = response.data.isDutyCharged;
					this.warehouseShipmentList[index].delivery.deliveries[deliveryId].totalDeliveryCost = response.data.total;
					this.warehouseShipmentList[index].delivery.deliveries[deliveryId].shippingMethod = response.data.shipping;
					this.warehouseShipmentList[index].delivery.deliveries[deliveryId].otherCharge = response.data.otherCharge;

					const formData = {
						userId: this.userId,
						data: this.warehouseShipmentList[index],
					};
					this.mywarehouseService.getshippimgmethod(formData).subscribe((response: any) => {

						this.warehouseShipmentList[index].showShipping = true;
						this.warehouseShipmentList[index].shipment = response.shipment;
						this.warehouseShipmentList[index].wrongInventory = 'N';
						this.warehouseShipmentList[index].enableCheckout = false;
						let wrongInventory = '';
						var array = this.hack(this.warehouseShipmentList[index].delivery.deliveries);
						array.forEach(function (item) {
							if ((item['wrongInventory'] == "Y") && item['shippingMethodId'] != '' && item['deliveryAllItemReturned'] == '0') {
								this.warehouseShipmentList[index].wrongInventory = 'Y';
								this.warehouseShipmentList[index].enableCheckout = true;
							}
						}, this);

						this.spinner.hide();
					});
				}

			});
		}, this);

		/*setTimeout(() => {
          const formData =  {
				userId : this.userId,
				data : this.warehouseShipmentList[index],
			};

			this.mywarehouseService.getshippimgmethod(formData).subscribe((response:any) => {
				console.log(response);
				this.warehouseShipmentList[index].showShipping = true;
				this.warehouseShipmentList[index].shipment = response.shipment;
				this.warehouseShipmentList[index].wrongInventory = 'N';
				this.warehouseShipmentList[index].enableCheckout = false;
				let wrongInventory = ''; 
				var array = this.hack(this.warehouseShipmentList[index].delivery.deliveries);
				array.forEach(function (item) {
		  			if((item['wrongInventory'] == "Y") && item['shippingMethodId'] != '' && item['deliveryAllItemReturned'] == '0'){
		  				this.warehouseShipmentList[index].wrongInventory = 'Y';
		  				this.warehouseShipmentList[index].enableCheckout = true;
		  			}
				}, this);

				this.spinner.hide();
			});
          }, 3000);*/
	}

	calculateDeliveryShippingCost($event, shipmentId, deliveryId, deliveryData, index) {
		this.spinner.show();
		var shippingMethodId = deliveryData.deliveries[deliveryId].shippingMethodId;

		var wronginventory = 0;

		//Hide Shipment Delivery Cost
		this.warehouseShipmentList[index].showShipping = false;
		this.warehouseShipmentList[index].wrongInventory = 'N';
		//let calculateBtn =  false;
		let calculateBtn = true;

		if (shippingMethodId != "") {
			let totalQuantity = deliveryData.deliveries[deliveryId].totalQty;
			const formData = { userId: this.userId, shipmentId: shipmentId, deliveryId: deliveryId, totalQuantity: totalQuantity, shippingId: shippingMethodId };
			this.mywarehouseService.calculateDeliveryShippingCost(formData).subscribe((response: any) => {
				if (response.status == 1) {
					deliveryData.deliveries[deliveryId].shippingCost = response.data.totalShippingCost;
					deliveryData.deliveries[deliveryId].totalPrevCost = deliveryData.deliveries[deliveryId].totalCost;
					deliveryData.deliveries[deliveryId].shippingCost = response.data.shippingCost;
					deliveryData.deliveries[deliveryId].clearingDutyCost = response.data.clearingDuty;
					deliveryData.deliveries[deliveryId].isDutyCharged = response.data.isDutyCharged;
					deliveryData.deliveries[deliveryId].totalDeliveryCost = response.data.total;
					deliveryData.deliveries[deliveryId].shippingMethod = response.data.shipping;

					deliveryData.deliveries[deliveryId].otherCharge = response.data.otherCharge;

					var array = this.hack(deliveryData.deliveries);
					array.forEach(function (row) {
						let item = row['id'];
						let deliveryShippingMethodId = deliveryData.deliveries[item].shippingMethodId;
						if (shippingMethodId != deliveryShippingMethodId) {
							calculateBtn = true;
						}

					}, this);

					if (calculateBtn == true)
						this.warehouseShipmentList[index].showCalculateBtn = true;
				}

				setTimeout(() => {
					this.spinner.hide();
				}, 3000);
			});
		} else {
			this.warehouseShipmentList[index].shippingMethodId = '';
			deliveryData.deliveries[deliveryId].shippingCost = this.defaultCurrencySymbol + ' 0.00';
			deliveryData.delivery.deliveries[deliveryId].totalCost = deliveryData.deliveries[deliveryId].totalPrevCost;

			setTimeout(() => {
				this.spinner.hide();
			}, 2000);
		}
	}

	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		this.checkWindowSize(event.target.innerWidth);
	}

	calculateShippingCost(shipmentId, deliveryData, index) {

		this.spinner.show();
		var shippingMethodNull = false;
		let wrongInventory = 0;
		let countmsg = 0;
		this.warehouseShipmentList[index].wrongInventory = 'N';

		this.warehouseShipmentList[index].showInventorymsg = true;
		this.warehouseShipmentList[index].delivery = deliveryData;
		var array = this.hack(deliveryData.deliveries);
		var arraySize = array.length;
		array.forEach(function (item) {
			if (item['shippingMethodId'] == "" && item['wrongInventory'] == "" && item['deliveryAllItemReturned'] == '0') {
				shippingMethodNull = true;
			}

			if ((item['wrongInventory'] == "Y") && item['shippingMethodId'] != '' && item['deliveryAllItemReturned'] == '0') {

				wrongInventory += 1;
			}
			if (item['deliveryAllItemReturned'] == "1") {
				arraySize -= 1;
			}
			if (item['wrongInventory'] == "Y" && item['deliveryAllItemReturned'] == '0') {
				countmsg += 1;
			}
		});

		if (wrongInventory == arraySize)
			this.warehouseShipmentList[index].wrongInventory = 'Y';

		if (countmsg == arraySize)
			this.warehouseShipmentList[index].showInventorymsg = false;

		if (!shippingMethodNull) {
			const formData = {
				userId: this.userId,
				data: this.warehouseShipmentList[index],
			};
			this.mywarehouseService.getshippimgmethod(formData).subscribe((response: any) => {
				this.warehouseShipmentList[index].showShipping = true;
				this.warehouseShipmentList[index].shipment = response.shipment;
				this.warehouseShipmentList[index].shippingTypes = this.hack(response.shippingcharges);
				//this.warehouseShipmentList[index].showShippingCharges = this.hack(response.shippingcharges);	

				//this.warehouseShipmentList[index].wrongInventory = 'Y';
				//let wrongInventory = ''; 
				/*var array = this.hack(deliveryData.deliveries);
				array.forEach(function (item) {
		  			if(item['wrongInventory'] == "" || item['wrongInventory'] == "N"){
		  				this.warehouseShipmentList[index].wrongInventory = 'N';
		  			}
				}, this);*/

				this.totalShipingValue = response.shipment.totalShippingCost;

				setTimeout(() => {
					this.spinner.hide();
				}, 2000);


			});
		} else {
			this.toastr.error("Please select shipping method for all deliveries to continue!!");
			setTimeout(() => {
				this.spinner.hide();
			}, 2000);

		}
	}

	// modal
	openRecipientModal() {
		this.bsModalRef = this.modalService.show(RecipientModalComponent, {
			class: 'modal-xs modal-dialog-centered'
		});
		this.bsModalRef.content.closeBtnName = 'Close';
	}

	// modal photo shot
	openPhotoShotModal(event, deliveryId, shipmentId, deliveryData, index) {
		if (event == 'Y') {
			this.bsModalRef = this.modalService.show(PhotoShotModalComponent, {
				class: 'modal-sm modal-dialog-centered'

			});

			this.bsModalRef.content.deliveryId = deliveryId;
			this.bsModalRef.content.shipmentId = shipmentId;
			this.bsModalRef.content.photoshotCost = this.warehouseShipmentList[index].delivery_snap_shot;
			this.bsModalRef.content.closeBtnName = 'Close';
			this.warehouseShipmentList[index].delivery = deliveryData;

			this.modalService.onHide.subscribe(() => {
				this.spinner.show();

				if (this.bsModalRef.content.isPhotoShotDisabled == 'Y') {
					this.warehouseShipmentList[index].delivery.deliveries[deliveryId].snapshot = this.bsModalRef.content.isPhotoShotDisabled;

					//Disable Individual Packages
					var packages = this.hack(deliveryData.deliveries[deliveryId].packages);
					packages.forEach(function (item) {
						item['snapshotOpt'] = 'Y';
					});

					this.warehouseShipmentList[index].delivery.deliveries[deliveryId].otherChargeCost = this.bsModalRef.content.shipment.otherChargeAmount;
					this.warehouseShipmentList[index].delivery.deliveries[deliveryId].totalCost = this.bsModalRef.content.shipment.totalCost;

					this.toastr.success('Charge added successfully');
				} else {
					deliveryData.deliveries[deliveryId].snapshot = 'N';
				}

				window.location.reload();
			});
		}
	}

	// modal package photo shot
	openPackagePhotoShotModal(event, deliveryId, shipmentId, packageId, index, packageIndex) {
		this.bsModalRef = this.modalService.show(PhotoShotModalComponent, {
			class: 'modal-sm modal-dialog-centered'

		});

		this.bsModalRef.content.deliveryId = deliveryId;
		this.bsModalRef.content.packageId = packageId;
		this.bsModalRef.content.shipmentId = shipmentId;
		this.bsModalRef.content.closeBtnName = 'Close';
		this.bsModalRef.content.packagetype = 'package';
		this.bsModalRef.content.photoshotCost = this.warehouseShipmentList[index].item_snap_shot;

		this.modalService.onHide.subscribe(() => {
			if (this.bsModalRef.content.isPhotoShotDisabled == 'Y') {
				this.warehouseShipmentList[index].delivery.deliveries[deliveryId].packages[packageIndex] = this.bsModalRef.content.isPhotoShotDisabled;
				this.warehouseShipmentList[index].delivery.deliveries[deliveryId].otherChargeCost = this.bsModalRef.content.shipment.otherChargeAmount;
				this.warehouseShipmentList[index].delivery.deliveries[deliveryId].totalCost = this.bsModalRef.content.shipment.totalCost;
			}

			window.location.reload();
		});
	}


	// modal recount
	openRecountModal(event, deliveryId, shipmentId, deliveryData, index) {
		if (event == 'Y') {
			this.bsModalRef = this.modalService.show(RecountModalComponent, {
				class: 'modal-sm modal-dialog-centered'

			});

			this.bsModalRef.content.deliveryId = deliveryId;
			this.bsModalRef.content.shipmentId = shipmentId;
			this.bsModalRef.content.recountCost = this.warehouseShipmentList[index].recount_cost;
			this.bsModalRef.content.closeBtnName = 'Close';

			this.modalService.onHide.subscribe(() => {
				this.spinner.show();

				if (this.bsModalRef.content.isRecountDisabled == 'Y') {
					this.warehouseShipmentList[index].delivery.deliveries[deliveryId].recount = this.bsModalRef.content.isRecountDisabled;
					this.warehouseShipmentList[index].delivery.deliveries[deliveryId].otherChargeCost = this.bsModalRef.content.shipment.otherChargeAmount;
					this.warehouseShipmentList[index].delivery.deliveries[deliveryId].totalCost = this.bsModalRef.content.shipment.totalCost;

					this.toastr.success('Charge added successfully');
				} else {
					this.warehouseShipmentList[index].delivery.deliveries[deliveryId].recount = 'N';
				}

				window.location.reload();
			});

		}
	}

	// modal reweigh
	openReweighModal(event, deliveryId, shipmentId, deliveryData, index) {
		if (event == 'Y') {
			this.bsModalRef = this.modalService.show(ReweighModalComponent, {
				class: 'modal-sm modal-dialog-centered'

			});

			this.bsModalRef.content.deliveryId = deliveryId;
			this.bsModalRef.content.shipmentId = shipmentId;
			this.bsModalRef.content.reweighCost = this.warehouseShipmentList[index].reweight_cost;
			this.bsModalRef.content.closeBtnName = 'Close';

			this.modalService.onHide.subscribe(() => {
				this.spinner.show();

				if (this.bsModalRef.content.isReweighDisabled == 'Y') {
					deliveryData.deliveries[deliveryId].reweigh = this.bsModalRef.content.isReweighDisabled;
					deliveryData.deliveries[deliveryId].otherChargeCost = this.bsModalRef.content.shipment.otherChargeAmount;
					deliveryData.deliveries[deliveryId].totalCost = this.bsModalRef.content.shipment.totalCost;

					this.toastr.success('Charge added successfully');
				} else {
					deliveryData.deliveries[deliveryId].reweigh = 'N';
				}

				window.location.reload();
			});

		}
	}

	//Show images modal

	openGalleryModal(shipmentId, deliveryId, itemId, index) {
		this.mywarehouseService.getShipmentImages(shipmentId, deliveryId, itemId).subscribe((response: any) => {

			if (response.results.length > 0) {
				this.bsModalRef = this.modalService.show(PhotoshotModalComponent, {
					class: 'modal-sm modal-dialog-centered'
				});

				this.bsModalRef.content.snapimages = response.results;

				this.bsModalRef.content.closeBtnName = 'Close';

			}

		});
	}

	//modal Return
	openReturnModal(event, deliveryId, shipmentId, deliveryData, index) {
		if (event == 'Y') {
			this.bsModalRef = this.modalService.show(ReturnModalComponent, {
				class: 'modal-lg modal-dialog-centered'

			});

			this.bsModalRef.content.deliveryId = deliveryId;
			this.bsModalRef.content.shipmentId = shipmentId;
			this.bsModalRef.content.returnCost = this.warehouseShipmentList[index].return_cost;
			this.bsModalRef.content.closeBtnName = 'Close';

			this.modalService.onHide.subscribe(() => {
				this.spinner.show();

				if (this.bsModalRef.content.isReturnDisabled == 'Y') {

				} else {
					deliveryData.deliveries[deliveryId].returnItem = 'N';
				}

				window.location.reload();
			});

		}

	}

	openItempriceEditModal(packageId, minPrice, maxPrice, shipmentIndex, packageIndex, deliveryId) {
		this.bsModalRef = this.modalService.show(ItempriceModalComponent, {
			class: 'modal-sm modal-dialog-centered'
		});
		this.bsModalRef.content.packageId = packageId;
		this.bsModalRef.content.minPrice = minPrice;
		this.bsModalRef.content.maxPrice = maxPrice;
		this.bsModalRef.content.deliveryId = deliveryId;
		this.modalService.onHide.subscribe(() => {
			this.getUserWarehouseList();
			//if(this.bsModalRef.content.valueEntered == 'max')
			//{
			//this.warehouseShipmentList[shipmentIndex].delivery.deliveries[deliveryId].packages[packageIndex].itemMaxPrice = this.bsModalRef.content.maxPrice;
			//this.warehouseShipmentList[shipmentIndex].delivery.deliveries[deliveryId].packages[packageIndex].itemPrice = this.bsModalRef.content.itemPrice;
			//this.warehouseShipmentList[shipmentIndex].delivery.deliveries[deliveryId].packages[packageIndex].itemTotalCost = this.bsModalRef.content.totalPrice;
			//window.location.reload();
			//this.getUserWarehouseList();
			//}


		});

	}

	openCheckoutModal(event, deliveryId, shipmentId, index, deliveryIndex) {
		if (event == 'Y') {
			const formData = { deliveryId: deliveryId, shipmentId: shipmentId };
			this.mywarehouseService.updateinventory(formData).subscribe((data: any) => {

			});
		} else if (event == 'N') {
			this.bsModalRef = this.modalService.show(WronginventoryModalComponent, {
				class: 'modal-sm modal-dialog-centered'
			});

			this.bsModalRef.content.shipmentId = shipmentId;
			this.bsModalRef.content.deliveryId = deliveryIndex + 1;
			this.bsModalRef.content.closeBtnName = 'Close';

			this.modalService.onHide.subscribe(() => {
				if (this.bsModalRef.content.isWrongInventoryDisabled == 'Y')
					this.warehouseShipmentList[index].delivery.deliveries[deliveryId].wrongInventory = "N";
				else
					this.warehouseShipmentList[index].delivery.deliveries[deliveryId].wrongInventory = '';
			});
		}

		this.warehouseShipmentList[index].wrongInventory = 'N';
		this.warehouseShipmentList[index].enableCheckout = false;
		this.warehouseShipmentList[index].showInventorymsg = true;
		let wrongInventory = 0;
		let countmsg = 0;
		var array = this.hack(this.warehouseShipmentList[index].delivery.deliveries);
		var arraySize = array.length;
		array.forEach(function (item) {
			if ((item['wrongInventory'] == "Y") && item['shippingMethodId'] != '' && item['deliveryAllItemReturned'] == "0") {

				wrongInventory += 1;
			}
			if (item['deliveryAllItemReturned'] == "1") {
				arraySize -= 1;
			}
			if (item['wrongInventory'] == "Y" && item['deliveryAllItemReturned'] == "0") {
				countmsg += 1;
			}


		}, this);


		if (wrongInventory == arraySize) {
			if (this.warehouseShipmentList[index].showCalculateBtn == true && this.warehouseShipmentList[index].showShipping == false) {
				this.warehouseShipmentList[index].wrongInventory = 'N';
				this.warehouseShipmentList[index].enableCheckout = false;
			} else if (this.warehouseShipmentList[index].showCalculateBtn == true && this.warehouseShipmentList[index].showShipping == true) {
				this.warehouseShipmentList[index].wrongInventory = 'Y';
				this.warehouseShipmentList[index].enableCheckout = true;

			} else {
				this.warehouseShipmentList[index].wrongInventory = 'Y';
				this.warehouseShipmentList[index].enableCheckout = true;
			}

		}
		if (countmsg == arraySize) {
			this.warehouseShipmentList[index].showInventorymsg = false;
		}


	}


	//Save Cart Items For Procurement
	checkout($event, shipmentId, index) {
		this.spinner.show();
		const formData = this.warehouseShipmentList[index];

		this.mywarehouseService.warehouseCheckout(formData).subscribe((response: any) => {
			if (response.status == 1) {
				localStorage.removeItem('userWarehouseCart');
				localStorage.setItem('userWarehouseCart', JSON.stringify(response.shipment));
				this.router.navigate(['my-warehouse', 'checkout']);
			} else {
				this.toastr.error("Error in operation. Please try again later!!");
			}

			setTimeout(() => {
				this.spinner.hide();
			}, 2000);
		});

	}

	hack(value) {

		if (value != undefined && value != null) {
			return Object.values(value);
		}

	}

	fileUpload(event, shipmentId, i) {
		let fileItem = event.target.files[0];
		const fileSelected: File = fileItem;
		this.mywarehouseService.uploadInvoice(fileSelected, shipmentId).subscribe((data: any) => {
			if (data.status == 1) {
				this.toastr.success('File Uploaded Successfully');
				this.warehouseShipmentList[i].wrongInvoice = 'Y';
				this.warehouseShipmentList[i].wrongInvoiceFile = data.results;
			}
			else {
				this.toastr.error(data.results.fileItem);
			}
		});
	}


	downloadInvoice(packageId) {

		this.mywarehouseService.downloadInvoice(packageId).subscribe((data: any) => {

		});

	}

}
