import { NgModule } from '@angular/core';
import { MyWarehouseRoutingModule } from './my-warehouse-routing.module';
import { MyWarehouseComponent } from './my-warehouse.component';
import { SharedModule } from '@app/shared';
import { OverviewComponent } from './overview/overview.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { CartComponent } from './checkout/cart/cart.component';
import { PersonalDetailComponent } from './checkout/personal-detail/personal-detail.component';
import { ShippingAndPaymentComponent } from './checkout/shipping-and-payment/shipping-and-payment.component';
import { PlaceOrderComponent } from './checkout/place-order/place-order.component';
import { PhotoshotModalComponent } from './overview/photoshot-modal.component';
import { PaymentComponent } from './payment/payment.component';


@NgModule({
	imports: [
		MyWarehouseRoutingModule,
		SharedModule
	],
	exports: [
		PhotoshotModalComponent
	],
	declarations: [
		MyWarehouseComponent,
		OverviewComponent,
		CheckoutComponent,
		CartComponent,
		PersonalDetailComponent,
		ShippingAndPaymentComponent,
		PlaceOrderComponent,
		PhotoshotModalComponent,
		PaymentComponent
	],
	entryComponents: [
		PhotoshotModalComponent
	]
})
export class MyWarehouseModule {

}
