import { Component, OnInit } from '@angular/core';
import { MywarehouseService } from '../../../../services/mywarehouse.service';
import { Router} from "@angular/router";
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { CurrencyModalComponent } from '../../../../shared/components/currency-modal/currency-modal.component';
import { RegistrationService } from '../../../../services/registration.service';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-cart',
	templateUrl: './cart.component.html',
	styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
	dtOptions: DataTables.Settings = {};
	bagList : Array<any> = [];
	
	cartDetails : any;
	showDiscount = false;
	bsModalRef: BsModalRef;
    exchangeRate: any;
    currencySymbol: any = '';
    defaultCurrencySymbol: any = '';
    isCurrencyChanged: any = 'N';
    couponcode: string;
    couponInValidMsg: string;
    couponInValid:boolean = false;
    blankCouponCode:boolean = true;
	personaldetails : Array<any> = [];
	user : Array<any>;


	constructor(
		private  mywarehouseService : MywarehouseService,
		private auth: RegistrationService,
		private router: Router,
		private modalService: BsModalService,
		private toastr: ToastrService,
	) {
		this.user = this.auth.item.user;
	 }

	ngOnInit() {
		// datatable
		this.dtOptions = {
			paging: false,
			ordering: false,
			searching: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};

		this.getUserCartList();
	}

	//Fetch Cart List from Session
  	getUserCartList() {
  		const usercart = JSON.parse(localStorage.getItem('userWarehouseCart'));
  		console.log(usercart);
  		this.cartDetails = usercart;
  		this.isCurrencyChanged = usercart.isCurrencyChanged;
  		this.currencySymbol = usercart.currencySymbol;
  		this.defaultCurrencySymbol = usercart.defaultCurrencySymbol;
  		this.exchangeRate = usercart.exchangeRate;
	}


	showCurrency() {
		
		const usercart = JSON.parse(localStorage.getItem('userWarehouseCart'));
		const currencyCode = usercart.currencyCode;

		this.mywarehouseService.getCurrencyList(currencyCode).subscribe((response:any) => {
			this.bsModalRef = this.modalService.show(CurrencyModalComponent, {});
			this.bsModalRef.content.currencyList = response;
		});

	}

	addInsurance($event) {
		const usercart = JSON.parse(localStorage.getItem('userWarehouseCart'));

		if($event.target.checked == true){
			this.cartDetails['isInsuranceCharged']  = 'Y';
			this.cartDetails['totalCost'] = parseFloat(this.cartDetails['totalInsurance'])+parseFloat(this.cartDetails['totalCost']);	
		} else {
			this.cartDetails['isInsuranceCharged'] = 'N';	
			this.cartDetails['totalCost'] = parseFloat(this.cartDetails['totalCost'])-parseFloat(this.cartDetails['totalInsurance']);
		}

		//Reassign Storage Data
	    localStorage.setItem('userWarehouseCart', JSON.stringify(this.cartDetails));
	}

	checkout() {
		this.router.navigate(['/my-warehouse','checkout','personal-detail']);
	}

	clearcart() {
		localStorage.removeItem('userWarehouseCart');
		this.router.navigate(['/my-warehouse','overview']);
	}

	hack(value) {
	   return Object.values(value)
	}


	checkEmptyCouponInput(){
		if(this.couponcode == ''){
			this.blankCouponCode = true;
		} else {
			this.blankCouponCode = false;
		}
	}


}
