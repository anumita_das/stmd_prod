import { Component, OnInit } from '@angular/core';
import { ContentService } from '../../../../services/content.service';
import { RegistrationService } from '../../../../services/registration.service';
import { SharedModule } from '@app/shared';
import { NgForm }   from '@angular/forms';
import {Router} from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
	selector: 'app-personal-detail',
	templateUrl: './personal-detail.component.html',
	styleUrls: ['./personal-detail.component.scss'],
})
export class PersonalDetailComponent implements OnInit {
	billForm = true;
	isReadOnly = true;
	countryList : any;
	stateList:any;
	cityList:any;
	userDetails: any = [];
	userDefaultShippingAddress : any = [];
	userDefaultBillingAddress : any = [];
	shippingCountryName  : any;
	shippingStateName  : any;
	shippingCityName : any;
	billingCountryName : any;
	billingStateName : any;
	billingCityName : any;
	defaultBillingCountryId : any;
	dataSubmit : boolean = true;

	constructor(
		private content:ContentService,
		private auth: RegistrationService,
		private router: Router,
		public spinner: NgxSpinnerService
	) {
		this.spinner.show();
		if("userWarehouseCart" in localStorage){
		  	var usercart = localStorage.getItem('userWarehouseCart');
			if(usercart == null || usercart == ""){
			 	this.router.navigate(['my-warehouse','overview']);   
			 } else {
			 	if("delivery" in JSON.parse(localStorage.getItem('userWarehouseCart'))){
			 		//do nothing
			 	} else {
			 		 this.router.navigate(['my-warehouse','checkout','cart']);   
			 	}
			}
		} else {
		   this.router.navigate(['my-warehouse','overview']);   
		}

		this.spinner.show();
	}

	ngOnInit() {	
		this.getUserDetails();	
		this.getUserBillingAddress();	
		this.getUserShippingAddress();	
		this.getAllCountry();

		/*setTimeout(() => {
	        this.spinner.hide();
		}, 3000);*/
	}

	getUserDetails() {
		var usercart =  JSON.parse(localStorage.getItem('userWarehouseCart'));
		if(typeof(usercart.personaldetails) !== 'undefined'){
			this.userDetails['title'] = usercart.personaldetails['userTitle'];
			this.userDetails['email'] = usercart.personaldetails['userEmail'];
			this.userDetails['firstName'] = usercart.personaldetails['firstName'];
			this.userDetails['lastName'] = usercart.personaldetails['lastName'];
			this.userDetails['company'] = usercart.personaldetails['company'];
			this.userDetails['contactNumber'] = usercart.personaldetails['contactNumber'];
		} else {
			this.userDetails = this.auth.item.user;
		}
		
	}
	
	getUserBillingAddress() {
		var usercart =  JSON.parse(localStorage.getItem('userWarehouseCart'));

		if(typeof(usercart.personaldetails) !== 'undefined'){
			this.dataSubmit = false;
			//Set Data From Local Storage
			this.userDefaultBillingAddress['firstName'] = usercart.personaldetails['billingFirstName'];
			this.userDefaultBillingAddress['lastName'] = usercart.personaldetails['billingLastName'];
			this.userDefaultBillingAddress['email'] = usercart.personaldetails['billingEmail'];
			this.userDefaultBillingAddress['address'] = usercart.personaldetails['billingAddress'];
			this.userDefaultBillingAddress['alternateAddress'] = usercart.personaldetails['billingAlternateAddress'];
			this.userDefaultBillingAddress['countryId'] = usercart.personaldetails['billingCountryId'];
			this.userDefaultBillingAddress['stateId'] = usercart.personaldetails['billingStateId'];
			this.userDefaultBillingAddress['cityId'] = usercart.personaldetails['billingCityId'];
			this.userDefaultBillingAddress['zipcode'] = usercart.personaldetails['billingZipcode'];
			this.userDefaultBillingAddress['phone'] = usercart.personaldetails['billingPhone'];

			this.defaultBillingCountryId = usercart.personaldetails['billingCountryId'];
			this.billingCountryName = usercart.personaldetails['billingCountryName'];
			this.billingStateName  = usercart.personaldetails['billingStateName'];
			this.billingCityName  = usercart.personaldetails['billingCityName'];

			this.getStateCityList();
		} else {
			this.auth.userShippingAddress('billing').subscribe((data:any) =>{
				if(data.status == 1)
				{
					this.dataSubmit = false;
					this.userDefaultBillingAddress = data.results[0];
					this.defaultBillingCountryId = data.results[0].countryId;
					this.billingCountryName  = data.results[0].country.name;
					this.billingStateName  = data.results[0].state.name;
					this.billingCityName  = data.results[0].city.name;

					this.getStateCityList();
				}
			});
		}
	}

	getStateCityList() {
		this.content.stateCityOfCountry(this.defaultBillingCountryId).subscribe((data:any) => {
				if(data.stateList)
				{
					this.stateList = data.stateList;
				}
				if(data.cityList)
				{
					this.cityList = data.cityList;
				}
			});
	}


	getUserShippingAddress() {
		var usercart =  JSON.parse(localStorage.getItem('userWarehouseCart'));
		
		if(typeof(usercart.personaldetails) !== 'undefined'){
			//Set Data From Local Storage
			this.userDefaultShippingAddress['firstName'] = usercart.personaldetails['shippingFirstName'];
			this.userDefaultShippingAddress['lastName'] = usercart.personaldetails['shippingLastName'];
			this.userDefaultShippingAddress['email'] = usercart.personaldetails['shippingEmail'];
			this.userDefaultShippingAddress['address'] = usercart.personaldetails['shippingAddress'];
			this.userDefaultShippingAddress['alternateAddress'] = usercart.personaldetails['shippingAlternateAddress'];
			this.userDefaultShippingAddress['countryId'] = usercart.personaldetails['shippingCountryId'];
			this.userDefaultShippingAddress['stateId'] = usercart.personaldetails['shippingStateId'];
			this.userDefaultShippingAddress['cityId'] = usercart.personaldetails['shippingCityId'];
			this.userDefaultShippingAddress['zipcode'] = usercart.personaldetails['shippingZipcode'];
			this.userDefaultShippingAddress['phone'] = usercart.personaldetails['shippingPhone'];

			this.shippingCountryName = usercart.personaldetails['shippingCountryName'];
			this.shippingStateName  = usercart.personaldetails['shippingStateName'];
			this.shippingCityName  = usercart.personaldetails['shippingCityName'];
		} else {
			this.auth.userShippingAddress('shipping').subscribe((data:any) =>{
				if(data.status == 1)
				{
					this.userDefaultShippingAddress = data.results[0];
					this.shippingCountryName  = data.results[0].country.name;
					this.shippingStateName  = data.results[0].state.name;
					this.shippingCityName  = data.results[0].city.name;
				}
			});
		}
	}

	
	getAllCountry(){
		this.content.allCountry().subscribe((data:any)=>{
			this.countryList = data;
			setTimeout(() => {
		        /** spinner ends after 3 seconds */
		        this.spinner.hide();
			}, 1000);
		});
	}

	getStateCity(event){
		let selectedCountry = event.target.value;
		this.content.stateCityOfCountry(selectedCountry).subscribe((data:any) => {
			if(data.stateList)
			{
				this.stateList = data.stateList;
			}
		});
	}

	getCity(event) {
		let selectedState = event.target.value;
		this.content.cityOfStates(selectedState).subscribe((data:any) => {
			if(data.cityList)
			{
				this.cityList = data.cityList;
			}
		});
	}

	showForm(event) {
		this.billForm = !this.billForm;
	}


	setBillingCountryName(event) {
		this.billingCountryName = event.target.text;
	}

	setBillingStateName(event) {
		this.billingStateName = event.target.text;
	}

	setBillingCityName(event) {
		this.billingCityName = event.target.text;
	}

	setShippingCountryName(event){
		this.shippingCountryName = event.target.text;
	}

	setShippingStateName(event){
		this.shippingStateName = event.target.text;
	}

	setShippingCityName(event){
		this.shippingCityName = event.target.text;
	}

	hack(value) {
	   return Object.values(value)
	}

	onSubmit(form: NgForm){
    	var storedData =  JSON.parse(localStorage.getItem('userWarehouseCart'));
    	
    	//Clear Storage Data
    	delete storedData.personaldetails ;
    	
    	//Reassign Storage Data
    	const newStoredData = Object.assign({"personaldetails":form.value},storedData);
    	localStorage.setItem('userWarehouseCart', JSON.stringify(newStoredData));

    	this.router.navigate(['my-warehouse','checkout', 'shipping-and-payment']);
    }
}
