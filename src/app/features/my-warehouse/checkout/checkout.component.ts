import { Component, OnInit, NgZone, HostListener } from '@angular/core';

@Component({
	selector: 'app-checkout',
	templateUrl: './checkout.component.html',
	styleUrls: ['./checkout.component.scss']
})

export class CheckoutComponent implements OnInit {

	steps = [];
	// isDisabled: boolean;
	// item: any;
	// public steps: Array<any>;
	url: string;

	constructor() { }

	ngOnInit() {
		this.steps = [
			{
				title: 'Your cart',
				icon: 'shop-online.svg',
				link: '/my-warehouse/checkout/cart',
				isCompleted: true,
				isDisabled: false
			},
			{
				title: 'Personal details',
				icon: 'user.svg',
				link: '/my-warehouse/checkout/personal-detail',
				isCompleted: false,
				isDisabled: false
			},
			{
				title: 'Shipping & payment',
				icon: 'payment.svg',
				link: '/my-warehouse/checkout/shipping-and-payment',
				isCompleted: false,
				isDisabled: true
			},
			{
				title: 'Place order',
				icon: 'place-order.svg',
				link: '/my-warehouse/checkout/place-order',
				isCompleted: false,
				isDisabled: true
			}
		];
	}

	// modal photo shot
	visitedSteps(event) {
		for (const item in this.steps) {
			if (this.steps[item].title === event) {
				this.steps[item].isCompleted = true;
			}
		}
	}



	// addVisited
	addVisited() {
		if (this.url === '/my-warehouse/checkout/personal-detail') {
			// visited: true;
			console.log('a', 'b');
		}
	}
}
