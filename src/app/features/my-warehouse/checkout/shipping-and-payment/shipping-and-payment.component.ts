import { Component, OnInit } from '@angular/core';
import { MywarehouseService } from '../../../../services/mywarehouse.service';
import {Router} from "@angular/router";
import { ToastrService } from 'ngx-toastr';
import { RegistrationService } from '../../../../services/registration.service';

@Component({
	selector: 'app-shipping-and-payment',
	templateUrl: './shipping-and-payment.component.html',
	styleUrls: ['./shipping-and-payment.component.scss']
})
export class ShippingAndPaymentComponent implements OnInit {
	isReadOnly = true;
	personalDetails : any;
	paymentMethodList : Array<any>;
	billingCountryId : any;
	paymentMethodId  : any;
	paymentMethodName : any;
	paymentMethodKey : any;
	shipmentMethodId : any;
	paymentTax : any;
	paymentTaxType : any;
	totalCost : number;

	constructor(
            private router: Router,
            private mywarehouseService : MywarehouseService,
            private toastr: ToastrService,
            private regService : RegistrationService,
	) {
		if("userWarehouseCart" in localStorage){
		  	var usercart = localStorage.getItem('userWarehouseCart');
			if(usercart == null || usercart == ""){
			 	this.router.navigate(['my-warehouse','overview']);   
			 } else {
			 	if("personaldetails" in JSON.parse(localStorage.getItem('userWarehouseCart'))){
			 		//do nothing
			 	} else {
			 		 this.router.navigate(['my-warehouse','checkout','personal-detail']);   
			 	}
			}
		} else {
		   this.router.navigate(['my-warehouse','overview']);   
		}
	 }

	ngOnInit() {
		const usercart =  JSON.parse(localStorage.getItem('userWarehouseCart'));

		if(typeof(usercart.paymentMethod) != 'undefined'){
			this.paymentMethodId = usercart.paymentMethod.paymentMethodId;
			this.paymentMethodName = usercart.paymentMethod.paymentMethodName;
			this.paymentMethodKey = usercart.paymentMethod.paymentMethodKey;
			this.paymentTaxType = usercart.paymentMethod.paymentTaxType;
			this.paymentTax = usercart.paymentMethod.paymentTax;
		}

		this.personalDetails = usercart.personaldetails;
		this.billingCountryId  = usercart.personaldetails.billingCountryId;
		this.totalCost = usercart.totalCost;
		this.getPaymentMethodList();
	}

	getPaymentMethodList() {
		const data = {billingCountryId : this.billingCountryId};
		this.mywarehouseService.getPaymentMethodList(data).subscribe((data:any) => {
			this.paymentMethodList = data;
		});
	}

	changePaymentMethod($event, paymentMethodName, paymentMethodKey, paymentTax, paymentTaxType) {
		this.paymentMethodName = paymentMethodName;
		this.paymentMethodKey = paymentMethodKey;
		this.paymentTax = paymentTax;
		this.paymentTaxType = paymentTaxType;
	}


	proceedToPlaceorder() {
		if(typeof(this.paymentMethodId) != 'undefined'){
			var storedData =  JSON.parse(localStorage.getItem('userWarehouseCart'));

			console.log(storedData);
	    	
	    	//Clear Storage Data
	    	delete storedData.paymentMethod;

	    	var totalTax = 0;

	    	var taxApplicableAmount =  parseFloat(storedData.totalShippingCost);

	    		
	    	
	    	if(this.paymentTaxType == 0)
	    			totalTax = parseFloat(this.paymentTax);
	    		else
	    			totalTax = (taxApplicableAmount / 100)*parseFloat(this.paymentTax);
	    	totalTax = parseFloat(totalTax.toFixed(2));
	    	
	    	//Reassign Storage Data
	    	storedData['totalTax'] = totalTax;

	    	localStorage.setItem('userWarehouseCart', JSON.stringify(storedData));

	    	//Reassign Storage Data
	    	const newStoredData = Object.assign({"paymentMethod":{"paymentMethodId" : this.paymentMethodId, "paymentMethodName" : this.paymentMethodName, "paymentMethodKey" : this.paymentMethodKey, "paymentTax" : this.paymentTax, "paymentTaxType" : this.paymentTaxType}},storedData);
	    	localStorage.setItem('userWarehouseCart', JSON.stringify(newStoredData));

                 if(this.regService.item.user.subscription.couponCode){
                    if("coupondetails" in JSON.parse(localStorage.getItem('userWarehouseCart'))){  
                        this.router.navigate(['shop-for-me','checkout', 'place-order']);
                    } else {
                        this.validateCoupon(this.regService.item.user.subscription.couponCode);
                    }
                } else {
                    this.router.navigate(['my-warehouse','checkout', 'place-order']);
                }
	    } else {
	    	this.toastr.error("Please select a payment method to continue");
	    	return false;
	    }
	}

        validateCoupon(couponcode){
            this.mywarehouseService.validatecouponcode(couponcode).subscribe((response:any) => {
                var storedData =  JSON.parse(localStorage.getItem('userWarehouseCart'));
                if(response.results == 'valid'){
                    if(storedData.isCurrencyChanged == 'Y'){
                            var defaultTotalDiscount = parseFloat(response.amount_to_be_discounted);
                            var discount = defaultTotalDiscount * storedData.exchangeRate;
                            var totalCost = parseFloat(storedData.totalCost);
                            var totalDiscountCost = totalCost-discount;

                            storedData['totalDiscount'] = discount;
                            storedData['defaultTotalDiscount'] = defaultTotalDiscount;
                            storedData['totalBDiscountCost'] = totalCost;
                            storedData['defaultTotalBDiscountCost'] = storedData.defaultTotalCost; 
                            storedData['totalCost'] = totalDiscountCost;
                            storedData['defaultTotalCost'] = parseFloat(storedData.defaultTotalCost) - defaultTotalDiscount;

                    } else {
                            var discount = parseFloat(response.amount_to_be_discounted);
                            var totalCost = parseFloat(storedData.totalCost);
                            var totalDiscountCost = totalCost-discount;

                            storedData['totalDiscount'] = discount;
                            storedData['totalCost'] = totalDiscountCost;
                            storedData['totalBDiscountCost'] = totalCost;
                    }
                    localStorage.setItem('userWarehouseCart', JSON.stringify(storedData));  

                    delete storedData.coupondetails;

                    const couponStore = Object.assign({"coupondetails":{"couponCode": couponcode, "discountAmount":discount, "discountPoint":'', "discountType" : "amount"}},storedData);

                    localStorage.setItem('userWarehouseCart', JSON.stringify(couponStore));
                    this.router.navigate(['my-warehouse','checkout', 'place-order']);
                } else {
                    this.router.navigate(['my-warehouse','checkout', 'place-order']);
                }
            });
	}
}
