import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TrackShipmentComponent } from './track-shipment.component';

const routes: Routes = [
	{ path: '', component: TrackShipmentComponent }
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class TrackShipmentRoutingModule {

}
