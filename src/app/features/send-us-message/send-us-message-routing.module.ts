import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SendUsMessageComponent } from './send-us-message.component';
import { ThankyouComponent } from './thankyou/thankyou.component';


const routes: Routes = [
	{ path: '', component: SendUsMessageComponent },
	{ path: 'thankyou' , component: ThankyouComponent}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class SendUsMessageRoutingModule {

}
