import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShopFromComponent } from './shop-from.component';
import { ShopDirectComponent } from './shop-direct/shop-direct.component';

const routes: Routes = [
	{ path: '', component: ShopFromComponent },
	{
		path: '',
		component: ShopDirectComponent,
		children: [
		{ path: '', redirectTo: 'shop-direct', pathMatch: 'full' },
		{ path: 'shop-direct', component: ShopDirectComponent },
		]
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ShopFromRoutingModule {

}
