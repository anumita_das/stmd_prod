import { NgModule } from '@angular/core';
import { ShopFromComponent } from './shop-from.component';
import { SharedModule } from '@app/shared';
import { ShopFromRoutingModule } from './shop-from-routing.module';
import { ShopDirectComponent } from './shop-direct/shop-direct.component';

@NgModule({
	imports: [
		ShopFromRoutingModule,
		SharedModule
	],
	declarations: [
		ShopFromComponent,
		ShopDirectComponent
	]
})
export class ShopFromModule {

}
