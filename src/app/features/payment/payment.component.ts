import { Component, OnInit, NgZone, HostListener } from '@angular/core';
import { RegistrationService } from '../../services/registration.service';
import { ShopformeService } from '../../services/shopforme.service';
import { Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import { ToastrService } from 'ngx-toastr';
import {Router} from "@angular/router";
import { AppGlobals } from '../../app.globals';

@Component({
	selector: 'app-payment',
	templateUrl: './payment.component.html',
	styleUrls: ['./payment.component.scss'],
	providers: [AppGlobals]
})
export class PaymentComponent implements OnInit {
	isMediumScreen: boolean;
	dtOptions: DataTables.Settings = {};
	payments: Array<any> = [];
	userId : number;
	perPage:any = 10;
	currentPage: number = 1;
	pageSize: number;
	totalItems: number;
    dtTrigger: Subject<any> = new Subject();
    dtTrigger1: Subject<any> = new Subject();
    invoices: Array<any> = [];
    currentInvoicePage: number = 1;
    totalInvoiceItems: number;
    invpageSize:any = 2;
    defaultCurrencySymbol: any;

	constructor(
		private shopformeService : ShopformeService,
		private regService : RegistrationService,
		private toastr: ToastrService,
		private router : Router,
		private _global: AppGlobals,
	) {
		if(this.regService.item)
			this.userId = this.regService.item.user.id;
		this.defaultCurrencySymbol = this._global.defaultCurrencySymbol;
	 }

	ngOnInit() {
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		console.log('Width: ' + width);
		this.checkWindowSize(width);

		this.getPaymentHistory();
		this.getInvoiceList();
		
		// datatable
		this.dtOptions[0] = {
			paging: false,
			ordering: false,
			searching: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};

		this.dtOptions[1] = {
			paging: false,
			ordering: false,
			searching: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};

	}

	getInvoiceList() {
		const formdata = {currentPage : this.currentInvoicePage, userId : this.userId};
		this.regService.getInvoiceList(formdata).subscribe((response:any) => {
			if(response.results == 'success'){
				this.invoices = response.data;				
				this.totalInvoiceItems = response.totalItems;
				this.invpageSize = response.itemsPerPage;
				this.dtTrigger1.next();
			}
		});
	}


	getPaymentHistory() {
		const formdata = {currentPage : this.currentPage, userId : this.userId};
		this.regService.getPaymentHistory(formdata).subscribe((response:any) => {
			if(response.results == 'success'){
				this.payments = response.data;				
				this.totalItems = response.totalItems;
				this.pageSize= response.itemsPerPage;
				this.dtTrigger.next();
			}
		});

	}

	pageinvoiceChanged($event)  {
		const formdata = {currentPage : $event, userId : this.userId, perPage:this.invpageSize};

		this.regService.getInvoiceList(formdata).subscribe((response:any) => {
			if(response.results == 'success'){
				this.currentInvoicePage = $event;
				this.invoices = response.data;
				this.totalInvoiceItems = response.totalItems;
				this.invpageSize= response.itemsPerPage;
			}
		});
	}


	pageChanged($event)  {
		const formdata = {currentPage : $event, userId : this.userId, perPage:this.pageSize};

		this.regService.getPaymentHistory(formdata).subscribe((response:any) => {
			if(response.results == 'success'){
				this.currentPage = $event;
				this.payments = response.data;
				this.totalItems = response.totalItems;
				this.pageSize= response.itemsPerPage;
			}
		});
	}


	perPageChanged($event)  {
		const formdata = {currentPage : 1, userId : this.userId, perPage:this.pageSize};

 		 this.regService.getPaymentHistory(formdata).subscribe((response:any) => {
			if(response.results == 'success'){
				this.currentPage = 1;
				this.payments = response.data;
				this.totalItems = response.totalItems;
				this.pageSize= response.itemsPerPage;
			}
		});
	}

	print(paidForId,paidFor,invoiceId) {
		this.shopformeService.getPaymentInvoice(paidForId,paidFor,invoiceId).subscribe((response:any) => {
		  if(response.status == 1) {
			 	let printContents, popupWin;
				    printContents = response.data;
				    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
				    popupWin.document.open();
				    popupWin.document.write(`
				      <html>
				        <head>
				          <title>Print tab</title>
				          <style>
				          //........Customized style.......
				          </style>
				        </head>
				    <body onload="window.print();window.close()">${printContents}</body>
				      </html>`
				    );
				    popupWin.document.close();
			  } else {
				this.toastr.error('Invoice not available. Please contact site admin!!');
			  }
		});
	}

	payinvoice(invoiceId){
		this.router.navigate(['payment','invoice',invoiceId]);
	}


	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		console.log('Width: ' + event.target.innerWidth);
		this.checkWindowSize(event.target.innerWidth);
	}
}
