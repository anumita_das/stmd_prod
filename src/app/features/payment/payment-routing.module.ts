import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PaymentComponent } from './payment.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { AuthGuardService } from '../../services/auth-guard.service';

const routes: Routes = [
	{ path: '', canActivateChild: [AuthGuardService], component: PaymentComponent },
	{ path: 'invoice/:id', canActivateChild: [AuthGuardService], component: InvoiceComponent },
	
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class PaymentRoutingModule {

}
