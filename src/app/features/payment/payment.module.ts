import { NgModule } from '@angular/core';
import { PaymentRoutingModule } from './payment-routing.module';
import { PaymentComponent } from './payment.component';
import { SharedModule } from '@app/shared';
import {NgxPaginationModule} from 'ngx-pagination';
import { InvoiceComponent } from './invoice/invoice.component';


@NgModule({
	imports: [
		PaymentRoutingModule,
		SharedModule,
		NgxPaginationModule
	],
	declarations: [
		PaymentComponent,
		InvoiceComponent
	]
})
export class PaymentModule {

}
