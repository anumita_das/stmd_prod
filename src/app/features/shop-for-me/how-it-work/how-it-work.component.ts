import { Component, OnInit, HostListener } from '@angular/core';

@Component({
	selector: 'app-how-it-work',
	templateUrl: './how-it-work.component.html',
	styleUrls: ['./how-it-work.component.scss']
})
export class HowItWorkComponent implements OnInit {
	public isMediumScreen: boolean;

	constructor() { }

	ngOnInit() { }

	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}
	
	@HostListener('window: resize', ['$event'])
		onResize(event) {
		this.checkWindowSize(event.target.innerWidth);
	}
}
