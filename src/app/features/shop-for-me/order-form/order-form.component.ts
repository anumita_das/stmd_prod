import { Component, OnInit, NgZone, HostListener } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgForm }   from '@angular/forms';
import { AppGlobals } from '../../../app.globals';
import { Meta, Title } from '@angular/platform-browser';
import { ContentService } from '../../../services/content.service'
import { RegistrationService } from '../../../services/registration.service';
import { ShopformeService } from '../../../services/shopforme.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { StorefromModalComponent } from '../../../shared/components/storefrom-modal/storefrom-modal.component';
import {Router} from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
	selector: 'app-order-form',
	templateUrl: './order-form.component.html',
	styleUrls: ['./order-form.component.scss'],
	providers: [AppGlobals]
})
export class OrderFormComponent implements OnInit {
    isMediumScreen: boolean;
	sidebar: boolean;
	showShipping: boolean;
	dtOptions: DataTables.Settings = {};
	warehouseCode : any;
	bagList : Array<any> = [];
	formSubmit : boolean;
    userId : number;
	paymentDetails : Array<any>;		
	shippingTypes : Array<any>;
	defaultCurrencySymbol : any;
	selectedFile : any;
	isStoreShopForMe: boolean = false;
	bsModalRef: BsModalRef;

	shopforme =  {
		warehouseId : "",
		urgent : "N",
		storeId : "",
		siteCategoryId : "",
		siteSubCategoryId : "",
		siteProductId : "",
		siteProductImage : "",
		websiteUrl : "",
		itemName : "",
		options : "",
		itemPrice : "",
		itemQuantity : "",
		itemShippingCost : "",
		itemImage : "",
	};

	public newAttribute: any = {};
	public warehouseList : any;
	public storeList : any;
	public categoryList : any;
	public subcategoryList : any;
	public productList : any;
	

	constructor(
		private ngZone: NgZone,
		private content: ContentService,
		private regService : RegistrationService,
		private shopformeService : ShopformeService,
		private spinner: NgxSpinnerService,
		private router: Router,
		private _global: AppGlobals,
		private toastr: ToastrService,
		private modalService: BsModalService,
	) {
	  /*let myObj = {storeId : 1, categoryId : 4, warehouseId : 1, warehouseCode : 'US'};
	  localStorage.setItem('storeShopForMe', JSON.stringify(myObj));*/

      this.userId = this.regService.item.user.id;
	  this.defaultCurrencySymbol = this._global.defaultCurrencySymbol;

	  /* Remove Local Storage Data */
	  localStorage.removeItem('userShopForMeCart');
	}

	ngOnInit() {
		this.spinner.show();
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		this.checkWindowSize(width);

		this.getWarehouseList();

		setTimeout(() => {
			this.getStoreList();
			this.getCategoryList();
			this.spinner.hide();
		}, 3000);


		// datatable
		this.dtOptions = {
			paging: false,
			// bInfo: false,
			ordering: false,
			searching: false,
			// bSort: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};
	}

	getStoreList() {
		const formdata = {type : 'shopforme', warehouseId : this.shopforme.warehouseId};
		this.content.storelist(formdata).subscribe((data:any) => {
			this.storeList = data;
			const storeShopForMe = JSON.parse(localStorage.getItem('storeShopForMe'));
			
			if(storeShopForMe){
				
				this.shopforme.storeId = storeShopForMe.storeId;
			}
			else{
				this.shopforme.storeId = '';
			}
		});
	}

	//Fetch Category List
	getCategoryList(){

		if(this.shopforme.storeId!='')
		{
			const formdata = {type : 'shopforme', storeId : this.shopforme.storeId};

			this.content.categorylist(formdata).subscribe((data:any) => {
				this.categoryList = data;
				const storeShopForMe = JSON.parse(localStorage.getItem('storeShopForMe'));

				if(storeShopForMe){				
					this.shopforme.siteCategoryId = storeShopForMe.categoryId;
					this.categoryChanged(this.shopforme.siteCategoryId);
				}
				else
					this.shopforme.siteCategoryId = '';
			});

		}else{


			this.content.categorylistwithoutstore('shopforme').subscribe((data:any) => {
				this.categoryList = data;
				const storeShopForMe = JSON.parse(localStorage.getItem('storeShopForMe'));

				if(storeShopForMe){				
					this.shopforme.siteCategoryId = storeShopForMe.categoryId;
					this.categoryChanged(this.shopforme.siteCategoryId);
				}
				else
					this.shopforme.siteCategoryId = '';
			});
		}
		
	}


	//Fetch Category List on Store Change
	storeChanged(storeId){
		this.categoryList = [];

		if(storeId!='')
		{
			const formdata = {type : 'shopforme', storeId : storeId};
			this.content.categorylist(formdata).subscribe((data:any) => {
			if(data.length > 0){
				this.categoryList = data;
				this.shopforme.siteCategoryId = '';
				}
			else{
				this.shopforme.siteCategoryId = '';
			}
			});
		}
		
	}

	//Fetch Warehouse List
	getWarehouseList(){
		this.content.warehouselist(this.userId).subscribe((data:any) => {
			if(data.length > 0){
				this.warehouseList = data;
				this.shopforme.warehouseId = data[0].id;
				this.warehouseCode = data[0].code;
				
				this.getUserCartList();
			}
		});
	}

	//Fetch Sub Category List
	categoryChanged(categoryId){
		this.subcategoryList = [];
		this.productList = [];

		if(categoryId != ''){
			this.content.subcategorylist(categoryId).subscribe((data:any) => {
				if(data.length > 0){
					this.subcategoryList = data;
					this.shopforme.siteSubCategoryId  = "";
					this.shopforme.siteProductId = "";
				} else {
					this.shopforme.siteSubCategoryId  = "";
					this.shopforme.siteProductId = "";
				}
			});
		}
	}

	//Fetch Product List
	subCategoryChanged(subCategoryId){
		this.productList = [];

		if(subCategoryId != '') {
			this.content.productlist(subCategoryId).subscribe((data:any) => {
				if(data.length > 0){
					this.productList = data;
					this.shopforme.siteProductId  = "";
				} else {
					this.shopforme.siteProductId  = "";
				}
			});
		}
		
	}

	//Set Product Image
	productChanged($event){
		this.shopforme.siteProductImage = $event.target.getAttribute('data-image');
	}

	warehouseChanged($event) {
		this.warehouseCode = $event.target.getAttribute('data-code');
	}


	setTwoNumberDecimal($event) {
		if(!isNaN(parseFloat($event.target.value))){
		 	$event.target.value = Math.abs(parseFloat($event.target.value)).toFixed(2);
		}
	 	else {
	 		$event.target.value = parseFloat("0").toFixed(2);
	 	}
	}

	setInteger($event)
	{
		$event.target.value = Math.abs(Math.floor($event.target.value));
	}

	fileUpload(event) {
		let fileItem = event.target.files[0];
		const file: File = fileItem;
		this.selectedFile = file;
	}

		//Fetch Cart List 
	getUserCartList() {
		let showCart = true;

		const formData = {
	    	id : this.userId,
	    	urgent : this.shopforme.urgent,
	        warehouseId : this.shopforme.warehouseId,
	        type : 'shopforme'
		}
		this.shopformeService.getUserCartList(formData).subscribe((data:any) => {
			const storeShopForMe = JSON.parse(localStorage.getItem('storeShopForMe'));
			if(storeShopForMe){
				if(data.warehouseId > 0){
					showCart = false;
				    this.isStoreShopForMe = true;
				  	this.shopforme.warehouseId = data.warehouseId;
					this.bsModalRef = this.modalService.show(StorefromModalComponent, {backdrop: 'static', keyboard: false});
					this.bsModalRef.content.warehouseCode = this.warehouseCode;
					this.bsModalRef.content.bagList = data.items;
				} else {
				  this.isStoreShopForMe = false;
				  this.shopforme.warehouseId = storeShopForMe.warehouseId;
				  this.warehouseCode = storeShopForMe.warehouseCode;
				  this.shopforme.storeId = storeShopForMe.storeId;
				  this.shopforme.siteCategoryId = storeShopForMe.categoryId;
				}
		  	  
			} else {
				if(data.warehouseId){
					this.isStoreShopForMe = true;
					this.shopforme.warehouseId = data.warehouseId;
				} else {
					this.isStoreShopForMe = false;
				}
			}

			if(showCart){
				this.bagList = data.items;
				this.paymentDetails = data.details;
			}
		});
	}

	//Save Cart Items For Later 
	saveCartItems($event) {
		const formData =  {items : this.bagList };
		this.shopformeService.savecartlist(formData).subscribe((data:any) => {
			if(data.results == 'success'){
				this.toastr.success('Items have been saved for later!');
	 			this.router.navigate(['/shop-for-me','my-wishlist']);
	 		}
			
		});
	}

	//Recalculate Cost
	reCalculateCart($event) {
		    const formData = {
		    	userId : this.userId,
		    	urgent : this.shopforme.urgent,
		        warehouseId : this.shopforme.warehouseId,
		        type : 'shopforme'
		    }
	  
		this.shopformeService.reCalculateCart(formData).subscribe((response:any) => {
			if(response.status == 1){
				this.paymentDetails = response.results.details;

				if(!this.paymentDetails['urgentCostCharged'] && $event.target.name == 'urgent' && this.shopforme.urgent == 'Y'){
					this.shopforme.urgent = 'N';
					this.toastr.error('Urgent cost can only be charged for minimum item cost of '+this.paymentDetails['minUrgentCost']);
				}
			}
		});
	}

	//Save Cart Items For Procurement
	submitProcurement($event, shippingId) {
		const formData =  {
			items : this.bagList, 
			shippingId : shippingId,
			warehouseId : this.shopforme.warehouseId, 
			urgent :  this.shopforme.urgent,
			paymentDetails :  this.paymentDetails
		};
		this.shopformeService.submitprocurementdata(formData).subscribe((data:any) => {
			if(data.results == 'success'){
				this.bagList = [];
	 			this.showShipping = false;	 			
	 			if(data.procurementId != ''){
	 				if(shippingId == -1)
	 				{
	 						this.toastr.success('Your order has been received and we will send you the shipping cost within 24 business hours. To see details of this order at any time, go to you Shop For Me order history, scroll to the bottom and click on details.');
	 					
	 				}else{
	 					this.toastr.success('Shop For Me Data Added Successfully');
	 					
	 				}
	 			setTimeout(() => {
	 				
			        /** spinner ends after 13 seconds */
			       		window.location.reload();
		    	 }, 13000);
	 			}
	 			else {
	 				localStorage.removeItem('userShopForMeCart');
	 				localStorage.setItem('userShopForMeCart', JSON.stringify(data.procurement));
	 				this.router.navigate(['shop-for-me','checkout']);
	 			}
	 		}
			
		});
	}


	//Remove Item From Cart List 
	deleteItem(index, id){
	  this.shopformeService.removeCartItem(id).subscribe((data:any) => {
	    if(data.results == 'success'){
	 		this.getUserCartList();
	 		this.showShipping = false;
	 	}
	  });
	}

	calculateShippingCost() {
		const formData =  {
			userId : this.userId,
			warehouseId : this.shopforme.warehouseId, 
			urgent :  this.shopforme.urgent,
			type : 'shopforme'
		};

		this.shopformeService.calculateShippingCost(formData).subscribe((response:any) => {
			if(response.status == 1) {
				this.shippingTypes = response.data;
				this.showShipping = true;
			} else {
			this.shippingTypes = [];
				this.showShipping = true;
			}
			
		});
	}

		//Add Items to Bag On Submit
	onSubmit(form: NgForm){
		this.spinner.show();

		const formData = new FormData();
		if(this.selectedFile != null)
	  		formData.append('itemImage', this.selectedFile, this.selectedFile.name);
	  	for(let eachField in form.value)
	  	{
	  		formData.append(eachField,form.value[eachField]);
	  	}
	  	formData.append('warehouseId',this.shopforme.warehouseId);

		this.shopformeService.updateUserCart(formData, 'shopforme').subscribe((data:any) => {
			if(data.status == '1'){

			const formData = {
		    	id : this.userId,
		    	urgent : this.shopforme.urgent,
		        warehouseId : this.shopforme.warehouseId,
		        type : 'shopforme'
			}
			this.shopformeService.getUserCartList(formData).subscribe((data:any) => {
				this.bagList = data.items;
				this.paymentDetails = data.details;
				if(!this.paymentDetails['urgentCostCharged'] && this.shopforme.urgent == 'Y'){
					this.shopforme.urgent = 'N';
					this.toastr.error('Urgent cost can only be charged for minimum item cost of '+this.paymentDetails['minUrgentCost']);
				}
			});

			this.selectedFile = null;

			localStorage.removeItem('storeShopForMe');

			this.shopforme =  {
				warehouseId : this.shopforme.warehouseId,
				urgent : this.shopforme.urgent,
				storeId : "",
				siteCategoryId : "",
				siteSubCategoryId : "",
				siteProductId : "",
				siteProductImage : "",
				websiteUrl : "",
				itemName : "",
				options : "",
				itemPrice : "",
				itemQuantity : "",
				itemShippingCost : "",
				itemImage : "",
			};

			form.resetForm(this.shopforme);

			setTimeout(() => {
		        /** spinner ends after 3 seconds */
		        this.spinner.hide();
	    	}, 3000);
	    	this.isStoreShopForMe = true;
		} else {
			setTimeout(() => {
		        /** spinner ends after 3 seconds */
		        this.spinner.hide();
		    }, 3000);
		}
		});	
	}



	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		this.checkWindowSize(event.target.innerWidth);
	}

}
