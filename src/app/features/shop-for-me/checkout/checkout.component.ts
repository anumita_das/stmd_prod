import { Component, OnInit, NgZone, HostListener } from '@angular/core';
import { ActivatedRoute } from '@angular/router';


@Component({
	selector: 'app-checkout',
	templateUrl: './checkout.component.html',
	styleUrls: ['./checkout.component.scss']
})

export class CheckoutComponent implements OnInit {

	steps = [];
	// isDisabled: boolean;
	// item: any;
	// public steps: Array<any>;
	url: string;
	isCompleted : boolean = true;
	isDisabled : boolean = true;

	constructor() { }

	ngOnInit() {
		this.steps = [
			{
				title: 'Your cart',
				icon: 'shop-online.svg',
				link: '/shop-for-me/checkout/cart',
				isCompleted: true,
				isDisabled: false
			},
			{
				title: 'Personal details',
				icon: 'user.svg',
				link: '/shop-for-me/checkout/personal-detail',
				isCompleted: false,
				isDisabled: true
			},
			{
				title: 'Shipping & payment',
				icon: 'payment.svg',
				link: '/shop-for-me/checkout/shipping-and-payment',
				isCompleted: false,
				isDisabled: true
			},
			{
				title: 'Place order',
				icon: 'place-order.svg',
				link: '/shop-for-me/checkout/place-order',
				isCompleted: false,
				isDisabled: true
			}
		];
		const curUrl = location.href;

		const curhost = curUrl.split("#");


		this.visitedSteps(curhost[1]);
	}

	// modal photo shot
	visitedSteps(event) {
		for (const item in this.steps) {
			if (this.steps[item].link === event) {
				this.steps[item].isDisabled = false;
				this.steps[item].isCompleted = true;
			}
		}
	}



	// addVisited
	addVisited() {
		if (this.url === '/shop-for-me/checkout/personal-detail') {
			// visited: true;
			console.log('a', 'b');
		}
	}
}
