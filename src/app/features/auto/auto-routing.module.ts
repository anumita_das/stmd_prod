import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AutoComponent } from './auto.component';
import { BuyACarForMeComponent } from './buy-a-car-for-me/buy-a-car-for-me.component';
import { ShipMyCarComponent } from './ship-my-car/ship-my-car.component';
import { OtherVehicleComponent } from './other-vehicle/other-vehicle.component';
import { CarIHaveBoughtComponent } from './car-i-have-bought/car-i-have-bought.component';
import { CarWarehouseComponent } from './my-warehouse/my-warehouse.component';
import { AutoCheckoutComponent } from './auto-checkout/auto-checkout.component';
import { AuthGuardService } from '../../services/auth-guard.service';
// import { MyCartComponent } from './my-cart/my-cart.component';


const routes: Routes = [
	{
		path: '', redirectTo: 'buy-a-car-for-me', pathMatch: 'full'
	},
	{
		path: '',
		component: AutoComponent,
		children: [
			{ path: 'buy-a-car-for-me',canActivateChild: [AuthGuardService], component: BuyACarForMeComponent },
			{ path: 'ship-my-car',canActivateChild: [AuthGuardService], component: ShipMyCarComponent },
			{ path: 'other-vehicle',canActivateChild: [AuthGuardService], component: OtherVehicleComponent },
			{ path: 'car-i-have-bought',canActivateChild: [AuthGuardService], component: CarIHaveBoughtComponent },
			{ path: 'my-warehouse',canActivateChild: [AuthGuardService], component: CarWarehouseComponent },
			{ path: 'checkout',canActivateChild: [AuthGuardService], component: AutoCheckoutComponent }
			// { path: 'my-cart', component: MyCartComponent }
		]
	}

];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class AutoRoutingModule {

}
