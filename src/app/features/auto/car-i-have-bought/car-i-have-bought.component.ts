import { Component, OnInit, Renderer2, HostListener } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgForm }   from '@angular/forms';
import { AppGlobals } from '../../../app.globals';
import { Meta, Title } from '@angular/platform-browser';
import { AutoService } from '../../../shared/services/auto.service';
import {Router} from "@angular/router";
import { RegistrationService } from '../../../services/registration.service';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
	selector: 'app-car-i-have-bought',
	templateUrl: './car-i-have-bought.component.html',
	styleUrls: ['./car-i-have-bought.component.scss']
})
export class CarIHaveBoughtComponent implements OnInit {
	tracks: Array<any>;
	links: Array<any>;
	isMediumScreen: boolean;
	userId:any;
	cars:any=[];
	perPage:any = 10;
	currentPage: number = 1;
	pageSize: number;
	totalItems: number;
	constructor(
		private _global: AppGlobals,
		private meta: Meta, 
		private title: Title,
		private toastr: ToastrService,
		private renderer: Renderer2,
		public auto: AutoService,
		public regService : RegistrationService,
		public spinner : NgxSpinnerService,
	) { }

	ngOnInit() {
		this.spinner.show();
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		console.log('Width: ' + width);
		this.checkWindowSize(width);

		// links
		this.links = [
			{
				title: 'Cars I Have Bought',
				url: 'auto/car-i-have-bought'
			},
			{
				title: 'My Cars in Transit',
				url: 'auto/my-warehouse'
			}
		];

		//var parsed =  JSON.parse(localStorage.getItem('tokens'));
		this.userId = this.regService.userId;

		this.auto.getCarIHaveBoughtDetails(this.userId, this.currentPage, this.perPage).subscribe((data:any) => {
			if(data.results == 'success'){
				this.cars = data.cars;
				this.totalItems = data.totalItems;
				this.pageSize= data.itemsPerPage;
			}
			this.spinner.hide();
		});


		
	}

	pageChanged($event)  {
		this.spinner.show();
		this.auto.getCarIHaveBoughtDetails(this.userId, $event, this.perPage).subscribe((data:any) => {
			if(data.results == 'success'){
				this.currentPage = $event;
				this.cars = data.cars;
				this.totalItems = data.totalItems;
				this.pageSize= data.itemsPerPage;	
			}
			this.spinner.hide();
		});
	}

	perPageChanged($event)  {
		this.spinner.show();
		this.auto.getCarIHaveBoughtDetails(this.userId, 1, this.perPage).subscribe((data:any) => {
			if(data.results == 'success'){
				this.currentPage = 1;
				this.cars = data.cars;
				this.totalItems = data.totalItems;
				this.pageSize= data.itemsPerPage;		
			}
			this.spinner.hide();
		});
	}


	// check window size
	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		console.log('Width: ' + event.target.innerWidth);
		this.checkWindowSize(event.target.innerWidth);
	}

	showSuccess() {
		this.toastr.success('Hello world!', 'Toastr fun!');
	}

	print(procurementId,invoiceType) {
		let printContents, popupWin;
		this.spinner.show();
		this.auto.getInvoice(procurementId,'buycarforme',invoiceType,this.userId).subscribe((response:any) => {
		    printContents = response.data;
		    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
		    popupWin.document.open();
		    popupWin.document.write(`
		      <html>
		        <head>
		          <title>Print tab</title>
		          <style>
		          //........Customized style.......
		          </style>
		        </head>
		    <body onload="window.print();window.close()">${printContents}</body>
		      </html>`
		    );
		    popupWin.document.close();
		    this.spinner.hide();
		});

	    
	}
}
