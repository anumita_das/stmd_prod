import { NgModule } from '@angular/core';
import { AutoRoutingModule } from './auto-routing.module';
import { AutoComponent } from './auto.component';
import { SharedModule } from '@app/shared';
import { BuyACarForMeComponent } from './buy-a-car-for-me/buy-a-car-for-me.component';
import { ShipMyCarComponent } from './ship-my-car/ship-my-car.component';
import { OtherVehicleComponent } from './other-vehicle/other-vehicle.component';
import { CarIHaveBoughtComponent } from './car-i-have-bought/car-i-have-bought.component';
import { CarWarehouseComponent } from './my-warehouse/my-warehouse.component';
import { AutoCheckoutComponent } from './auto-checkout/auto-checkout.component';
// import { MyCartComponent } from './my-cart/my-cart.component';
import {NgxPaginationModule} from 'ngx-pagination';

@NgModule({
	imports: [
		AutoRoutingModule,
		SharedModule,
		NgxPaginationModule
	],
	declarations: [
		AutoComponent,
		BuyACarForMeComponent,
		ShipMyCarComponent,
		OtherVehicleComponent,
		CarIHaveBoughtComponent,
		AutoCheckoutComponent,
		CarWarehouseComponent
		// MyCartComponent
	],
	exports: [
		AutoComponent,
	]
})
export class AutoModule {

}
