import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JoinComponent } from './join.component';
import { PaymentComponent } from './payment/payment.component';

const routes: Routes = [
	{ path: '', component: JoinComponent },
	{ path: 'banner/:id',  component: JoinComponent },
        { path: 'payment', component: PaymentComponent },
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class JoinRoutingModule {

}
