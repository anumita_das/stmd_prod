import { Component, OnInit, ElementRef } from '@angular/core';
import { NgForm }   from '@angular/forms';
import { User } from '../../shared/models/user.model';
import { RegistrationService } from '../../services/registration.service';
import { ContentService } from '../../services/content.service';
import { Router, ActivatedRoute, Params, ParamMap } from '@angular/router';
import { AppGlobals } from '../../app.globals';
import { Meta, Title } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
//import { NumberOnlyDirective } from '../../number.directive';

@Component({
	selector: 'app-join',
	templateUrl: './join.component.html',
	styleUrls: ['./join.component.scss'],
	providers: [AppGlobals]
})
export class JoinComponent implements OnInit {

	user  = new User();
	bannerImage : string;
	userId : string;
	token : string;
	registrationSuccess : boolean;
	tokenActivate : boolean;
	registrationForm : boolean;
	activationMsg : string = 'checking';
	formSubmitted : boolean;
	referralCode : boolean;
	formError : string;
	countryList : any;
	stateList:any;
	cityList:any;
	phcodeList:any;
	registrationMsg : string;
	selectedItem : any;
	activationResendUser : any = "";
	showPassText : boolean = false;
	showRepeatPassText : boolean = false;

	hidePwdStrength:any;

	deactiveBtn : any = true;
	passwordMatch : boolean = true;
	emailMatch : boolean = true;
        subscribedplan:any;

	public barLabel: string = "Password strength:";
        public myColors = ['#DD2C00', '#FF6D00', '#FFD600', '#AEEA00', '#00C853'];
        wcodePattern = "^.{8,}$"; 
        //wcodePattern = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,20}$";

	constructor(
		private regService : RegistrationService,
		private contentService:ContentService,
		private route: ActivatedRoute,
		private _global: AppGlobals,
		private meta: Meta, 
		private title: Title,
		private toastr: ToastrService,
		public spinner : NgxSpinnerService,
		public router: Router,
		private el: ElementRef
	)  { 
		title.setTitle(this._global.siteTitle);
	    meta.addTags([ 
	      {
	        name: 'keywords', content: this._global.siteMetaKeywords
	      },
	      {
	        name: 'description', content: this._global.siteMetaDescription
	      },
	    ]);
	}

	ngOnInit() {
		if (this.regService.isLoggedIn) {
	      // Get the redirect URL from our auth service
	      // If no redirect has been set, use the default
	      const redirect = this.regService.redirectUrl ? this.regService.redirectUrl : 'admin';
	      // Redirect the user
	      this.router.navigate([redirect]);
	    }

		this.user.title = "";
		this.registrationSuccess = false;
		this.tokenActivate = false;
		this.registrationForm = true;
		this.formSubmitted = false;
		this.referralCode = false;
		this.formError = ''; 
		this.user.country = '';
		this.user.state = '';
		this.user.city = '';
		this.user.phcode = "234";


		if(this.route.snapshot.paramMap.get('id') && !this.route.snapshot.paramMap.get('token'))
		{
			this.getBannerById(this.route.snapshot.paramMap.get('id'));
		}
		else if(this.route.snapshot.paramMap.get('id') && this.route.snapshot.paramMap.get('token'))
		{
			this.getBanner();
			if(this.route.snapshot.paramMap.get('id') == 'refer')
			{
				this.referralCode = true;
				this.user.referralCode = this.route.snapshot.paramMap.get('token');
			}
			else
			{
				this.userId = this.route.snapshot.paramMap.get('id');
				this.token = this.route.snapshot.paramMap.get('token');
				this.registrationForm = false;
				this.tokenActivate = true;
				this.registrationSuccess = false; 
				this.checkTokens();
			}
			
		}
		else
		{
			this.getBanner();
		}
		//console.log(this.referralCode);
		//this.resetForm();

		this.getAllCountry();
		this.getAllCode();

                if("subscribedetail" in localStorage)
                    this.subscribedplan = JSON.parse(localStorage.getItem('subscribedetail'));
                else
                    this.subscribedplan = '';

                if("paymentsuccess" in localStorage) {
                        let user = JSON.parse(localStorage.getItem('paymentsuccess'));
                        this.activationResendUser = user.userId;
                        this.tokenActivate = false;
                        this.registrationSuccess = true; 
                        this.formSubmitted = false;
                        this.registrationForm = false;
                        localStorage.removeItem('paymentsuccess');
                }
                    
	}

	resetForm(form?: NgForm) {
		if(form != null)
		form.resetForm();
		this.user = {
			title : "",
			firstName : "",
			lastName : "",
			email : "",
			confirmEmail : "",
			number : "",
			password : "",
			confPassword : "",
			company : "",
			referralCode : "",
			phcode:"",
			address : "",
			alternateaddress : "",	
			country : "",
			state : "",
			city : "",
			unit:""
		}
	}

	onSubmit(form: NgForm){
		if (!form.valid) {
            let target;

            target = this.el.nativeElement.querySelector('.form-control.ng-invalid');
            if (target) {
                $('html,body').animate({ scrollTop: $(target).offset().top }, 'slow');
                target.focus();
            }
        }
        else
        {
            this.spinner.show();
                localStorage.removeItem('registrationdetail');

                this.formSubmitted = true;
                this.regService.registerUser(form.value).subscribe((data:any) => {

                    if(data.status == 1)
                    {
                        if("subscribedetail" in localStorage) {
                            localStorage.setItem('registrationdetail', JSON.stringify(data.results));
                            this.router.navigate(['/join', 'payment']);
                        } else {
                        this.activationResendUser = data.results.id;
                        this.tokenActivate = false;
                        this.registrationSuccess = true; 
                        this.formSubmitted = false;
                        this.registrationForm = false;
                        this.spinner.hide();
                       }
                    }
                    else
                    {
                            this.tokenActivate = false;
                            this.registrationSuccess = false; 
                            this.formSubmitted = false;
                            this.spinner.hide();
                            setTimeout(() => {
                                    this.spinner.hide();
                                    this.registrationForm = true;
                                    this.spinner.hide();
                                    this.toastr.error(data.results);
                            }, 2);
                    }
            });
            }
	  }

	getAllCountry(){
		this.contentService.allCountry().subscribe((data:any)=>{
			this.countryList = data;
		});
	}
	getAllCode()
	{
		this.contentService.allCode().subscribe((data:any)=>{
			this.phcodeList = data;
			this.selectedItem = data[0]['image']+data[0]['name']+data[0]['isdCode'];
		});
	}

	getStateCity(event){
		let selectedCountry = event.target.value;
		this.contentService.stateCityOfCountry(selectedCountry).subscribe((data:any) => {
			if(data.stateList)
			{
				this.stateList = data.stateList;
				this.cityList = [];
				this.cityList = data.cityList;
			}
		});
	}

	getCity(event) {
		let selectedState = event.target.value;
		this.contentService.cityOfStates(selectedState).subscribe((data:any) => {
			if(data.cityList)
			{
				this.cityList = data.cityList;
			}
		});
	}



	getBanner() {

		this.contentService.registrationBanner().subscribe((data:any) => {
			if(data.status == 1)
			{
				this.bannerImage = data.results.bannerImage;
			}

			//console.log(this.bannerImage);
		});
	}

	getBannerById(imageId) {
		this.contentService.registrationBannerById(imageId).subscribe((data:any) => {
			if(data.status == 1)
			{
				this.bannerImage = data.results.bannerImage;
			}

			console.log(this.bannerImage);
		});
	}

	checkTokens() {
		const token = {
			id:this.userId,
			token:this.token,
		}
		this.regService.checkActivationLink(token).subscribe((data:any)=>{
			if(data.status=='1')
				this.activationMsg = "activate";
			else if(data.status == '-1')
				this.activationMsg = "error";
		});
	}

	checkEmptyPassword(event){
		if(event == ''){
			this.hidePwdStrength = true;
		} else {
			this.hidePwdStrength = false;
		}

		if(this.user.confPassword !== event){
			this.deactiveBtn = false;
		} else {
			this.deactiveBtn = true;
		}
	}

	checkMatchPasswords(event){

		if(this.user.password !== event){
			this.deactiveBtn = false;
			this.passwordMatch = false;
		} else {
			this.deactiveBtn = true;
			this.passwordMatch = true;
		}

	}

	checkMatchEmails(event){

		if(this.user.email !== event){
			this.deactiveBtn = false;
			this.emailMatch = false;
		} else {
			this.deactiveBtn = true;
			this.emailMatch = true;
		}

	}

	resendActivationMail(userid) {
		this.spinner.show();
		this.regService.resendActivation(userid).subscribe((data:any)=>{
			this.spinner.hide();
			if(data.status=='1')
			{
				this.activationResendUser = '';
			}
			else
			{
				this.toastr.error('Something went wrong! Please click resend link once again')
			}
		});
	}

	changePasswordView(field) {
		if(field == 'pass')
			this.showPassText = !this.showPassText;
		else
			this.showRepeatPassText = !this.showRepeatPassText
	}

}
