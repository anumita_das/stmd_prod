import { Component, OnInit, NgZone, HostListener } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { RegistrationService } from '../../services/registration.service';
import { ContentService } from '../../services/content.service';
import { AddressbookModalComponent } from '../../shared/components/addressbook-modal/addressbook-modal.component';
import { UserAddress } from '../../shared/models/useraddress.model';
import { WarehouseModalComponent } from '../../shared/components/warehouse-modal/warehouse-modal.component';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { DeliveryaddressotpModalComponent } from '../../shared/components/deliveryaddressotp-modal/deliveryaddressotp-modal.component';

@Component({
	selector: 'app-addressbook',
	templateUrl: './addressbook.component.html',
	styleUrls: ['./addressbook.component.scss']
})
export class AddressbookComponent implements OnInit {
	isMediumScreen: boolean;
	sidebar: boolean;
	isCollapsed: boolean;
	bsModalRef: BsModalRef;
	userShippingAddress : Array<object>;
	userAddress = new UserAddress();
	addressId : any = '';
	phcode : any;
	altphcode : any;
	userId:any;
	resendLinkEnabled:any = false;
	constructor(
		private ngZone: NgZone,
		private modalService: BsModalService,
		private auth: RegistrationService,
		private content: ContentService,
		public toastr : ToastrService,
		public spinner : NgxSpinnerService,
		public router: Router,
	) { this.userId = this.auth.item.user.id; }

	ngOnInit() {
		this.spinner.show();
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		//console.log('Width: ' + width);
		this.checkWindowSize(width);

		// account isCollapsed
		this.isCollapsed = false;
		this.getUserAddressInfo();
	}

	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		//console.log('Width: ' + event.target.innerWidth);
		this.checkWindowSize(event.target.innerWidth);
	}

	/*checkVirtualtourVisit() {

		this.auth.checkVirtualtourVisit('addressbook').subscribe((data:any) => {
			if(data.status == '1')
			{
				if(data.results == '0')
				{
					this.router.navigateByUrl('/virtual-tour/my-account/addressbook');
				}
			}
		});
	}*/

	getUserAddressInfo() {
		this.auth.userShippingAddress('All').subscribe((data:any) =>{
			if(data.status == 1)
			{
				this.userShippingAddress = data.results;
			}
			this.spinner.hide();
		});
	}

	modifyAddress(addressId,type){
		this.spinner.show();
		this.auth.userSelectedShippingAddress(addressId).subscribe((data:any) =>{
			//console.log(data);
			this.spinner.hide();
			if(data.status==1)
			{
				for(let eachShippingAddress of data.results)
				{
					this.content.stateCityOfCountry(eachShippingAddress.countryId).subscribe((data:any) => {
						if(data.stateList)
						{
							this.bsModalRef.content.stateList = data.stateList;
						}
					});

					this.content.cityOfStates(eachShippingAddress.stateId).subscribe((data:any) => {
						if(data.cityList)
						{
							this.bsModalRef.content.cityList = data.cityList;
						}
					});

					this.userAddress = {
						userid : eachShippingAddress.userId,
						unit: eachShippingAddress.unit,
						title : eachShippingAddress.title,
						firstName : eachShippingAddress.firstName,
						lastName : eachShippingAddress.lastName,
						email : eachShippingAddress.email,
						address : eachShippingAddress.address,
						alternateAddress : eachShippingAddress.alternateAddress,
						country : eachShippingAddress.countryId,
						state : eachShippingAddress.stateId,
						city : eachShippingAddress.cityId,
						zipcode : eachShippingAddress.zipcode,
						phone : eachShippingAddress.phone,
						alternatePhone : eachShippingAddress.alternatePhone,
						addressType : '',
					};
					this.phcode = eachShippingAddress.isdCode;
					this.altphcode = eachShippingAddress.altIsdCode;
				}
			}

			this.bsModalRef = this.modalService.show(AddressbookModalComponent, {});
			this.bsModalRef.content.closeBtnName = 'Close';
			this.bsModalRef.content.UserAddress = this.userAddress;
			this.bsModalRef.content.phcodeVal = this.phcode;
			this.bsModalRef.content.altphcodeVal = this.altphcode;
			this.bsModalRef.content.adressbookId = addressId;

		});
	}

	setAddressDefault(type, addressId){

		if(addressId!='')
		{
			this.addressId = addressId;
			
			this.spinner.show();
			if(type == 'shipping'){
				//this.spinner.hide();
					this.auth.sendOtpForModifyShippingAddress(this.userId, this.addressId, 'no').subscribe((data:any) => {
						//this.bsModalRef = this.modalService.show(DeliveryaddressotpModalComponent, {});
						console.log("mail fired");
					});
					this.spinner.hide();
					this.bsModalRef = this.modalService.show(DeliveryaddressotpModalComponent, {});
					this.bsModalRef.content.setisdefault = 'default';
					this.bsModalRef.content.addressType = type;
					this.bsModalRef.content.adressbookId = this.addressId;
					setTimeout(() => {
					      // simulating API call, sets to true after 5 seconds
					      this.resendLinkEnabled = true;
					      this.bsModalRef.content.resendLinkEnabled = this.resendLinkEnabled;
					   	}, 11000);

						var counter = 10;
						var newYearCountdown = setInterval(() => {
						  //console.log(counter);
						  this.bsModalRef.content.countdown = counter;
						  counter--;
						  if (counter === 0) {
						    clearInterval(newYearCountdown);
						  }
						}, 1000);


				} else {
					this.auth.setDefaultShippingAddress(this.addressId,type).subscribe((data:any) => {
						this.spinner.hide();
						this.addressId = '';
						if(data.status==1)
						{
							this.getUserAddressInfo();
							this.toastr.success('Selected address modified to default '+type+' address');
						}
						else if(data.status == '-1')
						{
							this.toastr.error(data.results);
						}
						else
						{
							this.toastr.error('Something went wrong');
						}
					});
				}
			
		}
		else
		{
			this.toastr.error('Please select a address first');
		}
	}

	addNewAddress() {
		this.spinner.show();
		this.bsModalRef = this.modalService.show(AddressbookModalComponent, {});
		this.bsModalRef.content.closeBtnName = 'Close';
		this.spinner.hide();
	}

	updateAddressId(addressId) {
		this.addressId = addressId;
	}
}
