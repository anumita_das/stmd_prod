 import { Injectable } from '@angular/core';

@Injectable()
export class AppGlobals {
 	readonly siteTitle: string = 'Import Export from US UK & Nigeria| Shoptomydoor';
    readonly siteMetaKeywords: string = 'shipping from USA and UK to Nigeria, Shop &amp; ship from USA to Nigeria, shipping companies in Nigeria, shipping to Nigeria from US, cargo to Nigeria, shipping from Nigeria to us, procurement from US, Shipping cost from Nigeria to USA,  online stores that ship to Nigeria, door to door shipping from USA to Nigeria, US stores that ship to Nigeria, shipping companies in Nigeria, Air shipping to Nigeria, ship to Nigeria';
    readonly siteMetaDescription: string = 'Shop, ship and relax with Shoptomydoor. We give cheapest import/export solution for your business/personal needs. We ship from US/UK. Shop from amazon, eBay, Zappos, Walmart etc.';
    defaultCurrencySymbol: string = '$';
    virtualTourEntered : string;
}